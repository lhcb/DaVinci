2024-06-18 DaVinci v64r7
===

This version uses
Moore [v55r10p1](../../../../Moore/-/tags/v55r10p1)
Allen [v4r10p1](../../../../Allen/-/tags/v4r10p1),
Rec [v36r10p1](../../../../Rec/-/tags/v36r10p1),
Lbcom [v35r10](../../../../Lbcom/-/tags/v35r10),
LHCb [v55r10](../../../../LHCb/-/tags/v55r10),
Detector [v1r33](../../../../Detector/-/tags/v1r33),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to DaVinci [v64r6](/../../tags/v64r6), with the following changes:


### Fixes ~"bug fix" ~workaround

- Remove unneccesary ProbNN options, !1105 (@mveghel)


### Enhancements ~enhancement

- ~Functors | New functors for estimation of helicity angle, !1040 (@vsvintoz)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Change in exclusion list following LHCb!4565, !1099 (@msaur)
- Update References for: LHCb!4565, Moore!3448, Moore!3506, DaVinci!1099 based..., !1106 (@msaur)
