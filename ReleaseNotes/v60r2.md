2022-03-31 DaVinci v60r2
===

This version uses Analysis v40r2 and Rec v34r1.

This version is released on `master` branch.
Built relative to DaVinci [v60r1](/../../tags/v60r1), with the following changes:

### New features ~"new feature"

- ~Configuration | Add feature for getting particles from TES matching a decay descriptor, !670 (@dfazzini)
- Upstream project highlights :star:

- ~Tuples | FunTuple - API changes and simplification of Python helper modules, Analysis!862 (@erodrigu)
- Remove special field EVENT, add parameter event_variables to FunTuple, Analysis!875 (@sstahl) [DaVinci#43]
- ~Functors | Added definition of HltFilter ThOr functor, Rec!2598 (@lpica) [Rec#218]
- ~Functors | Add functors for IsPhoton, IsNotE, IsNotH, ShowerShape, Rec!2736 (@tnanut)
- ~Functors | Add functors for composite flight distance vector from BPV, Rec!2643 (@mrudolph)
- ~Functors | Add functor for impact parameter wrt bestPV, Rec!2720 (@amathad)
- ~Tuples | Functor returning decisions of list of HLT lines and extra ODIN functors, Rec!2747 (@amathad)
- ~Functors ~Tuples ~"Flavour tagging" | Add ParticleTagger tool and related Thor functors, Rec!2682 (@dfazzini)


### Fixes ~"bug fix" ~workaround

- ~Configuration | Avoid race conditions between tests, !672 (@dfazzini)
- ~Configuration | Fix typo in jobOptions-Example.py, !644 (@erodrigu)
- ~Tuples | Fix ConfiguredMCTruthAndBkgCatAlg correctly propagating RootInTES, !640 (@erodrigu)
- Upstream project highlights :star:


### Enhancements ~enhancement

- Test New Functor Collections, !646 (@gunther)
- Upstream project highlights :star:
- ~Functors ~Tuples | Add additional functors to example-tupling-AllFunctors.py, !656 (@graven)
- ~Functors ~Tuples | Storing event info (trigger and odin) with FunTuple, !654 (@amathad)
- ~Functors ~Tuples | Test the new BPVIP functor, !643 (@amathad)
- ~Tuples | FunTuple - API changes, !652 (@erodrigu)
- Instantiation of FunctorCollection() with empty dictionary, !664 (@amathad)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Configuration - better test coverage and doc improvements, !655 (@erodrigu)
- ~Configuration | Clean up & add unit tests, !642 (@dfazzini)
- ~Functors | Test fixed SUBCOMB functor, !666 (@gunther)
- ~Functors ~Tuples | Add more functors to all test, !657 (@pkoppenb)
- ~Simulation | Modernize xgen test, !641 (@pkoppenb)
- ~Build | Add missing find_package to private dependencies, !677 (@rmatev)
- Fix race condition in tests, !675 (@rmatev)
- Exclude pattern FunctorFactory\s{1,}INFO Cache miss for functor: .*, !645 (@pkoppenb)
- Update tests and examples following fix to pyConf for data dependencies, !637 (@amathad)
- Add test for difference between end vertices of two composites, !635 (@amathad)
- Adding qmtest for ParticleTaggerAlg and related functors, !627 (@dfazzini)
- Upstream project highlights :star:
- ~Configuration | Use absolute path in user algorithms of gaudirun example, !661 (@sstahl)
- ~Configuration ~Tuples | Rename ConfiguredMCTruthAndBkgCatAlg and ConfiguredFuntuple, !662 (@amathad)
- ~"MC checking" ~Simulation | Test MC associations for Sprucing output, !653 (@nskidmor)
- ~Tuples | Remove special field EVENT, add parameter event_variables to FunTuple, !669 (@sstahl)
- Update References for: Detector!158, LHCb!3454, Rec!2794, Panoptes!146 based on lhcb-master-mr/4119, !680 (@rmatev)
- Update References for: LHCb!3430, Rec!2699, Moore!1416, Analysis!871 based on lhcb-master-mr/4076, !678 (@chasse)
