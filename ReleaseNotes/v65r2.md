2025-02-20 DaVinci v65r2
===

This version uses
Moore [v56r3](../../../../Moore/-/tags/v56r3),
Allen [v5r3](../../../../Allen/-/tags/v5r3),
Rec [v37r3](../../../../Rec/-/tags/v37r3),
Lbcom [v36r4](../../../../Lbcom/-/tags/v36r4),
LHCb [v56r4](../../../../LHCb/-/tags/v56r4),
Gaudi [v39r2](../../../../Gaudi/-/tags/v39r2),
Detector [v2r4](../../../../Detector/-/tags/v2r4) and
LCG [105c](http://lcginfo.cern.ch/release/105c/) with ROOT 6.30.08.


This version is released on the `master` branch.
Built relative to DaVinci [v65r1](/../../tags/v65r1), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- Update CMakeLists.txt, !1240 (@masmith)
- Fix Integration test following TrackMonitor changes, !1238 (@tmombach)
- Update References for: Rec!4264 based on lhcb-master-mr/12614, !1236 (@lhcbsoft)
