2021-12-15 DaVinci v60r0
===

First version with only FunTuple as tupling engine.

This version uses
Analysis [v40r0](../../../../Analysis/-/tags/v40r0),
Phys [v33r4](../../../../Phys/-/tags/v33r4),
Rec [v33r4](../../../../Rec/-/tags/v33r4),
Lbcom [v33r5](../../../../Lbcom/-/tags/v33r5),
LHCb [v53r5](../../../../LHCb/-/tags/v53r5),
Gaudi [v36r2](../../../../Gaudi/-/tags/v36r2) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to DaVinci [v54r1](/../../tags/v54r1), with the following changes:

### New features ~"new feature"

- ~Configuration | Adding function for reco v2 RecVertices, !598 (@dfazzini)
- Adding  function to return a list of filters and tupling algorithms, !587 (@dfazzini)
- Upstream project highlights :star:
- ~Tuples | Remove all TupleTools, Analysis!828 (@pkoppenb)
- ~Tuples | FunTuple enhancements, Analysis!838 (@amathad)
- ~Tuples | DecayTreeFitterAlg to access refitted candidates, Phys!970 (@pkoppenb)
- ~Tuples | Add functor to access decaytreefitted particles, Rec!2504 (@pkoppenb)
- ~"Flavour tagging" | Algorithm that selects the particles used in the FlavourTagging, Phys!1002 (@cprouve)


### Fixes ~"bug fix" ~workaround

- ~Configuration | Fix issue for reading hlt2 files, !602 (@dfazzini)
- Fix DaVinciTests/tests/options/Upgrade/DaVinci-Options.yaml, !607 (@erodrigu)
- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Add test of override_data_options option set in random order, !611 (@erodrigu)
- Add Pytest tests for the davinci script in DaVinciSys, !608 (@erodrigu)
- Introduce Pytest-based tests for the DaVinci configuration, !603 (@erodrigu)
- Adding test for run DaVinci with gaudirun.py, !599 (@dfazzini)
- Adapted ref files to change in Packers, !571 (@sponce)
- Upstream project highlights :star:


### Documentation ~Documentation

- Update README.md to document the legacy stripping branches, !583 (@cattanem)
### Other

- ~Configuration | Resolve "Overriding options in davinci script depends on order of special flag overwrite_data_options" & bug with bool passed via command line, !609 (@dfazzini) [#22]
- ~Configuration | More davinci script documentation, !606 (@erodrigu)
- ~Configuration | Better documentation for master function run_davinci, !604 (@erodrigu)
- ~Configuration | Resolve "Wrong exception when DB tags are not given", !596 (@dfazzini) [#17]
- ~Configuration | Improving design of DV add_filter function, !591 (@dfazzini)
- ~Configuration | Adding missing features for correctly reading a spruced file, !577 (@dfazzini)
- ~Tuples | Fix for issue 175 : Mass constraint in DecayTreeFitterAlg does not work, !593 (@pkoppenb)
- ~Tuples | Update references for FunTuple DV tests, !585 (@mferrill)
- ~Tuples | Test for DecayTreeFitterAlg, !569 (@pkoppenb)
- Update References for: LHCb!3241, Rec!2537, Moore!993 based on lhcb-master-mr/3276, !605 (@lhcbsoft)
- Test of all functors, !594 (@pkoppenb)
