2024-11-05 DaVinci v64r13
===

This version uses
Moore [v55r16](../../../../Moore/-/tags/v55r16),
Allen [v4r16](../../../../Allen/-/tags/v4r16),
Rec [v36r16](../../../../Rec/-/tags/v36r16),
Lbcom [v35r16](../../../../Lbcom/-/tags/v35r16),
LHCb [v55r16](../../../../LHCb/-/tags/v55r16),
Gaudi [v38r1p1](../../../../Gaudi/-/tags/v38r1p1),
Detector [v1r36](../../../../Detector/-/tags/v1r36) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to DaVinci [v64r12](/../../tags/v64r12), with mostly changes to upstream pacakges:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:
  - ~Functors | Fix Issue 589: make sure `Child` returns a reference, and not a copy of the child., Rec!4114 (@graven) [#589]
  - ~Functors | Fix SHARE_BPV functor to not compare addresses of two pointers, Rec!4107 (@mrudolph)
  - ~"Flavour tagging" | Bugfix: Fix the wrong sign of OSelectron tag decision, Rec!4108 (@mfuehrin)

### Other

- Update References for: LHCb!4722 based on lhcb-2024-patches-mr/1694, !1172 (@lhcbsoft)
