2024-04-09 DaVinci v64r3
===

This version uses
Moore [v55r6](../../../../Moore/-/tags/v55r6)
Allen [v4r6](../../../../Allen/-/tags/v4r6),
Rec [v36r6](../../../../Rec/-/tags/v36r6),
Lbcom [v35r6](../../../../Lbcom/-/tags/v35r6),
LHCb [v55r6](../../../../LHCb/-/tags/v55r6),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r30](../../../../Detector/-/tags/v1r30) and
LCG [105a](http://lcginfo.cern.ch/release/105a/).


This version is released on the `master` branch.
Built relative to DaVinci [v64r2](/../../tags/v64r2), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Fix for the merge of the genfsr, !1063 (@dfazzini)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Configuration ~Decoding ~Persistency | Follow-up from https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/4404, !1045 (@erodrigu)
- ~Luminosity | Review lumi job options, !1034 (@efranzos)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Follow https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/4496 (Remove data_type option), !1061 (@amathad)
- Test genfsr - add required missing option, !1058 (@erodrigu)
- TAU -> CTAU renaming, !1057 (@avilla)
- Following changes in Moore!2907, !1038 (@msaur)
- Use `os.remove(f)` instead of `os.system(f"rm {f}")`, !1043 (@graven)
- Upstream project highlights :star:


### Other

- ~Configuration ~Persistency | Follow up on particle PV pointer, offline PV unbiasing example, !1013 (@wouter) [Moore#608]
- ~Persistency | Adapt to newly persisted calo pid objects, !1050 (@mveghel)
- ~"MC checking" | Update HltEfficiencyChecker examples to use 2024 min bias samples, !1019 (@rjhunter) [#180]
- Update References for: LHCb!4457, Rec!3783, Boole!549 based on lhcb-master-mr/11193, !1060 (@lhcbsoft)
- Update References for: LHCb!4299, Rec!3665, Moore!2658, Alignment!445, DaVinci!1013 based on lhcb-master-mr/11111, !1055 (@lhcbsoft)
