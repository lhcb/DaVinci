2018-08-23 DaVinci v50r1
========================

Development release prepared on the master branch.

It is based on Gaudi v30r3, LHCb v50r1, Lbcom v30r1, Rec v30r1, Phys v30r1, Analysis v30r1 and Stripping v11r7,
and uses LCG_93 with ROOT 6.12.06.

- Cherry-pick MR !238 to master branch
  - See merge request lhcb/DaVinci!240
- Disable functor cache building for Tesla
  - See merge request lhcb/DaVinci!235
- Cherry-pick MR !219 to master branch
  - See merge request lhcb/DaVinci!221
- Test DaVinciTests/tests/options/gpython_algs.py updated to use TestFileDB
  - See merge request lhcb/DaVinci!218
- Cherry-pick MR !205 to master branch
  - See merge request lhcb/DaVinci!208
- Cherry-pick MR !206 to master branch
  - See merge request lhcb/DaVinci!207
- Cherry-pick MR !198 to master branch
  - See merge request lhcb/DaVinci!204
- Cherry-pick MR !201 to master branch
  - See merge request lhcb/DaVinci!202
- Cherry-pick MR !199 to master branch
  - See merge request lhcb/DaVinci!200
- Fix python error when using the DaVinci configurable with the Upgrade DataType
  - See merge request lhcb/DaVinci!195
- Cherry-pick MR !192 to master branch
  - See merge request lhcb/DaVinci!194
- Revert "Merge branch 'revert-df9b1ec2' into 'master'"
  - See merge request lhcb/DaVinci!187
