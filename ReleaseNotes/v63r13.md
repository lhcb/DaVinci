2023-09-11 DaVinci v63r13
===

This version uses Analysis [v41r13](../../../../Analysis/-/tags/v41r13),
Moore [v54r19](../../../../Moore/-/tags/v54r19),
Allen [v3r19](../../../../Allen/-/tags/v3r19),
Rec [v35r18](../../../../Rec/-/tags/v35r18),
Lbcom [v34r17](../../../../Lbcom/-/tags/v34r17),
LHCb [v54r17](../../../../LHCb/-/tags/v54r15),
Detector [v1r19](../../../../Detector/-/tags/v1r19),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `` branch.
Built relative to DaVinci [v63r12](/../../tags/v63r12), with the following changes:


### Enhancements ~enhancement

- ~Luminosity | Test service to write luminosity information to TTree when reading from FSRs, !937 (@egraveri)
- Example with spruced turbo, !961 (@pkoppenb)
