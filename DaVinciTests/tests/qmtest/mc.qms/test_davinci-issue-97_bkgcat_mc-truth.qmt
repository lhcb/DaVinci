<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="args"><set>
    <text>DaVinciTests.mc.option_davinci-issue-97_bkgcat_mc-truth:main</text>
    <text>--override-option-class=DaVinci.LbExec:TestOptions</text>
  </set></argument>
  <argument name="options_yaml_fn"><text>$DAVINCITESTSROOT/python/DaVinciTests/mc/option_davinci-issue-97_bkgcat_mc-truth.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    ntuple_file: davinci-issue-97_bkgcat_mc-truth_ntuple.root
    histo_file: davinci-issue-97_bkgcat_mc-truth_histos.root
    evt_max: 4
  </text></argument>
  <argument name="reference"><text>../refs/test_davinci-issue-97_bkgcat_mc-truth.ref</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor, remove_known_warnings
validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)

countErrorLines({"FATAL": 0, "ERROR": 0}, stdout=remove_known_warnings(stdout))

from pathlib import Path
from DaVinciTests.QMTest.check_helpers import get_pandas_dataframe, df_has_nan

filename = Path("./davinci-issue-97_bkgcat_mc-truth_ntuple.root")
ntuple = "Hlt2Lb2JpsiLambdaTuple/Tuple"

df = get_pandas_dataframe(filename.name, ntuple)

# Check ntuple structure
if df.empty:
    causes.append(f"File {filename}: ntuple does not contain any branches")
if df.shape != (7, 116):
    causes.append("Ntuple not with expected number of entries and/or branches")

# Check there are no NaN values in the ntuple except where expected
from DaVinciTests.QMTest.check_helpers import list_fields_with_nan
l_branches_with_nans = ['Lambda0_TRUEORIGINVX',
                        'Lambda0_TRUEORIGINVY',
                        'Lambda0_TRUEORIGINVZ',
                        'Lambda0_TRUEPT',
                        'Lb_TRUEORIGINVX',
                        'Lb_TRUEORIGINVY',
                        'Lb_TRUEORIGINVZ',
                        'Lb_TRUEPT'
                       ]
l_test = list_fields_with_nan(filename.name, ntuple)
if sorted(l_test) != sorted(l_branches_with_nans):
    causes.append("Unexpected list of branches with NaN values")

# Checks PIDs are correctly assigned
# Changes in the input data file or in the truth matching might cause changes in the reference values
# If the reason for changes is  understood, use the printed values to update the conditions below.
if not ( ( (df["mu_plus_TRUEID"].abs() == 13).sum() == 3 )
         and ( (df["mu_minus_TRUEID"].abs() == 13).sum() == 5 )
         and ( (df["pi_minus_TRUEID"].abs() == 211).sum() == 7 )
         and ( (df["Jpsi_TRUEID"].abs() == 443).sum() == 7 )
         and ( df["Lambda0_TRUEID"].to_list() == [0, 310, 0, 310, 0, 0, 3122] )
         and ( (df["Lb_TRUEID"] == 5122).sum() == 1 )
         and ( (df["p_plus_TRUEID"].abs() == 2212).sum() == 3 )
       ):

       print("mu_plus_TRUEID: ", (df["mu_plus_TRUEID"].abs() == 13).sum() )
       print("mu_minus_TRUEID: ", (df["mu_minus_TRUEID"].abs() == 13).sum() )
       print("pi_minus_TRUEID: ", (df["pi_minus_TRUEID"].abs() == 211).sum() )
       print("Jpsi_TRUEID: ", (df["Jpsi_TRUEID"].abs() == 443).sum() )
       print("Lambda0_TRUEID: ", df["Lambda0_TRUEID"].to_list()  )
       print("Lb_TRUEID: ", (df["Lb_TRUEID"] == 5122).sum()  )
       print("p_plus_TRUEID: ", (df["p_plus_TRUEID"].abs() == 2212).sum()  )
       causes.append("Ntuple contains unexpected TRUEID values")

# Check background categories
if not ( (df.filter(regex=("(p_|pi|mu).*BKGCAT"))==-1).all().all()  # all entries are -1 for final-state particles
         and (df["Jpsi_BKGCAT"]==0).all().all()  # all signal
         and ( (df["Lambda0_BKGCAT"]==0).sum() + (df["Lb_BKGCAT"]==0).sum() == 2 )  # single entry with both == 0
         and ( df["Lb_BKGCAT"].to_list() == [100, 100, 100, 100, 100, 110, 0] )
         and ( df["Lambda0_BKGCAT"].to_list() == [100, 30, 130, 30, 100, 120, 0] )
       ):

    print("Jpsi_BKGCAT: ", df["Jpsi_BKGCAT"].to_list() )
    print("Lambda0_BKGCAT: ", df["Lambda0_BKGCAT"].to_list()  )
    print("Lb_BKGCAT: ", df["Lb_BKGCAT"].to_list()  )
    causes.append("Ntuple contains unexpected BKGCAT values")

print('Test successfully completed!')
filename.unlink()
  </text></argument>
</extension>
