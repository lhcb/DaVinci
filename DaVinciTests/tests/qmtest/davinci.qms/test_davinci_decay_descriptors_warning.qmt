<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->

<!--
This is a test for Analysis issue 47 - ensure particles that are self-conjugated, marked with `[]CC` in a decay descriptor, will throw a warning when the descriptor is parsed by the tupling framework.
-->

<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="test_file_db_options_yaml"><text>mc_2024_sim10d_HLT2-2024.W31.34_Bd2KstMuMu_MagUp</text></argument>
  <argument name="args"><set>
    <text>DaVinciTests.option_analysis-issue-47_decay_descriptors:main</text>
    <text>--override-option-class=DaVinci.LbExec:TestOptions</text>
  </set></argument>
  <argument name="extra_options_yaml"><text>
    testfiledb_key: mc_2024_sim10d_HLT2-2024.W31.34_Bd2KstMuMu_MagUp
    evt_max: 10
    histo_file: DV-example-tupling-DTF-his.root
    ntuple_file: DV-example-tupling-DTF-ntp.root
    input_process: Hlt2
  </text></argument>
<argument name="validator"><text>
findReferenceBlock("""# WARNING: DimuonsTuple: Particles ['J/psi(1S)'] are marked with `[]CC` and self-conjugated. Are you sure this is really what you meant?""")
countErrorLines({"ERROR":0})
</text></argument>
</extension>
