<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2021-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
#######################################################
# SUMMARY OF THIS TEST
# ...................
# Author: masmith
# Purpose: Test to run a large DaVinci job for memory usage checks. For PrMon - not to be run regularly.
# Prerequisites: None
#######################################################
-->

<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="test_file_db_options_yaml"><text>collision_24_magup_spruce_24c4_data_rd_stream</text></argument>
  <argument name="extra_options_yaml"><text>
    testfiledb_key: collision_24_magup_spruce_24c4_data_rd_stream
    input_process: Spruce
    lumi: true
    input_stream: rd
    input_type: ROOT
    ntuple_file: 'dv_run_large_job.root'
    print_freq: 10000
    simulation: false
    dddb_tag: default
    conddb_tag: default
    evt_max: 100
  </text></argument>
  <argument name="args"><set>
  <text>DaVinciTests.dv_run_large_job_prtest:main</text>
  <text>--override-option-class=DaVinci.LbExec:TestOptions</text>
  </set></argument>
  <argument name="reference"><text>../refs/test_dv_run_large_job.ref</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor
validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)
countErrorLines({"FATAL":0, "ERROR":0})

from pathlib import Path
ntuple  = Path('./dv_run_large_job.root')

if not ntuple.is_file(): raise Exception(f"File {ntuple} does not exist!")
ntuple.unlink()
print('Test successfully completed')
    </text></argument>
</extension>
