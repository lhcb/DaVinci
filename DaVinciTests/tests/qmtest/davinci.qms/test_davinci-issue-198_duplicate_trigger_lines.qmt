<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="timeout"><integer>1000</integer></argument>
  <argument name="args"><set>
    <text>DaVinciTests.option_davinci-issue-198_duplicate_trigger_lines:main</text>
    <text>--override-option-class=DaVinci.LbExec:TestOptions</text>
 </set></argument>
  <argument name="options_yaml_fn"><text>$DAVINCITESTSROOT/tests/options/HltTisTos_test.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    histo_file: HltTisTos_histo.root
    ntuple_file: HltTisTos_ntuple.root
    print_freq: 1
  </text></argument>
  <argument name="reference"><text>../refs/test_davinci-issue-198_duplicate_trigger_lines.ref</text></argument>
  <argument name="error_reference"><text>../refs/test_davinci-issue-198_duplicate_trigger_lines_warnings.ref</text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor
validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)

from pathlib import Path
from ROOT import TFile

B_vars_stored  = ['RUNNUMBER', 'EVENTNUMBER']
B_vars_stored += [ 'Hlt2_TCK'
                 , 'Hlt2Topo2BodyDecision'
                 , 'Hlt2Topo3BodyDecision'
                 , 'Hlt1_TCK'
                 , 'Hlt1TrackMVADecision'
                 , 'Hlt1TwoTrackMVADecision'
                 , 'Jpsi_PT'
                 , 'Jpsi_Hlt2Topo2BodyDecision_TIS'
                 , 'Jpsi_Hlt2Topo3BodyDecision_TIS'
                 , 'Jpsi_Hlt2Topo2BodyDecision_TOS'
                 , 'Jpsi_Hlt2Topo3BodyDecision_TOS'
                 , 'Jpsi_Hlt1TrackMVADecision_TIS'
                 , 'Jpsi_Hlt1TrackMVADecision_TOS'
                 , 'Jpsi_Hlt1TwoTrackMVADecision_TIS'
                 , 'Jpsi_Hlt1TwoTrackMVADecision_TOS'
                 ]

#sort the expected vars
B_vars_stored = sorted(B_vars_stored)

#open the TFile and TTree
ntuple = Path('./HltTisTos_ntuple.root')
if not ntuple.is_file(): raise Exception(f"File: {ntuple} does not exist!")
f   = TFile.Open(ntuple.name)
t_B = f.Get('Tuple/DecayTree')

#sort the stores vars
b_names = sorted([b.GetName() for b in t_B.GetListOfLeaves()])

B_excluded_1 = set(B_vars_stored) - set(b_names)
B_excluded_2 = set(b_names) - set(B_vars_stored)
if len(B_excluded_1) != 0: raise Exception('Number of stored variables is less than what is expected. The extra variables expected are: ' , B_excluded_1)
if len(B_excluded_2) != 0: raise Exception('Number of stored variables is greater than what is expected. The extra variables stored are: ', B_excluded_2)

entry_num = 0
for entry in t_B:
    tos = entry.Jpsi_Hlt2Topo2BodyDecision_TOS
    tis = entry.Jpsi_Hlt2Topo2BodyDecision_TIS
    dec = entry.Hlt2Topo2BodyDecision
    #None of the candidates we ran are TOB i.e. (!tis and !tos and dec)
    # we check that (tis | tos) == dec
    tos_or_tis = tos or tis
    if tos_or_tis != dec:
        raise Exception(f'The entry # {entry_num} is perhaps TOB? i.e. (tis | tos) is {tos_or_tis} and decision is {dec}. However we did not run over a sample with TOB candidates. Please check!')
    entry_num += 1

f.Close()

countErrorLines({"FATAL":0, "ERROR":0})
  </text></argument>
</extension>
