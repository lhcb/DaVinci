###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for checking the correct processing of Spruce data including luminosity FSR.
rst_title: Tupling from Spruce data
rst_description: Test for checking the correct processing of Spruce data.
rst_running: lbexec DaVinciTests.option_davinci_tupling_with_lumi_from_spruce:main $DAVINCITESTSROOT/tests/options/Spruce_with_lumi_all_lines_dst.yaml
rst_yaml: ../DaVinciTests/tests/Spruce_with_lumi_all_lines_dst.yaml
"""
from PyConf.reading import get_particles
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config

import numpy as np
import ROOT
import json


def main(options: Options):
    line_B0Dspi = "SpruceB2OC_BdToDsmPi_DsmToKpKmPim"
    bd2dspi_line = get_particles(f"/Event/Spruce/{line_B0Dspi}/Particles")

    fields_dspi = {
        "B0": "[B0 -> D_s- pi+]CC",
        "Ds": "[B0 -> ^D_s- pi+]CC",
        "pip": "[B0 -> D_s- ^pi+]CC",
    }

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        "ALL": variables_all,  # adds variables to all fields
    }

    tuple_B0Dspi = Funtuple(
        name="B0Dspi_Tuple",
        tuple_name="DecayTree",
        fields=fields_dspi,
        variables=variables,
        inputs=bd2dspi_line,
    )

    filter_B0Dspi = create_lines_filter(
        name="HDRFilter_B0Dspi", lines=[f"{line_B0Dspi}"]
    )

    algs = {
        "B0Dspi": [filter_B0Dspi, tuple_B0Dspi],
    }

    return make_config(options, algs)


def check_ttree_data(dst_file, root_file):
    # Open the ROOT file and get the TTree
    root_file = ROOT.TFile.Open(root_file)
    # if not os.path.isfile(root_file):
    if root_file.IsZombie():
        raise Exception("Error in opening ROOT File")

    tree = root_file.Get("lumiTree")

    # Get the expected values from the JSON data
    expected_values = {}

    # Load the JSON data
    # if not os.path.isfile(dst_file):
    for idst in dst_file:
        f = ROOT.TFile.Open(idst)
        if f.IsZombie():
            raise Exception("Error in opening DST File")

        json_data = json.loads(str(f.FileSummaryRecord))
        if "inputs" in json_data.keys():
            runs = np.array(
                [
                    list(ientry["LumiCounter.eventsByRun"]["counts"].keys())[0]
                    for ientry in json_data["inputs"]
                ]
            )
            runs = np.unique(runs)
            run_dict = {irun: 0 for irun in runs}
            for ientry in json_data["inputs"]:
                for irun, icounts in ientry["LumiCounter.eventsByRun"][
                    "counts"
                ].items():
                    run_dict[irun] += icounts
            input_json = {"LumiCounter.eventsByRun": {"counts": run_dict}}
        else:
            input_json = json_data

        for key, value in input_json["LumiCounter.eventsByRun"]["counts"].items():
            if int(key) in expected_values.keys():
                expected_values[int(key)] += value
            else:
                expected_values[int(key)] = value
            print(
                f"//////////  DST  :   RUN     {key}   -     nlumiEv     {expected_values[int(key)]}"
            )

    # Check if the TTree data matches the expected values
    assert tree.GetEntries() > 0

    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        key = tree.runNumber
        sum_value = tree.sumLumievents
        print(f"**********  ROOT :   RUN     {key}   -     nlumiEv     {sum_value}")
        if key not in expected_values or sum_value != expected_values[key]:
            return False

    return True


def validated_tree(FILENAMESDT, FILENAMEROOT):
    test_content = check_ttree_data(FILENAMESDT, FILENAMEROOT)
    if not test_content:
        print("Problem with the TTree content: does not match FSR JSON content")
        raise Exception(
            "Problem with the TTree content: does not match FSR JSON content"
        )
        return False
    else:
        print("CHECK VALIDATED")
    return True
