###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Option file for testing the correct DaVinci configuration for Turbo with persistreco = True.
"""
from PyConf.Algorithms import ParticleRangeFilter, TwoBodyCombiner
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from RecoConf.standard_particles import make_long_kaons, make_long_pions
from RecoConf.reconstruction_objects import make_pvs


def make_Kst2Kpi():
    kaons = make_long_kaons()
    pions = make_long_pions()
    pvs = make_pvs()
    descriptor = "K*(892)0 -> K+ pi-"
    filtered_kaons = ParticleRangeFilter(Input=kaons, Cut=F.FILTER(F.PROBNN_K > 0.5))
    filtered_pions = ParticleRangeFilter(Input=pions, Cut=F.FILTER(F.PROBNN_K < 0.5))

    combination_code = in_range(872 * MeV, F.MASS, 912 * MeV)
    vertex_code = F.CHI2DOF < 30

    p = TwoBodyCombiner(
        name="TightKst2Kpi",
        Input1=filtered_kaons,
        Input2=filtered_pions,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        ParticleCombiner="ParticleVertexFitter",
        CompositeCut=vertex_code,
        PrimaryVertices=pvs,
    )

    variables = FunctorCollection({"PX": F.PX, "PY": F.PY, "PZ": F.PZ})
    kst_fields = {"Kst": "K*(892)0 -> K+ pi-", "Kp": "K*(892)0 -> ^K+ pi-"}
    kst_vars = {"Kst": variables, "Kp": variables}

    ntuple = Funtuple(
        name="TupleKst",
        tuple_name="DecayTree",
        fields=kst_fields,
        variables=kst_vars,
        inputs=p.OutputParticles,
    )
    return ntuple


def main(options: Options):
    line_name = "Hlt2RD_BdToKstGamma"

    filter_RD = create_lines_filter(name="HDRFilter_RD", lines=[f"{line_name}"])
    kst2kpi = make_Kst2Kpi()

    return make_config(options, [filter_RD, kst2kpi])
