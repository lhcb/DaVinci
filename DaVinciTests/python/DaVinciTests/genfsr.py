###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import GenFSRLog
from DaVinci import Options, make_config


def main(options: Options):
    # Change the alg name to ensure the code is finalized after all the others,
    # see discussion at https://gitlab.cern.ch/lhcb/DaVinci/-/merge_requests/1058#note_7795091.
    return make_config(options, {"FSRLogNode": [GenFSRLog(name="ZGenFSRLog")]})
