###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This options file is used in the LHCb integration test: Tupling_FT.TupleFromHLT2.
The test aims to determine if the flavour tagging variables are available in the ntuples
after processing an .xdigi file of simulated Bd2DPi decays.
The processing consists of an HLT1 job and afterwards an HLT2 job with an exclusive Bd2DPi
line that a .dst file as well as extra outputs necessary for the flavour tagging.

This options file reads the output of the Moore HLT2 job and produces an nTuple with Flavour Tagging variables.

Based on: https://gitlab.cern.ch/lhcb/DaVinci/-/blob/v63r2/DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_tupling_from_hlt2.py

"""

import Functors as F
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config
from DaVinciMCTools import MCTruthAndBkgCat
from RecoConf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.flavourTagging import run2_all_taggers


def main(options: Options):
    line_name = "Hlt2B2OC_BdToDmPi_DmToPimPimKp"
    line_data = get_particles(f"/Event/HLT2/{line_name}/Particles")
    my_filter = create_lines_filter(name="HDRFilter", lines=[f"{line_name}"])

    fields = {
        "B": "[[B0]CC -> (D- -> K+ pi- pi-) pi+]CC",
        "h_comp": "[[B0]CC -> (D- -> K+ pi- pi-) ^pi+]CC",
        "D": "[[B0]CC -> ^(D- -> K+ pi- pi-) pi+]CC",
        "D_child_1": "[[B0]CC -> (D- -> ^K+ pi- pi-) pi+]CC",
        "D_child_2": "[[B0]CC -> (D- -> K+ ^pi- pi-) pi+]CC",
        "D_child_3": "[[B0]CC -> (D- -> K+ pi- ^pi-) pi+]CC",
    }

    # Creating v2 reconstructed vertices to be used in the following functor
    taggingPions = make_has_rich_long_pions()
    MCTRUTHTAG = MCTruthAndBkgCat(taggingPions, name="MCTruthAndBkgCatTag")
    MCTRUTH = MCTruthAndBkgCat(line_data, name="MCTruthAndBkgCat")

    b_composite_variables = FunctorCollection()
    b_composite_variables["ID"] = F.PARTICLE_ID

    tag_particle = F.FORWARDARG0
    B_particle = F.FORWARDARG1
    true_tag_particle = F.MAP_TO_RELATED(MCTRUTHTAG.MCAssocTable) @ tag_particle
    true_B_particle = F.MAP_TO_RELATED(MCTRUTH.MCAssocTable) @ B_particle
    origin_flag = F.MC_ORIGINFLAG.bind(true_tag_particle, true_B_particle)
    array_origin_flag = F.MAP(F.VALUE_OR(-1) @ origin_flag).bind(
        F.TES(taggingPions), F.FORWARDARGS
    )
    b_composite_variables["Origin_Flag"] = array_origin_flag

    all_run2_taggers = run2_all_taggers(line_data)
    b_composite_variables = b_composite_variables + FC.FlavourTaggingResults(
        all_run2_taggers
    )

    variables = {
        "B": b_composite_variables,
    }

    # define FunTuple instance
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=line_data,
    )

    return make_config(options, [my_filter, my_tuple])
