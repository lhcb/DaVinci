###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read the output of an Sprucing job with the new DaVinci configuration.
"""
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config
from PyConf.reading import get_particles


def main(options: Options):
    process = options.input_process

    if process == "Spruce":
        line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST"
        prefix = "Spruce"
        b_name = "B0"
        daug1_name = "D_s-"
        daug2_name = "K+"
        daug3_name = ""
    else:
        line = "Hlt2Charm_D0ToKsPimPip_LL"
        prefix = "HLT2"
        b_name = "D0"
        daug1_name = "KS0"
        daug2_name = "pi-"
        daug3_name = "pi+"

    data = get_particles(f"/Event/{prefix}/{line}/Particles")
    fields = {
        "B": f"[{b_name} -> {daug1_name} {daug2_name} {daug3_name}]CC",
        "daug1": f"[{b_name} -> ^{daug1_name} {daug2_name} {daug3_name}]CC",
        "daug2": f"[{b_name} -> {daug1_name} ^{daug2_name} {daug3_name}]CC",
    }

    variables_b = FunctorCollection(
        {
            "LOKI_daug1_PT": F.CHILD(1, F.PT),
            "LOKI_daug2_PT": F.CHILD(2, F.PT),
            "daug1_PID_K": F.CHILD(1, F.PID_K),
            "daug2_PROBNN_K": F.CHILD(2, F.PROBNN_K),
        }
    )

    variables_extra = FunctorCollection(
        {"LOKI_NTRCKS_ABV_THRSHLD": "NINTREE(ISBASIC & (PT > 15*MeV))"}
    )
    variables_b += variables_extra

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        "ALL": variables_all,  # adds variables to all fields
        "B": variables_b,
        "daug1": variables_extra,
        "daug2": variables_extra,
    }

    tuple_dv = Funtuple(
        name=f"Tuple_{process.lower()}",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=data,
    )

    filter_dv = create_lines_filter("HDRFilter_DV", lines=[f"{line}"])
    algs = [filter_dv, tuple_dv]

    return make_config(options, algs)
