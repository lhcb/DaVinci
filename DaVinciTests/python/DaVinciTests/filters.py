###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read the output of a Sprucing job in DaVinci with line specific filters.
"""
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    evt_prefilter = create_lines_filter(
        "Hlt2TopoLineFilter", lines=["Hlt2Topo2BodyLineDecision"]
    )
    filter_B0DsK = create_lines_filter(
        "HDRFilter_B0DsK", lines=["SpruceB2OC_BdToDsmK_DsmToHHH_FEST"]
    )
    filter_B0Dspi = create_lines_filter(
        "HDRFilter_B0Dspi", lines=["SpruceB2OC_BdToDsmPi_DsmToKpKmPim"]
    )

    algs = {
        "B0DsK": [evt_prefilter, filter_B0DsK],
        "B0Dspi": [evt_prefilter, filter_B0Dspi],
    }
    return make_config(options, algs)
