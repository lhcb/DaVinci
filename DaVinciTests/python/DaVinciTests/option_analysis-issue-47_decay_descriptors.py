###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: Test for Analysis issue 47
rst_description: Test for Analysis issue 47 - ensure particles that are self-conjugated, marked with `[]CC` in a decay descriptor, will throw a warning when the descriptor is parsed by the tupling framework.

rst_running: lbexec DaVinciTests.option_analysis-issue-47_decay_descriptors:main "$DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml"
rst_yaml: ../../../DaVinciExamples/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml
"""
import Functors as F
from RecoConf.standard_particles import make_detached_mumu
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()

    # FunTuple: Jpsi info
    fields = {}
    fields["Jpsi"] = "[J/psi(1S) -> mu+ mu-]CC"

    # make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "THOR_MASS": F.MASS,
        }
    )

    # associate FunctorCollection to field (branch) name
    variables = {}
    variables["Jpsi"] = variables_jpsi

    # Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=dimuons,
    )

    return make_config(options, [tuple_dimuons])
