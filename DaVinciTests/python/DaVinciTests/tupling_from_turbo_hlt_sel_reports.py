###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Option file for testing the known problem with HLT1 sel reports.
"""
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles
from FunTuple.functorcollections import HltTisTos
from DaVinci.algorithms import create_lines_filter


def template(decay_descriptor, line_name):

    evtpath_prefix = "/Event/HLT2/"

    QQbar2mumu_data = get_particles(evtpath_prefix + f"{line_name}/Particles")

    Hlt1_decisions = ["Hlt1TrackMVADecision"]

    variables = {
        "QQbar": HltTisTos(
            selection_type="Hlt1", trigger_lines=Hlt1_decisions, data=QQbar2mumu_data
        )
    }

    # define FunTuple instance
    my_tuple = Funtuple(
        name=line_name,
        tuple_name="DecayTree",
        fields=decay_descriptor,
        variables=variables,
        inputs=QQbar2mumu_data,
    )

    return my_tuple


def line_prefilter(line_name):
    return create_lines_filter(name=f"HLT_PASS{line_name}", lines=[line_name])


from DaVinci import Options, make_config


def main(options: Options):
    decay_descriptor = {
        "QQbar": "J/psi(1S) -> mu+ mu-",
    }
    line_name = "Hlt2_JpsiToMuMu"
    my_tuple = template(decay_descriptor, line_name)
    my_filter = line_prefilter(line_name)
    return make_config(options, [my_filter, my_tuple])
