###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This set of options file is used in the LHCb integration test
'Tuple_track_refitting'. The goal of this option is to take
the output created by the sprucing step, get the particles
that were persisted, and refit the tracks associated to them.

It also adds test machinery that looks at the differences
due to this track refitting, and plots quantities related
to the different states of the tracks.

For information regarding the creation of this options' input,
please see the LHCb Integration Test.
"""
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from RecoConf.standard_particles import make_long_pions

from PyConf.reading import get_pvs

from PyConf.Algorithms import FunctionalChargedProtoParticleMaker
from PyConf.Algorithms import TrackCorrelationsMonitor, TrackMonitor
from PyConf.Algorithms import SelectTracksForParticles
from PyConf.Algorithms import LHCbIDOverlapRelationTable
from PyConf.Algorithms import FunctionalParticleMaker

from RecoConf import track_refitting
from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt2_tracking import make_PrStoreSciFiHits_hits

import Functors as F

from functools import partial


def make_protos(tracks):
    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[tracks])

    return charged_protos.Output


# See Moore!3208
def _make_particles(species, make_protoparticles, get_pvs=get_pvs):
    long_protos = make_protoparticles()

    return FunctionalParticleMaker(
        InputProtoParticles=long_protos,
        ParticleID=species,
        TrackPredicate=F.ALL,
        ProtoParticlePredicate=F.ALL,
        PrimaryVertices=get_pvs(),
    ).Particles


def make_muons(make_protos_function):
    """creates LHCb::Particles from LHCb::ProtoParticles of tracks"""
    return _make_particles(
        species="muon",
        make_protoparticles=make_protos_function,
    )


make_digits.global_bind(calo_raw_bank=False)


def main(options: Options):

    B_Line = "Spruce_Test_line_persistreco"
    my_filter = create_lines_filter(name="HDRFilter_Sprucingline", lines=[B_Line])

    # get all the particles from persist reco
    my_persist_reco_pions = make_long_pions()

    tracks_from_particles_output = SelectTracksForParticles(
        Inputs=[my_persist_reco_pions]
    )
    tracks_from_particles = tracks_from_particles_output.OutputTracks

    with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"):
        tracks_from_dst = track_refitting.refit_tracks(
            tracks_from_particles,
            True,
            track_fitter=track_refitting.TRACK_FIT_TYPE_PRKALMAN,
        )
        tracks_from_raw = track_refitting.refit_tracks(
            tracks_from_particles,
            False,
            track_fitter=track_refitting.TRACK_FIT_TYPE_PRKALMAN,
        )

    muons = make_muons(partial(make_protos, tracks_from_raw))  # original
    new_muons = make_muons(partial(make_protos, tracks_from_dst))

    scifihits = make_PrStoreSciFiHits_hits()

    # the refitted tracks
    corr_plots_refi = TrackCorrelationsMonitor(
        TracksInContainer=tracks_from_dst,
        SciFiHits=scifihits,
        name="RefittedTrackCorrelationsMonitor",
    )
    resolution_plots_refi = TrackMonitor(
        TracksInContainer=tracks_from_dst, name="RefittedTrackMonitor"
    )

    # the original tracks
    corr_plots_orig = TrackMonitor(
        TracksInContainer=tracks_from_particles, name="TrackMonitor"
    )
    resolution_plots_orig = TrackCorrelationsMonitor(
        TracksInContainer=tracks_from_particles,
        SciFiHits=scifihits,
        name="TrackCorrelationsMonitor",
    )

    pvs = get_pvs()
    refitted_pvs = track_refitting.refit_pvs(input_pvs=pvs).OutputVertices

    relation_table_match_by_lhcbid = LHCbIDOverlapRelationTable(
        MatchFrom=muons,
        MatchTo=new_muons,
        IncludeVP=True,
        IncludeFT=True,
        IncludeUT=True,
    ).OutputRelations

    import Functors as F

    from FunTuple import FunctorCollection
    from FunTuple import FunTuple_Particles as Funtuple

    variables = {
        "muminus": FunctorCollection(
            {
                "P": F.P,
                "PT": F.PT,
                "CHI2DOF": F.CHI2DOF,
                "ETA": F.ETA,
                "PHI": F.PHI,
                "StatePosition_": F.POSITION @ F.FRONT @ F.STATES @ F.TRACK,
                "Refitted_CHI2DOF[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.CHI2DOF, Relations=relation_table_match_by_lhcbid
                ),
                "Refitted_P[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.P, Relations=relation_table_match_by_lhcbid
                ),
                "Refitted_PT[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.PT, Relations=relation_table_match_by_lhcbid
                ),
                "Refitted_IPCHI2_OLDPV[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.BPVIPCHI2(pvs), Relations=relation_table_match_by_lhcbid
                ),
                "Refitted_IPCHI2_NEWPV[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.BPVIPCHI2(refitted_pvs),
                    Relations=relation_table_match_by_lhcbid,
                ),
                "Refitted_WEIGHT[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_WEIGHT(Relations=relation_table_match_by_lhcbid),
                "Refitted_ETA[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.ETA, Relations=relation_table_match_by_lhcbid
                ),
                "Refitted_PHI[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=F.PHI,
                    Relations=relation_table_match_by_lhcbid,
                ),
                "Refitted_StatePosition_X[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=(
                        F.VALUE_OR(-999)
                        @ F.X_COORDINATE
                        @ F.POSITION
                        @ F.FRONT
                        @ F.STATES
                        @ F.TRACK
                    ),
                    Relations=relation_table_match_by_lhcbid,
                ),
                "Refitted_StatePosition_Y[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=(
                        F.VALUE_OR(-999)
                        @ F.Y_COORDINATE
                        @ F.POSITION
                        @ F.FRONT
                        @ F.STATES
                        @ F.TRACK
                    ),
                    Relations=relation_table_match_by_lhcbid,
                ),
                "Refitted_StatePosition_Z[nMatchTracks]": F.REVERSE_RANGE
                @ F.MAP_INPUT_ARRAY(
                    Functor=(
                        F.VALUE_OR(-999)
                        @ F.Z_COORDINATE
                        @ F.POSITION
                        @ F.FRONT
                        @ F.STATES
                        @ F.TRACK
                    ),
                    Relations=relation_table_match_by_lhcbid,
                ),
            }
        )
    }

    fields = {
        "muminus": "[mu-]CC",
    }

    # define FunTuple instance
    my_tuple = Funtuple(
        name="DecayTreeTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables={},
        inputs=muons,
    )

    # Run
    algs = {
        "refit_tracks": [
            my_filter,
            tracks_from_dst,
            relation_table_match_by_lhcbid,
            corr_plots_refi,
            corr_plots_orig,
            resolution_plots_refi,
            resolution_plots_orig,
            my_tuple,
        ],
    }

    return make_config(options, algs)
