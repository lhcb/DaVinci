###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 Option file for testing running the flavour tagging (via the options setup in Moore) and the tupling of the results
 This example is meant to be run with:

    $ ./run lbexec flavour_tagging_with_typling:main DaVinciTests/tests/options/Spruce_passthrough_B2Dpi_line.yaml
"""

from PyConf.reading import get_particles
from Hlt2Conf.flavourTagging import run2_all_taggers
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import functorcollections as FC
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):

    bd2dpi_line = "Hlt2B2OC_BdToDmPi_DmToPimPimKp"
    bd2dpi_data = get_particles(f"/Event/HLT2/{bd2dpi_line}/Particles")

    # make collection of functors
    variables_B = FunctorCollection(
        {"THOR_MASS": F.MASS, "ID": F.PARTICLE_ID, "PT": F.PT}
    )

    all_tagging = run2_all_taggers(bd2dpi_data)

    # Defining fields and variables we want to store in our tuple
    fields = {"B0": "[[B0]CC -> (D- -> K+ pi- pi-) pi+]CC"}
    variables = {"B0": variables_B + FC.FlavourTaggingResults(all_tagging)}

    tuple_B02Dpi = Funtuple(
        name="B02Dpi_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=bd2dpi_data,
    )

    filter_B02Dpi = create_lines_filter("HDRFilter_B02Dpi", lines=[f"{bd2dpi_line}"])

    return make_config(options, [filter_B02Dpi, tuple_B02Dpi])
