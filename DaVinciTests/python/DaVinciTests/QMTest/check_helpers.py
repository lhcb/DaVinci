###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from collections.abc import Iterable
import pandas as pd
from ROOT import RDataFrame
import math


def has_nan(
    filename: str,
    ntuple_name: str,
    columns: Iterable[str] = None,
    exclude: Iterable[str] = None,
):
    """
    Check if the ntuple contains NaN values.

    Ntuple fields (branches) can be explicitly given and/or excluded
    with the `columns` and `exclude` arguments, respectively,
    as detailed in the documentation of `RDataFrame.AsNumpy`.

    Args:
        filename (str): The full path to the ROOT file.
        ntuple_name (str): The full path to the ntuple, e.g. My/Dir/MyNtuple.
        columns (iterable[str], optional): If None, return all fields (branches) as columns,
            otherwise specify names in iterable.
        exclude (iterable[str], optional): Exclude fields (branches) from selection.
    """
    df = get_pandas_dataframe(filename, ntuple_name, columns, exclude)

    return df.isna().any().any()


def df_has_nan(df: pd.DataFrame):
    """
    Check if a Pandas DataFrame contains NaN values.
    Typically used after a call to `get_pandas_dataframe`.
    """
    return df.isna().any().any()


def count_nan_in_fields(filename: str, ntuple_name: str, fields: Iterable[str]):
    """
    Count the number of NaN values for a give list of fields (branches).

    Args:
        filename (str): The full path to the ROOT file.
        ntuple_name (str): The full path to the ntuple, e.g. My/Dir/MyNtuple.
        fields (iterable[str]): The list of fields to inspect.
    """
    df = get_pandas_dataframe(filename, ntuple_name, fields)

    return {f: df[f].isna().sum() for f in fields}


def list_fields_with_nan(filename: str, ntuple_name: str):
    """
    List the fields (branches) that contain NaN values.

    Args:
        filename (str): The full path to the ROOT file.
        ntuple_name (str): The full path to the ntuple, e.g. My/Dir/MyNtuple.
    """
    df = get_pandas_dataframe(filename, ntuple_name)

    return df.columns[df.isna().any()].tolist()


def get_pandas_dataframe(
    filename: str,
    ntuple_name: str,
    columns: Iterable[str] = None,
    exclude: Iterable[str] = None,
):
    """
    Helper to produce a Pandas DataFrame from a ROOT RDataFrame.

    Args:
        filename (str): The full path to the ROOT file.
        ntuple_name (str): The full path to the ntuple, e.g. My/Dir/MyNtuple.
        columns (iterable[str], optional): If None, return all fields (branches) as columns,
            otherwise specify names in iterable.
        exclude (iterable[str], optional): Exclude fields (branches) from selection.
    """
    rdf = RDataFrame(ntuple_name, filename)
    ar = rdf.AsNumpy(columns, exclude)

    return pd.DataFrame(ar)


def round_to_significant(num, significant_digits: int = 4):
    """
    Round a float to the specified number of significant digits.

    Args:
        num (float or int): The float to round.
        significant_digits (int): The number of significant digits to round to. Defaults to 4.

    Returns:
        The rounded float.

    Examples:
        >>> round_to_significant(0.000123456789, 3)
        0.000123
    """
    return round(num, -int(math.floor(math.log10(abs(num)))) + (significant_digits - 1))


def get_hash_from_branch(
    filename: str, ntuple_name: str, branch_name: str, significant_digits: int = 4
):
    """
    Helper function for calculating the hash value of a branch within a TTree in a ROOT file.

    Args:
        filename (str): The full path to the ROOT file.
        ntuple_name (str): The full path to the ntuple, e.g. My/Dir/MyNtuple.
        branch_name (str): The name of the branch to hash.
        significant_digits (int, optional): The number of significant digits to round to. Defaults to 4.

    Returns:
        The hash value of the rounded branch values.

    Examples:
        >>> get_hash_from_branch('myfile.root', 'My/Dir/MyNtuple', 'MyBranch', 3)
        123456789
    """
    # Load the ROOT file and retrieve the branch values as a list
    rdf = RDataFrame(ntuple_name, filename)
    values = rdf.AsNumpy([branch_name])[branch_name].tolist()

    # Round the branch values to the specified number of significant digits
    rounded = tuple([round_to_significant(val, significant_digits) for val in values])

    # Compute and return the hash of the rounded values
    return hash(rounded)
