###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: Test for DaVinci issue 198
rst_description: Test for DaVinci issue 198.
When the list of trigger lines passed to HltTisTos contains duplicates, the number of lines can be counted in 2 different ways.
For Hlt1 the crash occurs in Rec/Phys/TisTosTobbing/src/HltTisTosAlg.cpp:246.
For Hlt2 the crash occurs in Rec/Phys/TisTosTobbing/src/HltTisTosAlg.cpp:378.
Both of these crashes are solved by removing the duplicate trigger lines in functor collections.
rst_running: lbexec DaVinciTests.option_davinci-issue-198_duplicate_trigger_lines:main "$DAVINCITESTSROOT/tests/options/test_tistos.yaml"
rst_yaml: $DAVINCITESTSROOT/tests/options/test_tistos.yaml
"""

from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter


def main(options: Options):
    # specify line name to get data and define a filter for that line.
    line_name = "SpruceBandQ_JpsiToMuMuDetached"
    data = get_particles(f"/Event/Spruce/{line_name}/Particles")
    my_filter = create_lines_filter(name="HDRFilter_test", lines=[f"{line_name}"])

    # define fields and variables for candidates in the Hlt lines.
    fields = {"Jpsi": "J/psi(1S) -> mu+ mu-"}
    variables_all = FunctorCollection({"PT": F.PT})

    # Test for issue 198.
    # Crashes can occur with both Hlt1 and Hlt2 if duplicate trigger lines are passed to HltTisTos.
    Hlt1_decisions = [
        "Hlt1TrackMVADecision",
        "Hlt1TrackMVADecision",
        "Hlt1TwoTrackMVADecision",
    ]
    Hlt2_decisions = ["Hlt2Topo2Body", "Hlt2Topo2Body", "Hlt2Topo3Body"]

    variables_all += FC.HltTisTos(
        selection_type="Hlt1", trigger_lines=Hlt1_decisions, data=data
    )
    variables_all += FC.HltTisTos(
        selection_type="Hlt2", trigger_lines=Hlt2_decisions, data=data
    )
    variables = {"ALL": variables_all}

    evt_variables = FC.SelectionInfo(
        selection_type="Hlt1", trigger_lines=Hlt1_decisions
    )
    evt_variables += FC.SelectionInfo(
        selection_type="Hlt2", trigger_lines=Hlt2_decisions
    )

    # define funtuple instance
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=data,
    )

    return make_config(options, [my_filter, my_tuple])
