###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read the output of an HLT2 job and create an ntuple for LHCbIntegrationTests/Tupling_neutrals tests
"""
from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
import FunTuple.functorcollections as FC
from DaVinciMCTools import MCTruthAndBkgCat


def main(options: Options):
    # Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> J/psi(1S) gamma",
        "Jpsi": "B_s0 -> ^J/psi(1S) gamma",
        "gamma": "B_s0 -> J/psi(1S) ^gamma",
    }

    # Load data from dst onto a TES
    hlt2_line = "Hlt2RD_BToEEGamma"
    input_data = get_particles(f"/Event/HLT2/{hlt2_line}/Particles")
    MCTRUTH = MCTruthAndBkgCat(input_data, name="MCTruthAndBkgCat_tuto")

    kin = FC.Kinematics()
    mckin = FC.MCKinematics(mctruth_alg=MCTRUTH)
    mchierarchy = FC.MCHierarchy(mctruth_alg=MCTRUTH)

    extra_info = {
        "ISPHOTON": F.IS_PHOTON,
        "ISNOTH": F.IS_NOT_H,
        "CaloTrackMatchChi2": F.CLUSTERMATCH_CHI2,
        "CaloNeutralShowerShape": F.CALO_NEUTRAL_SHOWER_SHAPE,
        "CaloClusterMass": F.CALO_CLUSTER_MASS,
        "CaloNeutralEcalEnergy": F.CALO_NEUTRAL_ECAL_ENERGY,
        "CaloNeutral1To9EnergyRatio": F.CALO_NEUTRAL_1TO9_ENERGY_RATIO,
        "CaloNeutral4To9EnergyRatio": F.CALO_NEUTRAL_4TO9_ENERGY_RATIO,
        "CaloNeutralHcal2EcalEnergyRatio": F.CALO_NEUTRAL_HCAL2ECAL_ENERGY_RATIO,
        "CaloNeutralID": F.CALO_NEUTRAL_ID,
        "CaloNumSaturatedCells": F.CALO_NUM_SATURATED_CELLS,
    }
    extra_info = FunctorCollection(extra_info)

    bkg_cat = FunctorCollection({"BKGCAT": MCTRUTH.BkgCat})

    variables = {"ALL": kin + mckin + mchierarchy + bkg_cat, "gamma": extra_info}

    my_filter = create_lines_filter("HDRFilter_SeeNoEvil", lines=[f"{hlt2_line}"])

    mytuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=input_data,
    )

    config = make_config(options, [my_filter, mytuple])
    return config
