Options YAML
============

The YAML provided to populate the ``options`` object passed to the user provided function, often called ``options.yaml``, is parsed using the following model:

.. autopydantic_model:: DaVinci.Options
   :noindex:
   :inherited-members: BaseModel
   :model-show-field-summary: False

.. autoclass:: GaudiConf.LbExec.options.DataTypeEnum
   :noindex:
   :members:
   :undoc-members:

.. autoclass:: GaudiConf.LbExec.options.FileFormats
   :noindex:
   :members:
   :undoc-members:

.. autoclass:: GaudiConf.LbExec.options.EventStores
   :noindex:
   :members:
   :undoc-members:
