Documenting DaVinci
===================

This documentation is written in `reStructuredText`_ and built in to web pages by `Sphinx`_.
The source can be found in the `doc`_ directory of the DaVinci project.

We follow the `Google style guide`_ (`example`_) when writing `docstrings`_ in Python.

Building the documentation locally
----------------------------------

After building ``DaVinci``, to get the documentation pages to build and be written to ``DaVinci/doc/_build/html/``, run the following commands:

.. code-block:: sh

 DaVinci/run make -C DaVinci/doc html
 DaVinci/run make -C DaVinci/doc linkcheck

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _doc: https://gitlab.cern.ch/lhcb/DaVinci/-/tree/master/doc
.. _Google style guide: https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
.. _docstrings: https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring
.. _example: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
