<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
#######################################################
# SUMMARY OF THIS TEST
# ...................
# Author: pkoppenb
# Purpose: Test for All functors
# Prerequisites: None
#######################################################
-->

<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <!-- Minimum bias dst processed using topo {2,3} hlt2 lines and all sprucing lines -->
  <argument name="extra_options_yaml"><text>
    input_files:
      - root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/spruce_all_lines_realtimereco_newPacking_newDst.dst
    input_manifest_file: 'root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/spruce_all_lines_realtime_newPacking_newDst.tck.json'
    input_type: ROOT
    simulation: true
    conddb_tag: sim-20171127-vc-md100
    dddb_tag: dddb-20171126
    conditions_version: master
    geometry_version: run3/trunk
    input_raw_format: 0.3
    persistreco_version: 0.0
    lumi: false
    ntuple_file: 'DV_example_allFunctors_ntp.root'
    print_freq: 1
    input_process: Spruce
    input_stream: default
  </text></argument>
  <argument name="timeout"><integer>1000</integer></argument>
  <argument name="args"><set>
  <text>DaVinciExamples.tupling.AllFunctors:alg_config</text>
  </set></argument>
<argument name="validator"><text>
from PyConf.components import findRootObjByDir

findReferenceBlock("""B0DsK_Tuple                         SUCCESS Booked 1 N-Tuples and 0 Event Tag Collections"""
, stdout, result, causes, signature_offset = 0)

countErrorLines({"FATAL":0, "ERROR":20})

from pathlib import Path
from ROOT import TFile

B_vars_stored  = ['B0_ISBASIC', 'B0_ABS_PX', 'B0_ALLPVX', 'B0_ALLPVY', 'B0_ALLPVZ', 'B0_ALLPV_FD', 'B0_ALLPV_IP', 'B0_ALV', 'B0_BKGCAT', 'B0_BPVCORRM', 'B0_BPVCORRMERR', 'B0_BPVDIRA', 'B0_BPVDLS', 'B0_BPVETA', 'B0_BPVFD', 'B0_BPVFDCHI2', 'B0_BPVFDIRX', 'B0_BPVFDIRY', 'B0_BPVFDIRZ', 'B0_BPVFDVECX', 'B0_BPVFDVECY', 'B0_BPVFDVECZ', 'B0_BPVIP', 'B0_BPVIPCHI2', 'B0_BPVLTIME', 'B0_BPVVDRHO', 'B0_BPVVDX', 'B0_BPVVDY', 'B0_BPVVDZ', 'B0_BPVX', 'B0_BPVY', 'B0_BPVZ', 'B0_CHARGE', 'B0_CHI2', 'B0_CHI2DOF', 'B0_CHILD1_PT', 'B0_DOCA', 'B0_DOCACHI2', 'B0_DTF_BPVIPCHI2', 'B0_DTF_MASS_BiggestDELTAPT', 'B0_DTF_MASS_SmallestDELTAPT', 'B0_DTF_PT', 'B0_Delta_END_VZ_DsB0', 'B0_Ds_END_VZ', 'B0_END_VRHO', 'B0_END_VX', 'B0_END_VY', 'B0_END_VZ', 'B0_ENERGY', 'B0_ETA', 'B0_FOURMOMENTUME', 'B0_FOURMOMENTUMX', 'B0_FOURMOMENTUMY', 'B0_FOURMOMENTUMZ', 'B0_M', 'B0_MASS', 'B0_MASSWITHHYPOTHESES', 'B0_MAXDOCA', 'B0_MAXDOCACHI2', 'B0_MAXPT', 'B0_MC_GD_GD_MOTHER_ID', 'B0_MC_GD_GD_MOTHER_KEY', 'B0_MC_GD_MOTHER_ID', 'B0_MC_GD_MOTHER_KEY', 'B0_MC_ISPROMPT', 'B0_MC_LONGLIVED_ID', 'B0_MC_LONGLIVED_KEY', 'B0_MC_MOTHER_ID', 'B0_MC_MOTHER_KEY', 'B0_MINIP', 'B0_MINIPCHI2', 'B0_MINPT', 'B0_OBJECT_KEY', 'B0_P', 'B0_PHI', 'B0_PT', 'B0_PX', 'B0_PY', 'B0_PZ', 'B0_REFERENCEPOINT_X', 'B0_REFERENCEPOINT_Y', 'B0_REFERENCEPOINT_Z', 'B0_SDOCA', 'B0_SDOCACHI2', 'B0_SUBCOMB12_MM', 'B0_SUMPT', 'B0_TRUEENDVERTEX_X', 'B0_TRUEENDVERTEX_Y', 'B0_TRUEENDVERTEX_Z', 'B0_TRUEENERGY', 'B0_TRUEID', 'B0_TRUEORIGINVERTEX_X', 'B0_TRUEORIGINVERTEX_Y', 'B0_TRUEORIGINVERTEX_Z', 'B0_TRUEP', 'B0_TRUEPT', 'B0_TRUEPX', 'B0_TRUEPY', 'B0_TRUEPZ', 'BUNCHCROSSING_ID', 'BUNCHCROSSING_TYPE', 'Ds_ISBASIC', 'Ds_ABS_PX', 'Ds_ALLPVX', 'Ds_ALLPVY', 'Ds_ALLPVZ', 'Ds_ALLPV_FD', 'Ds_ALLPV_IP', 'Ds_ALV', 'Ds_BKGCAT', 'Ds_BPVCORRM', 'Ds_BPVCORRMERR', 'Ds_BPVDIRA', 'Ds_BPVDLS', 'Ds_BPVETA', 'Ds_BPVFD', 'Ds_BPVFDCHI2', 'Ds_BPVFDIRX', 'Ds_BPVFDIRY', 'Ds_BPVFDIRZ', 'Ds_BPVFDVECX', 'Ds_BPVFDVECY', 'Ds_BPVFDVECZ', 'Ds_BPVIP', 'Ds_BPVIPCHI2', 'Ds_BPVLTIME', 'Ds_BPVVDRHO', 'Ds_BPVVDX', 'Ds_BPVVDY', 'Ds_BPVVDZ', 'Ds_BPVX', 'Ds_BPVY', 'Ds_BPVZ', 'Ds_CHARGE', 'Ds_CHI2', 'Ds_CHI2DOF', 'Ds_DOCA', 'Ds_DOCACHI2', 'Ds_DTF_BPVIPCHI2',  'Ds_DTF_MASS_BiggestDELTAPT', 'Ds_DTF_MASS_SmallestDELTAPT', 'Ds_DTF_PT', 'Ds_END_VRHO', 'Ds_END_VX', 'Ds_END_VY', 'Ds_END_VZ', 'Ds_ENERGY', 'Ds_ETA', 'Ds_FOURMOMENTUME', 'Ds_FOURMOMENTUMX', 'Ds_FOURMOMENTUMY', 'Ds_FOURMOMENTUMZ', 'Ds_M', 'Ds_MASS', 'Ds_MASSWITHHYPOTHESES', 'Ds_MAXDOCA', 'Ds_MAXDOCACHI2', 'Ds_MAXPT', 'Ds_MC_GD_GD_MOTHER_ID', 'Ds_MC_GD_GD_MOTHER_KEY', 'Ds_MC_GD_MOTHER_ID', 'Ds_MC_GD_MOTHER_KEY', 'Ds_MC_MOTHER_ID', 'Ds_MC_MOTHER_KEY', 'Ds_MINIP', 'Ds_MINIPCHI2', 'Ds_MINPT', 'Ds_OBJECT_KEY', 'Ds_P', 'Ds_PHI', 'Ds_PT', 'Ds_PX', 'Ds_PY', 'Ds_PZ', 'Ds_REFERENCEPOINT_X', 'Ds_REFERENCEPOINT_Y', 'Ds_REFERENCEPOINT_Z', 'Ds_SDOCA', 'Ds_SDOCACHI2', 'Ds_SUBCOMB12_MM', 'Ds_SUMPT', 'Ds_TRUEENDVERTEX_X', 'Ds_TRUEENDVERTEX_Y', 'Ds_TRUEENDVERTEX_Z', 'Ds_TRUEENERGY', 'Ds_TRUEID', 'Ds_TRUEORIGINVERTEX_X', 'Ds_TRUEORIGINVERTEX_Y', 'Ds_TRUEORIGINVERTEX_Z', 'Ds_TRUEP', 'Ds_TRUEPT', 'Ds_TRUEPX', 'Ds_TRUEPY', 'Ds_TRUEPZ', 'EVENTNUMBER', 'EVENTTYPE', 'GPSTIME', 'Kaon_ISBASIC', 'Kaon_ABS_PX', 'Kaon_ALLPVX', 'Kaon_ALLPVY', 'Kaon_ALLPVZ', 'Kaon_BPVIP', 'Kaon_BPVIPCHI2', 'Kaon_BPVX', 'Kaon_BPVY', 'Kaon_BPVZ', 'Kaon_BREMBENDCORR', 'Kaon_BREMTRACKBASEDENERGY', 'Kaon_BREMENERGY', 'Kaon_BREMHYPODELTAX', 'Kaon_BREMHYPOENERGY', 'Kaon_BREMHYPOMATCH_CHI2', 'Kaon_BREMPIDE', 'Kaon_CHARGE', 'Kaon_CHI2', 'Kaon_CHI2DOF', 'Kaon_CLUSTERMATCH_CHI2', 'Kaon_DTF_BPVIPCHI2', 'Kaon_DTF_MASS_BiggestDELTAPT', 'Kaon_DTF_MASS_SmallestDELTAPT', 'Kaon_DTF_PT', 'Kaon_ECALPIDE', 'Kaon_ECALPIDMU', 'Kaon_ELECTRONENERGY', 'Kaon_ELECTRONID', 'Kaon_ELECTRONMATCH_CHI2', 'Kaon_ELECTRONSHOWEREOP', 'Kaon_ELECTRONSHOWERDLL', 'Kaon_ENERGY', 'Kaon_ETA', 'Kaon_FOURMOMENTUME', 'Kaon_FOURMOMENTUMX', 'Kaon_FOURMOMENTUMY', 'Kaon_FOURMOMENTUMZ', 'Kaon_GHOSTPROB', 'Kaon_HASBREM', 'Kaon_HCALEOP', 'Kaon_HCALPIDE', 'Kaon_HCALPIDMU', 'Kaon_INECAL', 'Kaon_INHCAL', 'Kaon_INMUON', 'Kaon_ISMUON', 'Kaon_IS_ABS_ID_pi', 'Kaon_IS_ID_pi', 'Kaon_PDG_MASS_pi', 'Kaon_SIGNED_DELTA_MASS_pi', 'Kaon_ABS_DELTA_MASS_pi', 'Kaon_IS_NOT_H', 'Kaon_IS_PHOTON', 'Kaon_M', 'Kaon_MASS', 'Kaon_MC_GD_GD_MOTHER_ID', 'Kaon_MC_GD_GD_MOTHER_KEY', 'Kaon_MC_GD_MOTHER_ID', 'Kaon_MC_GD_MOTHER_KEY', 'Kaon_MC_MOTHER_ID', 'Kaon_MC_MOTHER_KEY', 'Kaon_MINIP', 'Kaon_MINIPCHI2', 'Kaon_NDOF', 'Kaon_NFTHITS', 'Kaon_NHITS', 'Kaon_NUTHITS', 'Kaon_NVPHITS', 'Kaon_OBJECT_KEY', 'Kaon_P', 'Kaon_PARTICLE_ID', 'Kaon_PHI', 'Kaon_PID_E', 'Kaon_PID_K', 'Kaon_PID_MU', 'Kaon_PID_P', 'Kaon_PID_PI', 'Kaon_PROBNN_D', 'Kaon_PROBNN_E', 'Kaon_PROBNN_GHOST', 'Kaon_PROBNN_K', 'Kaon_PROBNN_MU', 'Kaon_PROBNN_P', 'Kaon_PROBNN_PI', 'Kaon_PT', 'Kaon_PX', 'Kaon_PY', 'Kaon_PZ', 'Kaon_QOVERP', 'Kaon_REFERENCEPOINT_X', 'Kaon_REFERENCEPOINT_Y', 'Kaon_REFERENCEPOINT_Z', 'Kaon_SHOWER_SHAPE', 'Kaon_TRACKHASUT', 'Kaon_TRACKHASVELO', 'Kaon_TRACKHISTORY', 'Kaon_TRACKPT', 'Kaon_TRACK_MOM_X', 'Kaon_TRACK_MOM_Y', 'Kaon_TRACK_MOM_Z', 'Kaon_TRACK_POS_CLOSESTTOBEAM_X', 'Kaon_TRACK_POS_CLOSESTTOBEAM_Y', 'Kaon_TRACK_POS_CLOSESTTOBEAM_Z', 'Kaon_TRUEENERGY', 'Kaon_TRUEID', 'Kaon_TRUEP', 'Kaon_TRUEPT', 'Kaon_TRUEPX', 'Kaon_TRUEPY', 'Kaon_TRUEPZ', 'Kaon_TX', 'Kaon_TY', 'ODINTCK', 'PV_SIZE', 'RUNNUMBER', 'SpruceB2OC_BdToDsmK_DsmToHHH_FESTDecision', 'Spruce_TCK', 'nPVs', 'pip_ABS_PX', 'pip_ALLPVX', 'pip_ALLPVY', 'pip_ALLPVZ', 'pip_BPVIP', 'pip_BPVIPCHI2', 'pip_BPVX', 'pip_BPVY', 'pip_BPVZ', 'pip_BREMBENDCORR', 'pip_BREMENERGY', 'pip_BREMHYPODELTAX', 'pip_BREMHYPOENERGY', 'pip_BREMHYPOMATCH_CHI2', 'pip_BREMPIDE', 'pip_BREMTRACKBASEDENERGY', 'pip_CHARGE', 'pip_CHI2', 'pip_CHI2DOF', 'pip_CLUSTERMATCH_CHI2', 'pip_DTF_BPVIPCHI2', 'pip_DTF_MASS_BiggestDELTAPT', 'pip_DTF_MASS_SmallestDELTAPT', 'pip_DTF_PT', 'pip_ECALPIDE', 'pip_ECALPIDMU', 'pip_ELECTRONENERGY', 'pip_ELECTRONID', 'pip_ELECTRONMATCH_CHI2', 'pip_ELECTRONSHOWEREOP', 'pip_ELECTRONSHOWERDLL', 'pip_ENERGY', 'pip_ETA', 'pip_FOURMOMENTUME', 'pip_FOURMOMENTUMX', 'pip_FOURMOMENTUMY', 'pip_FOURMOMENTUMZ', 'pip_GHOSTPROB', 'pip_HASBREM', 'pip_HCALEOP', 'pip_HCALPIDE', 'pip_HCALPIDMU', 'pip_INECAL', 'pip_INHCAL', 'pip_INMUON', 'pip_ISMUON', 'pip_IS_ABS_ID_pi', 'pip_IS_ID_pi',  'pip_PDG_MASS_pi', 'pip_SIGNED_DELTA_MASS_pi', 'pip_ABS_DELTA_MASS_pi', 'pip_IS_NOT_H', 'pip_IS_PHOTON', 'pip_M', 'pip_MASS', 'pip_MC_GD_GD_MOTHER_ID', 'pip_MC_GD_GD_MOTHER_KEY', 'pip_MC_GD_MOTHER_ID', 'pip_MC_GD_MOTHER_KEY', 'pip_MC_MOTHER_ID', 'pip_MC_MOTHER_KEY', 'pip_MINIP', 'pip_MINIPCHI2', 'pip_NDOF', 'pip_NFTHITS', 'pip_NHITS', 'pip_NUTHITS', 'pip_NVPHITS', 'pip_OBJECT_KEY', 'pip_P', 'pip_PARTICLE_ID', 'pip_PHI', 'pip_PID_E', 'pip_PID_K', 'pip_PID_MU', 'pip_PID_P', 'pip_PID_PI', 'pip_PROBNN_D', 'pip_PROBNN_E', 'pip_PROBNN_GHOST', 'pip_PROBNN_K', 'pip_PROBNN_MU', 'pip_PROBNN_P', 'pip_PROBNN_PI', 'pip_PT', 'pip_PX', 'pip_PY', 'pip_PZ', 'pip_QOVERP', 'pip_REFERENCEPOINT_X', 'pip_REFERENCEPOINT_Y', 'pip_REFERENCEPOINT_Z', 'pip_SHOWER_SHAPE', 'pip_TRACKHASUT', 'pip_TRACKHASVELO', 'pip_TRACKHISTORY', 'pip_TRACKPT', 'pip_TRACK_MOM_X', 'pip_TRACK_MOM_Y', 'pip_TRACK_MOM_Z', 'pip_TRACK_POS_CLOSESTTOBEAM_X', 'pip_TRACK_POS_CLOSESTTOBEAM_Y', 'pip_TRACK_POS_CLOSESTTOBEAM_Z', 'pip_TRUEENERGY', 'pip_TRUEID', 'pip_TRUEP', 'pip_TRUEPT', 'pip_TRUEPX', 'pip_TRUEPY', 'pip_TRUEPZ', 'pip_TX', 'pip_TY', 'pip_ISBASIC']
B_vars_stored += ['Ds_DTF_CTAUERR', 'Ds_DTF_MASSERR', 'Ds_DTF_FDERR', 'B0_DTF_MASS', 'B0_DTF_PERR', 'B0_DTF_CTAUERR', 'Ds_DTF_PERR', 'Ds_DTF_MASS', 'B0_DTF_CTAU', 'B0_DTF_NDOF', 'B0_DTF_MASSERR', 'Ds_DTF_CTAU', 'Ds_DTF_FD', 'B0_DTF_CHI2', 'B0_DTF_FD', 'B0_DTF_NITER', 'Ds_DTF_P', 'B0_DTF_FDERR', 'B0_DTF_CHI2DOF', 'B0_DTF_P']
B_vars_stored += ["B0_TRUEPRIMARYVERTEX_X", "B0_TRUEPRIMARYVERTEX_Y", "B0_TRUEPRIMARYVERTEX_Z", "Kaon_TRUEPRIMARYVERTEX_X", "Kaon_TRUEPRIMARYVERTEX_Y", "Kaon_TRUEPRIMARYVERTEX_Z"]
B_vars_stored += ["Ds_TRUEPRIMARYVERTEX_X", "Ds_TRUEPRIMARYVERTEX_Y", "Ds_TRUEPRIMARYVERTEX_Z", "pip_TRUEPRIMARYVERTEX_X", "pip_TRUEPRIMARYVERTEX_Y", "pip_TRUEPRIMARYVERTEX_Z", "Kaon_HASBREMADDED", "pip_HASBREMADDED","Kaon_CLUSTERID","pip_CLUSTERID"]
B_vars_stored += ['pip_THREE_MOM_COV_MATRIX', 'Kaon_MOM_POS_COV_MATRIX', 'Kaon_THREE_MOM_COV_MATRIX', 'Kaon_POS_COV_MATRIX', 'pip_MOM_POS_COV_MATRIX', 'pip_POS_COV_MATRIX', 'pip_THREE_MOM_POS_COV_MATRIX', 'Kaon_THREE_MOM_POS_COV_MATRIX', 'pip_STATE_AT_T1_flag', 'pip_STATE_AT_T1_statevector', 'pip_STATE_AT_T1_covariance', 'pip_STATE_AT_T1_Z', 'Kaon_STATE_AT_T1_flag', 'Kaon_STATE_AT_T1_statevector', 'Kaon_STATE_AT_T1_covariance', 'Kaon_STATE_AT_T1_Z']

#sort the expected vars
B_vars_stored = sorted(B_vars_stored)

#open the TFile and TTree
ntuple = Path('./DV_example_allFunctors_ntp.root')
if not ntuple.is_file():
    causes.append(f"File {ntuple} does not exist!")
f   = TFile.Open(ntuple.name)
t_B    = findRootObjByDir(f, 'B0DsK_Tuple', 'DecayTree')

#sort the stores vars
b_names = sorted([b.GetName() for b in t_B.GetListOfLeaves()])

B_excluded_1 = set(B_vars_stored) - set(b_names)
B_excluded_2 = set(b_names) - set(B_vars_stored)
if len(B_excluded_1) != 0:
    causes.append(f"Number of stored variables is less than what is expected. The extra variables expected are: {B_excluded_1}")
if len(B_excluded_2) != 0:
    causes.append(f"Number of stored variables is greater than what is expected. The extra variables stored are: {B_excluded_2}")
f.Close()

# Check there are no NaN values in the ntuple except where expected.
from DaVinciTests.QMTest.check_helpers import list_fields_with_nan

l_branches_with_nans = ['B0_TRUEP', 'B0_TRUEPT', 'B0_TRUEPX', 'B0_TRUEPY', 'B0_TRUEPZ', 'B0_TRUEENERGY', 'B0_TRUEORIGINVERTEX_X', 'B0_TRUEORIGINVERTEX_Y', 'B0_TRUEORIGINVERTEX_Z', 'B0_TRUEENDVERTEX_X', 'B0_TRUEENDVERTEX_Y', 'B0_TRUEENDVERTEX_Z', 'Kaon_PROBNN_D', 'Kaon_PROBNN_MU', 'Kaon_TRUEP', 'Kaon_TRUEPT', 'Kaon_TRUEPX', 'Kaon_TRUEPY', 'Kaon_TRUEPZ', 'Kaon_TRUEENERGY', 'Kaon_BREMENERGY', 'Kaon_BREMBENDCORR', 'Kaon_BREMPIDE', 'Kaon_ECALPIDE', 'Kaon_ECALPIDMU', 'Kaon_HCALPIDE', 'Kaon_HCALPIDMU', 'Kaon_ELECTRONSHOWEREOP', 'Kaon_ELECTRONSHOWERDLL', 'Kaon_ELECTRONMATCH_CHI2', 'Kaon_BREMHYPOMATCH_CHI2', 'Kaon_ELECTRONENERGY', 'Kaon_BREMHYPOENERGY', 'Kaon_BREMHYPODELTAX', 'Kaon_BREMTRACKBASEDENERGY', 'Kaon_HCALEOP', 'Ds_TRUEP', 'Ds_TRUEPT', 'Ds_TRUEPX', 'Ds_TRUEPY', 'Ds_TRUEPZ', 'Ds_TRUEENERGY', 'Ds_TRUEORIGINVERTEX_X', 'Ds_TRUEORIGINVERTEX_Y', 'Ds_TRUEORIGINVERTEX_Z', 'Ds_TRUEENDVERTEX_X', 'Ds_TRUEENDVERTEX_Y', 'Ds_TRUEENDVERTEX_Z', 'Ds_BPVCORRMERR', 'Ds_BPVLTIME', 'pip_PROBNN_D', 'pip_PROBNN_MU', 'pip_TRUEP', 'pip_TRUEPT', 'pip_TRUEPX', 'pip_TRUEPY', 'pip_TRUEPZ', 'pip_TRUEENERGY', 'pip_BREMENERGY', 'pip_BREMBENDCORR', 'pip_BREMPIDE', 'pip_ECALPIDE', 'pip_ECALPIDMU', 'pip_HCALPIDE', 'pip_HCALPIDMU', 'pip_ELECTRONSHOWEREOP', 'pip_ELECTRONSHOWERDLL', 'pip_ELECTRONMATCH_CHI2', 'pip_BREMHYPOMATCH_CHI2', 'pip_ELECTRONENERGY', 'pip_BREMHYPOENERGY', 'pip_BREMHYPODELTAX', 'pip_BREMTRACKBASEDENERGY', 'pip_HCALEOP']
l_branches_with_nans += ["B0_TRUEPRIMARYVERTEX_X", "B0_TRUEPRIMARYVERTEX_Y", "B0_TRUEPRIMARYVERTEX_Z", "Kaon_TRUEPRIMARYVERTEX_X", "Kaon_TRUEPRIMARYVERTEX_Y", "Kaon_TRUEPRIMARYVERTEX_Z"]
l_branches_with_nans += ["Ds_TRUEPRIMARYVERTEX_X", "Ds_TRUEPRIMARYVERTEX_Y", "Ds_TRUEPRIMARYVERTEX_Z", "pip_TRUEPRIMARYVERTEX_X", "pip_TRUEPRIMARYVERTEX_Y", "pip_TRUEPRIMARYVERTEX_Z"]
l_branches_with_nans += ["Kaon_IS_NOT_H","Kaon_IS_PHOTON","Kaon_SHOWER_SHAPE","pip_IS_NOT_H","pip_IS_PHOTON","pip_SHOWER_SHAPE"]

l_test = list_fields_with_nan("DV_example_allFunctors_ntp.root", "B0DsK_Tuple/DecayTree")
if sorted(l_test) != sorted(l_branches_with_nans):
    causes.append("Unexpected list of branches with NaN values")

print('Test successfully completed!')
ntuple.unlink()
</text></argument>
</extension>
