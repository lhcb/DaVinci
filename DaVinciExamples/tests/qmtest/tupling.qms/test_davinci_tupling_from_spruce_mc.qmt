<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="args"><set>
    <text>DaVinciExamples.tupling.option_davinci_tupling_from_spruce_mc:main</text>
    <text>--override-option-class=DaVinci.LbExec:TestOptions</text>
  </set></argument>
  <argument name="options_yaml_fn"><text>$DAVINCIEXAMPLESROOT/example_data/test_spruce_MCtools.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    print_freq: 1
  </text></argument>
<argument name="validator"><text>
from PyConf.components import findRootObjByDir

findReferenceBlock("""Tuple                               SUCCESS Booked 1 N-Tuples and 0 Event Tag Collections"""
, stdout, result, causes, signature_offset = 0)

countErrorLines({"FATAL":0, "ERROR":0})

from pathlib import Path
from DaVinciTests.QMTest.check_helpers import get_pandas_dataframe, list_fields_with_nan

ntuple = Path('./sprucing_mc_tuple.root')

if not ntuple.is_file():
    causes.append(f"File {ntuple} does not exist!")

df = get_pandas_dataframe(ntuple.name, 'Tuple/DecayTree')

# Check ntuples structure
if df.empty:
    causes.append(f"File {ntuple}: ntuple does not contain any branches")
if df.shape != (15, 62):
    causes.append("Ntuple not with expected number of entries and/or branches")

# Check there are no NaN values in the ntuple except where expected
l_branches_with_nans = ['B0_TRUEENERGY',
                        'B0_TRUEFOURMOMENTUME',
			'B0_TRUEFOURMOMENTUMX',
			'B0_TRUEFOURMOMENTUMY',
			'B0_TRUEFOURMOMENTUMZ',
			'B0_TRUEP',
			'B0_TRUEPT',
			'B0_TRUEPX',
			'B0_TRUEPY',
			'B0_TRUEPZ',
			'Ds_TRUEENERGY',
			'Ds_TRUEFOURMOMENTUME',
			'Ds_TRUEFOURMOMENTUMX',
			'Ds_TRUEFOURMOMENTUMY',
			'Ds_TRUEFOURMOMENTUMZ',
			'Ds_TRUEP',
			'Ds_TRUEPT',
			'Ds_TRUEPX',
			'Ds_TRUEPY',
			'Ds_TRUEPZ']

l_test = list_fields_with_nan(ntuple.name, "Tuple/DecayTree")
if sorted(l_test) != sorted(l_branches_with_nans):
    causes.append("Unexpected list of branches with NaN values")

# Checks background category values and associated MC-truth PIDs are correctly assigned
ok = (((df["B0_BKGCAT"].abs() == 20).sum() == 12) and
      (df["Kp_BKGCAT"] == -1).all() and
      ((df["B0_TRUEID"].abs() == 531).sum() == 13) and
      ((df["Ds_TRUEID"].abs() == 431).sum() == 14) and
      ((df["Ds_TRUEID"] == 0).sum() == 1) and
      (df["Kp_TRUEID"] != 0).all()
     )
if not ok:
    causes.append("Ntuple contains unexpected BKGCAT and/or TRUEID values")

ntuple.unlink()
print('Test successfully completed!')
</text></argument>
</extension>
