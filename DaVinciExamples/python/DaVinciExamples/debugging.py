###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a DaVinci job printing decay trees via `PrintDecayTree`.

rst_title: Printing Decay Trees
rst_description: This example shows how to print the decay tree of a candidate.
rst_running: lbexec DaVinciExamples.debugging:print_decay_tree "$DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml"
rst_yaml: ../DaVinciExamples/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml
"""
from PyConf.application import configure, configure_input
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import PrintDecayTree, PrintHeader

from DaVinci import Options
from DaVinci.common_particles import make_std_loose_jpsi2mumu


def print_decay_tree(options: Options):
    config = configure_input(options)
    jpsis = make_std_loose_jpsi2mumu()

    pdt = PrintDecayTree(name="PrintJpsis", Input=jpsis)

    node = CompositeNode(
        "PrintJpsiNode", children=[jpsis, pdt], combine_logic=NodeLogic.NONLAZY_AND
    )

    config.update(configure(options, node))
    return config


def print_header(options: Options):
    config = configure_input(options)
    ph = PrintHeader(name="PrintHeader", ODINLocation=make_odin())

    node = CompositeNode("PHNode", children=[ph])

    config.update(configure(options, node))
    return config
