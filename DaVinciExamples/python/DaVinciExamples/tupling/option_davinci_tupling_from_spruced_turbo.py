###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""option_davinci_tupling_spruced_turbo.py
Example options to show how to run over spruced turbo output.
rst_title: Running on spruced turbo output
rst_description: This example shows how to run on spruced turbo output with 2023 commissioning data.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_from_spruced_turbo:main $DAVINCIEXAMPLESROOT/example_data/test_spruced_turbo.yaml
rst_yaml: ../DaVinciExamples/example_data/test_spruced_turbo.yaml
"""

from PyConf.reading import get_particles, get_pvs
from RecoConf.event_filters import require_pvs

from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
import Functors as F


def main(options: Options):
    line = "Hlt2QEE_JpsiToMuMu_Detached"
    data_location = f"/Event/HLT2/{line}/Particles"
    line_prefilter = create_lines_filter(name=f"PreFilter_{line}", lines=[line])

    pvs = get_pvs()

    decay_descriptor = {
        "Jpsi": "J/psi(1S) -> mu- mu+",
        "mum": "J/psi(1S) -> ^mu- mu+",
        "mup": "J/psi(1S) -> mu- ^mu+",
    }

    variables = {}
    shared_variables = FunctorCollection(
        {
            "M": F.MASS,
            "ID": F.PARTICLE_ID,
            "PX": F.PX,
            "PY": F.PY,
            "PZ": F.PZ,
            "ENERGY": F.ENERGY,
            "Q": F.CHARGE,
        }
    )
    muon_variables = FunctorCollection(
        {
            "ISMUON": F.ISMUON,
            "BPVIP": F.BPVIP(pvs),
            "BPVIPCHI2": F.BPVIPCHI2(pvs),
            "TRCHI2": F.CHI2,
        }
    )
    variables = {"ALL": shared_variables, "mup": muon_variables, "mum": muon_variables}

    event_info = FC.EventInfo()

    funtuple = Funtuple(
        name=line,
        tuple_name="DecayTree",
        fields=decay_descriptor,
        variables=variables,
        inputs=get_particles(data_location),
        event_variables=event_info,
    )

    algs = {line: [line_prefilter, require_pvs(pvs), funtuple]}

    return make_config(options, algs)
