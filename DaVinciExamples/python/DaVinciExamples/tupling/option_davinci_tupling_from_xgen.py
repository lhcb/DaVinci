###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read and process a .xgen file with the Run-3 DaVinci configuration.
It shows how one can print information from the MCParticles in the file,
via `PrintMCTree`, and how the MCParticles can be filtered
before ending up in the ntuple, with the `MCParticleFilter`.

rst_title: Run on XGEN files
rst_description: This example reads and process a `.xgen` file.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_from_xgen:main $DAVINCIEXAMPLESROOT/example_data/Gauss_12143001_xgen.yaml
rst_yaml: ../DaVinciExamples/example_data/Gauss_12143001_xgen.yaml
"""
from FunTuple import FunctorCollection, FunTuple_MCParticles as FuntupleMC
import FunTuple.functorcollections as FC
from PyConf.Algorithms import PrintMCTree
import Functors as F
from Functors.math import in_range
from DaVinci import Options, make_config
from PyConf.reading import get_mc_particles, get_mc_header
from RecoConf.algorithms_thor import MCParticleFilter


def main(options: Options):
    # FunTuple: define fields (branches)
    fields = {
        "Bu": "[B+ -> (J/psi(1S) -> mu+ mu-) K+]CC",
        "Jpsi": "[B+ -> ^(J/psi(1S) -> mu+ mu-) K+]CC",
        "Kplus": "[B+ -> (J/psi(1S) -> mu+ mu-) ^K+]CC",
        "muplus": "[B+ -> (J/psi(1S) -> ^mu+ mu-) K+]CC",
        "muminus": "[B+ -> (J/psi(1S) -> mu+ ^mu-) K+]CC",
    }

    # FunTuple: define variables for the B meson
    variables_B = FunctorCollection(
        {
            "ETA": F.ETA,
            "PHI": F.PHI,
            "TAU": F.MC_LIFETIME,
            "ORIGIN_VX": F.ORIGIN_VX,
            "ORIGIN_VY": F.ORIGIN_VY,
            "ORIGIN_VZ": F.ORIGIN_VZ,
            "END_VX": F.END_VX,
            "END_VY": F.END_VY,
            "END_VZ": F.END_VZ,
        }
    )

    # FunTuple: define common variables
    variables_all = FunctorCollection({"PT": F.PT, "P": F.FOURMOMENTUM})

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        "ALL": variables_all,
        "Bu": variables_B,
    }

    # FunTuple: define input data
    all_mc_particles = get_mc_particles("/Event/MC/Particles")

    # FunTuple: define event-level variables using functor collections
    mc_header = get_mc_header()
    evt_vars = FC.MCPrimaries(mc_header=mc_header)

    printMC = PrintMCTree(
        MCParticles=all_mc_particles, ParticleNames=["B+", "B-"], OutputLevel=4
    )

    # now we filter that set of B particles
    cut = F.require_all(in_range(1.8, F.ETA, 5.0))
    filtered_mc_particles = MCParticleFilter(all_mc_particles, F.FILTER(cut))

    tuple_BuJpsiK = FuntupleMC(
        name="BuJpsiK_MCTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_vars,
        inputs=filtered_mc_particles,
    )

    return make_config(options, {"BuJpsiK": [printMC, tuple_BuJpsiK]})
