###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""
Read an HLT2 file and create an ntuple with information from RecSummary
(e.g. nPVs, nTracks, nFTClusters).
rst_title: Store Event Information
rst_description: Read an HLT2 file and create an ntuple with information from RecSummary (e.g. nPVs, nTracks, nFTClusters).
A sample of :math:`\Lambda^0_b \to \Lambda_c^+ \mu \nu` simulated decays is used for this example.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_eventinfo:main $DAVINCIEXAMPLESROOT/example_data/Upgrade_LbToLcmunu.yaml
rst_yaml: ../DaVinciExamples/example_data/Upgrade_LbToLcmunu.yaml
"""
from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import make_config, Options


def main(options: Options):
    # define fields
    fields = {"Lb": "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) mu-]CC"}

    # define variables and add them to all fields
    # lb_variables = Kinematics()
    lb_variables = FunctorCollection({"PT": F.PT})
    variables = {"ALL": lb_variables}

    evt_vars = FC.EventInfo()
    evt_vars += FC.RecSummary()

    # get particles to run over
    line_name = "Hlt2SLB_LbToLcMuNu_LcToPKPi_Line"
    particles = get_particles(f"/Event/HLT2/{line_name}/Particles")
    my_filter = create_lines_filter(name="Myfilter", lines=[f"{line_name}"])
    # define tupling algorithm
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_vars,
        inputs=particles,
    )

    return make_config(options, [my_filter, my_tuple])
