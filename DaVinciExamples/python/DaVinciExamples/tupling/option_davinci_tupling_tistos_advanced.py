###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: TISTOS advanced
rst_description: For a multibody decay, this example shows how to get TIS/TOS information on the two-body combinations e.g. to check which of the two-body combinations fired the Hlt1TwoTrackMVA line.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_tistos_advanced:main $DAVINCIEXAMPLESROOT/example_data/test_tistos_advanced.yaml
rst_yaml: ../DaVinciExamples/example_data/test_tistos_advanced.yaml
"""
from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from PyConf.Algorithms import ParticleToSubcombinationsAlg


def main(options: Options):
    # define fields for candidates in the Hlt2 line
    fields = {
        "Bs": "[B_s0 ->  (phi(1020) ->  K+  K-)  (phi(1020) ->  K+  K-)]CC",
        "phi1": "[B_s0 -> ^(phi(1020) ->  K+  K-)  (phi(1020) ->  K+  K-)]CC",
        "phi2": "[B_s0 ->  (phi(1020) ->  K+  K-) ^(phi(1020) ->  K+  K-)]CC",
        "Kp1": "[B_s0 ->  (phi(1020) -> ^K+  K-)  (phi(1020) ->  K+  K-)]CC",
        "Km1": "[B_s0 ->  (phi(1020) ->  K+ ^K-)  (phi(1020) ->  K+  K-)]CC",
        "Kp2": "[B_s0 ->  (phi(1020) ->  K+  K-)  (phi(1020) -> ^K+  K-)]CC",
        "Km2": "[B_s0 ->  (phi(1020) ->  K+  K-)  (phi(1020) ->  K+ ^K-)]CC",
    }

    # define variables to be added to all fields
    variables_all = FunctorCollection({"PT": F.PT})

    # specify line name to get data and define a filter for that line
    line_name = "Hlt2BnoC_BdsToPhiPhi"
    data = get_particles(f"/Event/HLT2/{line_name}/Particles")

    my_filter = create_lines_filter(name="HDRFilter_test", lines=[f"{line_name}"])

    # Lets store the Hlt1specify Hlt1 line names for which TIS/TOS information is requested.
    # Note that the Hlt2 Tis/Tos information is work-in-progress and
    # only Hlt1 Tis/Tos information is available at the moment.
    Hlt1_decisions = ["Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision"]
    # Use the functorcollection "HltTisTos"
    # 1st argument is selection type: "Hlt1" or "Hlt2". Currently only "Hlt1" is supported.
    # 2nd argument is the list of Hlt1 line names for which TIS/TOS information is requested.
    # 3rd argument is the TES location of the reconstructed particles.
    variables_tistos = FC.HltTisTos(
        selection_type="Hlt1", trigger_lines=Hlt1_decisions, data=data
    )

    # Here, the TIS/TOS information is added for all possible
    # subcombinations of particles, i.e. (K1 K2), (K2 K4) etc.
    # This is done by first getting the standard TisTos functors
    # and then mapping them through a relation table
    data_subcombinations = ParticleToSubcombinationsAlg(Input=data)
    tistos_variables_extra = FC.HltTisTos(
        selection_type="Hlt1",
        trigger_lines=Hlt1_decisions,
        data=data_subcombinations.OutputParticles,
    )
    variables_tistos_subcomb = {}
    for k, v in tistos_variables_extra.get_thor_functors().items():
        variables_tistos_subcomb[k + "_SUBCOMB"] = F.MAP_INPUT_ARRAY(
            v, data_subcombinations.OutputRelations
        )
    variables_tistos_subcomb = FunctorCollection(variables_tistos_subcomb)

    variables_all += variables_tistos

    variables = {"Bs": variables_tistos_subcomb, "ALL": variables_all}

    # When the TIS and TOS information of candidate wrt a line is negative but the event has still fired a Hlt1 line,
    # then the trigger category for candidates wrt to that line is TOB (Trigger On Both) i.e. (!TIS && !TOS && event_decision).
    # To store "event_decision" of Hlt1 lines and check for "TOB", we use "SelectionInfo" functor collection.
    evt_variables = FC.SelectionInfo(
        selection_type="Hlt1", trigger_lines=Hlt1_decisions
    )

    # define funtuple instance
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=data,
    )

    return make_config(options, [my_filter, my_tuple])
