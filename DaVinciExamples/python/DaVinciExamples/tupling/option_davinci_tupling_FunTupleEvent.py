###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example for tupling on and event-by-event basis via the `FunTuple_Event` helper.
This algorithm takes no input and is configured with "void" functors, i.e. functors that expect no input.
This is is contrast to `FunTuple_(MC)Particles`, which take as input the TES location of (MC)Particles
and are configured with functors that operate on a single Particle/MCParticle.

rst_title: FunTuple event-by-event
rst_description: Example for tupling on and event-by-event basis via the `FunTuple_Event` helper.
This algorithm takes no input and is configured with "void" functors, i.e. functors that expect no input.
This is is contrast to `FunTuple_(MC)Particles`, which take as input the TES location of (MC)Particles
and are configured with functors that operate on a single Particle/MCParticle.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_FunTupleEvent:main $DAVINCIEXAMPLESROOT/example_data/Upgrade_LbToLcmunu.yaml
rst_yaml: ../DaVinciExamples/example_data/Upgrade_LbToLcmunu.yaml
"""

from FunTuple import FunTuple_Event as Funtuple
import FunTuple.functorcollections as FC
from DaVinci import make_config, Options


def main(options: Options):
    # get RecSummary object that holds information about nPVs, nTracks, nFTClusters
    # Note more information can be added to the RecSummary object
    # (see MRs: https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/1649)

    evt_vars = FC.EventInfo()
    evt_vars += FC.RecSummary()

    # define tupling algorithm
    my_tuple = Funtuple(name="Tuple", tuple_name="EventInfo", variables=evt_vars)

    return make_config(options, [my_tuple])
