###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
from collections import Counter

import Functors as F
from FunTuple import FunctorCollection


@pytest.fixture()
def simple_fc():
    return FunctorCollection({"PT": F.PT, "X": "X", "Y": "Y"})


def test_constructor_default():
    _ = FunctorCollection()


def test_constructor_from_dict():
    c = FunctorCollection(
        {
            "LoKi_Muonp_PT": "CHILD(PT, 1)",
            "LoKi_Muonm_PT": "CHILD(PT, 2)",
            "ThOr_Muonp_PT": F.CHILD(1, F.PT),
            "ThOr_Muonm_PT": F.CHILD(2, F.PT),
        }
    )
    assert len(c.get_loki_functors()) == 2
    assert len(c.get_thor_functors()) == 2


def test_constructor_from_dict_like(simple_fc):
    # The Counter class is a dict-like class and should work
    counter = Counter({"PT": F.PT, "X": "X", "Y": "Y"})
    fc = FunctorCollection(counter)
    assert len(fc.functor_dict) == 3
    assert fc == simple_fc


def test_constructor_AttributeError_input_type():
    with pytest.raises(ValueError):
        _ = FunctorCollection({"Not", "a", "dict"})


def test_constructor_TypeError_dict_keys():
    with pytest.raises(TypeError):
        _ = FunctorCollection({(1,): "CHILD(PT, 1)", "PT": F.PT})


def test_constructor_TypeError_bad_functor_type():
    with pytest.raises(TypeError):
        _ = FunctorCollection({"BadType": lambda x: x**2})


def test_repr_thor():
    c = FunctorCollection({"PT": F.PT})
    assert repr(c) == "<FunctorCollection: n_thor_fctors=1, n_loki_fctors=0>"


def test_repr_loki():
    c = FunctorCollection(
        {"LoKi_Muonp_PT": "CHILD(PT, 1)", "LoKi_Muonm_PT": "CHILD(PT, 2)"}
    )
    assert repr(c) == "<FunctorCollection: n_thor_fctors=0, n_loki_fctors=2>"


def test_str(simple_fc):
    _str = """    PT: ('Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum )', '( RHO_COORDINATE @ THREEMOMENTUM )')
    X: X
    Y: Y"""
    assert _str in str(simple_fc)


def test_setitem(simple_fc):
    simple_fc["Z"] = "Z"
    assert len(simple_fc) == 4


# Filter the expected UserWarning
@pytest.mark.filterwarnings("ignore:FunctorCollection.__setitem__")
def test_setitem_UserWarning(simple_fc):
    simple_fc["X"] = "X"
    assert len(simple_fc) == 3


def test_getitem():
    c = FunctorCollection(
        {
            "LoKi_Muonp_PT": "CHILD(PT, 1)",
            "LoKi_Muonm_PT": "CHILD(PT, 2)",
            "ThOr_Muonp_PT": F.CHILD(1, F.PT),
            "ThOr_Muonm_PT": F.CHILD(2, F.PT),
        }
    )

    assert c["LoKi_Muonp_PT"] is not None
    assert c["ThOr_Muonp_PT"] is not None

    with pytest.raises(KeyError):
        _ = c["Unknown"]


def test_update(simple_fc):
    simple_fc.update(
        {
            "ThOr_Muonp_PT": F.CHILD(1, F.PT),
            "ThOr_Muonm_PT": F.CHILD(2, F.PT),
        }
    )
    assert len(simple_fc) == 5


# Filter the expected UserWarning
@pytest.mark.filterwarnings("ignore:FunctorCollection.update")
def test_update_UserWarning(simple_fc):
    simple_fc.update({"Y": "YY"})


def test_update_TypeError(simple_fc):
    with pytest.raises(TypeError):
        simple_fc.update("NotADict")


def test_pop_str():
    c = FunctorCollection({"PT": F.PT})
    c.pop("PT")


def test_pop_list():
    c = FunctorCollection({"PT": F.PT})
    c.pop(["PT"])


def test_pop_KeyError(simple_fc):
    with pytest.raises(KeyError):
        simple_fc.pop("A")
        simple_fc.pop(["A", "Y"])


def test_pop_AttributeError(simple_fc):
    with pytest.raises(AttributeError):
        simple_fc.pop(("Y",))


def test_add(simple_fc):
    c = FunctorCollection({"PID_K": F.PID_K, "CHI2DOF": F.CHI2DOF})
    c2 = c + simple_fc
    assert len(c2) == 5


# Filter the expected UserWarning
@pytest.mark.filterwarnings("ignore:The two functors have common entries")
def test_add_with_common_keys(simple_fc):
    c = FunctorCollection({"PT": F.PT, "CHI2DOF": F.CHI2DOF})
    c2 = c + simple_fc
    assert len(c2) == 4


def test_add_TypeError(simple_fc):
    counter = Counter({"PT": F.PT, "X": "X", "Y": "Y"})
    # FunctorCollection(counter) is valid but addition is strict on types
    with pytest.raises(TypeError):
        _ = simple_fc + counter


def test_iadd(simple_fc):
    c = FunctorCollection({"PID_K": F.PID_K, "CHI2DOF": F.CHI2DOF})
    c += simple_fc
    assert len(c) == 5


def test_sub(simple_fc):
    c = FunctorCollection({"PT": F.PT, "CHI2DOF": F.CHI2DOF})
    c2 = simple_fc - c
    assert len(c2) == 3


def test_sub_RuntimeError(simple_fc):
    c = FunctorCollection({"PT": F.PT, "X": "X", "Y": "Y"})
    with pytest.raises(RuntimeError):
        _ = simple_fc - c


def test_isub(simple_fc):
    c = FunctorCollection({"PT": F.PT, "CHI2DOF": F.CHI2DOF})
    c -= simple_fc
    assert len(c) == 3


def test_isub_RuntimeError(simple_fc):
    c = FunctorCollection({"PT": F.PT, "X": "X", "Y": "Y"})
    with pytest.raises(RuntimeError):
        c -= simple_fc


def test_contains(simple_fc):
    assert "PT" in simple_fc
    assert not ("Nope" in simple_fc)


def test_eq(simple_fc):
    """
    Test __eq__ of FunctorCollection, i.e. the equality of the stored dicts.
    """
    c = FunctorCollection({"X": "X", "Y": "Y", "PT": F.PT})
    assert c == simple_fc
