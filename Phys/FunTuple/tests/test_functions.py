###############################################################################
# (c) Copyright 2020-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys
import pytest

from PyConf.application import default_raw_event
from PyConf.reading import reconstruction, get_pvs
from PyConf.components import Algorithm
from PyConf.mock import MockDataHandle, mock_is_datahandle
from PyConf import components
import Functors as F

from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC


def reset_global_store(algorithm_store={}):
    """
    Reset the global stored for algorithms to avoid name clashes,
    see the class `Algorithm` in the `PyConf.components` module.
    """
    old_algorithm_store = Algorithm._algorithm_store
    Algorithm._algorithm_store = algorithm_store
    components._IDENTITY_TABLE.clear()
    return old_algorithm_store


def data_handle(force_type):
    """
    Make a dummy DataHandle instance to input to FunTuple_Particles or FunTuple_MCParticles.
    """
    return MockDataHandle("/Not/Existing/Path", force_type)


@pytest.fixture(autouse=True)
def mock_datahandle(monkeypatch):
    """Be sure we replace is_datahandle everywhere."""
    monkeypatch.setattr(components, "is_datahandle", mock_is_datahandle)


# Some default arguments to FunTuple_Particles or FunTuple_MCParticles
fields = dict(A="A -> B C")
variables = dict(A=FC.Kinematics())

# All tests are parametrized with pytest.mark.parametrize
# to test both FunTuple_Particles and FunTuple_MCParticles
# available in the FunTuple module
list_function_tuple_name = (
    ("FunTuple_Particles", "FunTupleP", "SharedObjectsContainer<LHCb::Particle>"),
    ("FunTuple_MCParticles", "FunTupleMCP", "SharedObjectsContainer<LHCb::MCParticle>"),
    ("FunTuple_Composites", "FunTupleComposites", "LHCb::Event::Composites"),
    ("FunTuple_ChargedBasics", "FunTupleChgBasics", "LHCb::Event::ChargedBasics"),
)

# make a separate list for FunTuple_Event as it expects no inputs
list_function_tuple_name_event = (
    ("FunTuple_Event", "FunTupleEvent", "SharedObjectsContainer<LHCb::MCParticle>"),
)

this_module = sys.modules["FunTuple"]


def getattr_wrapper(my_module, my_func_name, **kwargs):
    """
    Wrap getattr to configure the odin first that is used for
    storing RUNNUMBER and EVENTNUMBER by FunTuple.
    This is currently required only for testing purposes.
    When running the options file, odin and other related objects
    are configured globally by DaVinci.
    """
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ):
        return getattr(my_module, my_func_name)(**kwargs)


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_dummy(func_name, name_argument, type_argument):
    """
    Check that a dummy tupling instance with no fields is not allowed and raises a RuntimeError.
    """
    fields = dict()
    variables = dict()

    with pytest.raises(RuntimeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument,
            tuple_name=name_argument,
            fields=fields,
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_simplest(func_name, name_argument, type_argument, monkeypatch):
    """
    Check the simplest tupling instance where all fields are defined via the reserved name 'ALL'.
    """
    fields = dict()
    variables = dict(ALL=FC.Kinematics())

    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_simple(func_name, name_argument, type_argument):
    """
    Check a very simple instantiation where only the mother particle is tupled
    with the Kinematics collection.
    """
    tuple = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )

    # Trick to compare tuple.name and name_argument
    # ignoring the "#1" added to name by the framework
    assert tuple.name.endswith(name_argument, 0, len(name_argument))
    assert tuple.properties["run_full_counter_mode"]


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_simple_with_ALL_field(func_name, name_argument, type_argument):
    """
    Check with a typical and still simple tupling instance.
    """
    fields = dict(A="A -> B C")
    variables = dict(
        ALL=FC.Kinematics(), A=FunctorCollection({"THOR_FourMom_P": F.FOURMOMENTUM})
    )

    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_with_event_variables(func_name, name_argument, type_argument):
    """
    Check that event variables passed as an optional argument are dealt with correctly.
    """
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ):
        event_variables = FC.EventInfo()

    tuple = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        event_variables=event_variables,
        inputs=data_handle(type_argument),
    )

    if name_argument == "FunTupleMCP":
        assert sorted(list(tuple.properties["void_thor_functors"].keys())) == sorted(
            ["BUNCHCROSSING_ID", "BUNCHCROSSING_TYPE", "ODINTCK", "GPSTIME"]
        )
    else:
        assert sorted(list(tuple.properties["void_thor_functors"].keys())) == sorted(
            [
                "EVENTNUMBER",
                "RUNNUMBER",
                "BUNCHCROSSING_ID",
                "BUNCHCROSSING_TYPE",
                "ODINTCK",
                "GPSTIME",
            ]
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_with_loki_preamble(func_name, name_argument, type_argument):
    """
    Check of a next-to-simple instantiation where a LoKi preamble (string)
    is passed as an optional argument.
    """
    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    tuple = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
        loki_preamble=loki_preamble,
    )

    # Trick to compare tuple.name and name_argument
    # ignoring the "#1" added to name by the framework
    assert tuple.name.endswith(name_argument, 0, len(name_argument))


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_run_full_counter_mode(func_name, name_argument, type_argument):
    """
    Check that the optional argument 'run_full_counter_mode' gets set correctly.
    """
    tuple = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
        run_full_counter_mode=False,
    )

    assert not tuple.properties["run_full_counter_mode"]


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_ALL_not_allowed_in_field_names(func_name, name_argument, type_argument):
    """
    Check that using the reserved name 'ALL' as a key for 'fields' raises a ValueError.
    """
    with pytest.raises(ValueError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=dict(ALL="A -> B C"),  # "ALL" is not allowed since special
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_more_than_one_caret_in_field(func_name, name_argument, type_argument):
    """
    Check that using more than one caret symbol in a field raises a ``SyntaxError``.
    """
    with pytest.raises(SyntaxError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=dict(A="A -> B ^C ^D"),
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_fields_is_dict(func_name, name_argument, type_argument):
    """
    A TypeError is raised if the 'fields' argument is not a dict.
    """
    with pytest.raises(TypeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields="A -> B C",  # should be a dict
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_field_name_unused(func_name, name_argument, type_argument):
    """
    A field, 'C', is defined with no corresponding variables to tuple.
    The framework removes that unused field automatically.
    """
    fields = dict(B="A -> ^B C", C="A -> B ^C")
    variables = dict(B=FC.Kinematics())

    tuple = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )

    assert tuple.properties["decay_descriptors"] == ["A -> ^B C"]
    assert tuple.properties["thor_functor_field_names"] == [
        ["M", "P", "PT", "PX", "PY", "PZ", "ENERGY"]
    ]
    assert len(tuple.properties["thor_functors"]) == 1
    assert len(tuple.properties["thor_functors"][0]) == 7


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_field_name_unmatched_to_variable_keys(func_name, name_argument, type_argument):
    """
    All field names should match the particles to be tupled.
    In this case the field name 'NotInVariablesKey' does not correspond to any particle
    as defined by the keys in 'variables', and a RuntimeError is raised.
    """
    fields = dict(NotInVariablesKey="A -> B C")
    variables = dict(A=FC.Kinematics())

    with pytest.raises(RuntimeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=fields,
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_is_dict(func_name, name_argument, type_argument):
    """
    A TypeError is raised if the 'variables' argument is not a dict.
    """
    with pytest.raises(TypeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=fields,
            variables=FC.Kinematics(),  # should be a dict
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_key_ALL(func_name, name_argument, type_argument):
    """
    Check that the reserved field name 'ALL' is accepted as a key to 'variables'.
    """
    variables = dict(ALL=FC.Kinematics())

    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_all_type_FunctorCollection(func_name, name_argument, type_argument):
    """
    Check that the 'variables' (values) are all of type FunctorCollection.
    """
    fields = dict(B="A -> ^B C", C="A -> B ^C")
    variables = {
        "B": FunctorCollection({"THOR_FourMom_P": F.FOURMOMENTUM}),
        "C": {"THOR_FourMom_P": F.FOURMOMENTUM},
    }

    with pytest.raises(TypeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=fields,
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_key_ALL_type_FunctorCollection(
    func_name, name_argument, type_argument
):
    """
    Check complementary to the above one for the reserved name 'ALL':
    the variables for the field name 'ALL' need to be of type FunctorCollection.
    """
    variables = {"ALL": {"THOR_FourMom_P": F.FOURMOMENTUM}}

    with pytest.raises(TypeError):
        _ = getattr_wrapper(
            this_module,
            func_name,
            name=name_argument + "_{hash}",
            tuple_name=name_argument,
            fields=fields,
            variables=variables,
            inputs=data_handle(type_argument),
        )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_arrayindex(func_name, name_argument, type_argument):
    """
    Test the checking modules of FunTuple for what concerns array variables,
    see the module `common_util.py` and in particular the `conduct_checks_and_transform`
    and `prepend_fieldname_to_arrayindex` helper functions.
    This test passes all checks, see further related tests below.
    """
    reset_global_store()
    fields = dict(A="A -> B C", B="A -> ^B C", C="A -> B ^C")
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ), reconstruction.bind(input_process="Spruce"):
        v2_pvs = get_pvs()
        variables = {
            "B": FunctorCollection({"THOR_FourMom_P": F.FOURMOMENTUM}),
            "ALL": FunctorCollection(
                {
                    "BPVIPCHI2[nPVs]": F.BPVIPCHI2(v2_pvs),
                }
            ),
        }

    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        fields=fields,
        variables=variables,
        inputs=data_handle(type_argument),
    )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_arrayindex_ValueError_1(func_name, name_argument, type_argument):
    """
    Test the checking modules of FunTuple for what concerns array variables,
    see the module `common_util.py` and in particular the `conduct_checks_and_transform`
    and `prepend_fieldname_to_arrayindex` helper functions.
    This test fails due to a badly configured array index.
    """
    reset_global_store()
    fields = dict(A="A -> B C", B="A -> ^B C", C="A -> B ^C")
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ), reconstruction.bind(input_process="Spruce"):
        v2_pvs = get_pvs()
        variables = {
            "B": FunctorCollection({"THOR_FourMom_P": F.FOURMOMENTUM}),
            "ALL": FunctorCollection(
                {
                    "THOR_FourMom_P": F.FOURMOMENTUM,
                    "BPVIPCHI2]": F.BPVIPCHI2(v2_pvs),
                }
            ),
        }

        with pytest.raises(ValueError):
            _ = getattr_wrapper(
                this_module,
                func_name,
                name=name_argument + "_{hash}",
                tuple_name=name_argument,
                fields=fields,
                variables=variables,
                inputs=data_handle(type_argument),
            )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name
)
def test_variables_arrayindex_ValueError_2(func_name, name_argument, type_argument):
    """
    Test the checking modules of FunTuple for what concerns array variables,
    see the module `common_util.py` and in particular the `conduct_checks_and_transform`
    and `prepend_fieldname_to_arrayindex` helper functions.
    This test fails due to a badly configured array index.
    """
    reset_global_store()
    fields = dict(A="A -> B C", B="A -> ^B C", C="A -> B ^C")
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ), reconstruction.bind(input_process="Spruce"):
        v2_pvs = get_pvs()
        variables = {
            "B": FunctorCollection({"THOR_FourMom_P": F.FOURMOMENTUM}),
            "ALL": FunctorCollection(
                {
                    "THOR_FourMom_P": F.FOURMOMENTUM,
                    "BPVIPCHI2[": F.BPVIPCHI2(v2_pvs),
                }
            ),
        }

        with pytest.raises(ValueError):
            _ = getattr_wrapper(
                this_module,
                func_name,
                name=name_argument + "_{hash}",
                tuple_name=name_argument,
                fields=fields,
                variables=variables,
                inputs=data_handle(type_argument),
            )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name_event
)
def test_ftevent_simplest(func_name, name_argument, type_argument):
    """
    Check the simplest tupling instance for FunTuple_Event.
    """
    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        inputs=data_handle(type_argument),
    )


@pytest.mark.parametrize(
    "func_name,name_argument,type_argument", list_function_tuple_name_event
)
def test_ftevent(func_name, name_argument, type_argument):
    """
    Check a tupling instance for FunTuple_Event with non-default arguments.
    """
    with default_raw_event.bind(
        raw_event_format=0.5, maker=lambda l: data_handle("LHCb::RawEvent")
    ):
        event_variables = FC.EventInfo()

    _ = getattr_wrapper(
        this_module,
        func_name,
        name=name_argument + "_{hash}",
        tuple_name=name_argument,
        variables=event_variables,
        inputs=data_handle(type_argument),
    )
