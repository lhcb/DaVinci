/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/StatusCode.h"

// custom class for error code handling (probably needs extending)
namespace LHCb::FTuple::EC {
  enum class ErrorCode : StatusCode::code_t {
    NameNumberMismatch = 10,
    NumberNotUnitary,
    MaxBuffer,
    SizeExceeded,
    TypeNotSupported,
    NoInvalidValueSupport
  };

  // declare ErrorCategory
  struct ErrorCategory : StatusCode::Category {
    [[nodiscard]] const char* name() const override;
    [[nodiscard]] bool        isRecoverable( StatusCode::code_t ) const override;
    [[nodiscard]] std::string message( StatusCode::code_t code ) const override;
  };
} // namespace LHCb::FTuple::EC

STATUSCODE_ENUM_DECL( LHCb::FTuple::EC::ErrorCode )
