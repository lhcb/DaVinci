/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "ErrorCode.h"

namespace LHCb::FTuple::EC {
  // custom class for error code handling (probably needs extending)
  const char* ErrorCategory::name() const { return "FunTuple"; }
  bool        ErrorCategory::isRecoverable( StatusCode::code_t ) const { return false; }
  std::string ErrorCategory::message( StatusCode::code_t code ) const {
    switch ( static_cast<ErrorCode>( code ) ) {
    case ErrorCode::NameNumberMismatch:
      return "Error (run_full_counter_mode=True): size of field names and number of counters do not match";
    case ErrorCode::NumberNotUnitary:
      return "Error (run_full_counter_mode=False): number of counters is not unitary";
    case ErrorCode::MaxBuffer:
      return "The size of vector exceeds the max buffer size of 10000";
    case ErrorCode::SizeExceeded:
      return "The size of vector exceeds more than 4. Tupling is not supported.";
    case ErrorCode::TypeNotSupported:
      return "Value type returned by functor not recognised. The problematic functor is shown below.";
    case ErrorCode::NoInvalidValueSupport:
      return "There is no support of a default invalid value for the specified functor. If your functor returns "
             "an \"int\", please specify a custom \"invalid_value\" by prepending \"F.VALUE_OR(invalid_value) @ "
             "...\" to your functor.";
    default:
      return StatusCode::default_category().message( code );
    }
  }
} // namespace LHCb::FTuple::EC

STATUSCODE_ENUM_IMPL( LHCb::FTuple::EC::ErrorCode, LHCb::FTuple::EC::ErrorCategory )
