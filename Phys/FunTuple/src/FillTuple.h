/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// #pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// Event -- to support various 'enum' classes
#include "Event/MCVertex.h"
#include "Event/ODIN.h"
#include "Event/State.h"
#include "Event/Tagger.h"
#include "Event/TrackEnums.h"
#include "Kernel/MCTaggingHelper.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "MCInterfaces/IMCReconstructible.h"

// custom classes
#include "InvalidValue.h"

// ThOr functor
#include "Functors/Optional.h"

// Custom error code
#include "ErrorCode.h"
#include <type_traits>

namespace LHCb::FTuple {
  // max buffer size for storing array
  constexpr unsigned int maxSizeArray{ 10000 };

  // For tupling arrays user can specify variable name (e.g. "PT") and length name ("nPVs") as "PT[nPVs]".
  // This function gets "nPVs" from fieldname, if nothing found assigns default name "indx"
  inline std::string get_length_name( const std::string& fieldname ) {
    std::string idxname   = " ";
    std::string start_del = "[";
    std::string stop_del  = "]";
    idxname               = fieldname.substr( fieldname.find( start_del ) + start_del.length(),
                                              fieldname.find( stop_del ) - ( fieldname.find( start_del ) + start_del.length() ) );
    if ( idxname == start_del || idxname == stop_del || idxname.empty() || idxname == fieldname ||
         idxname == fieldname.substr( 0, fieldname.find( stop_del ) ) ) {
      idxname = "indx";
    }
    return idxname;
  }

  // This function erases the array index name from the fieldname.
  // Needed to avoid FunTuple from creating matrices instead of arrays.
  // E.g.: if fieldname = var[nPVs], then FunTuple stores an object as var[nPVs][nPVs]
  //       removing [nPVs] from fieldname allow to store correctly the object as: var[nPVs].
  inline std::string erase_array_idxname( const std::string fieldname, const std::string& idxname ) {
    auto   full_idxname = std::string( "[" ) + idxname + std::string( "]" );
    size_t pos          = fieldname.rfind( full_idxname );

    std::string fieldname_new = fieldname;
    // Replace the whole idxname to empty string will erase it.
    if ( pos != std::string::npos ) fieldname_new.replace( pos, full_idxname.length(), "" );

    return fieldname_new;
  }

  // declare fill function
  inline StatusCode fill_( const Tuples::Tuple&, const std::string&, std::any const& );

  // default fill: just forward to column
  template <typename T>
  StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname, const T& val ) {
    return ntuple->column( colname, val );
  }

  // fill for scalar SIMD types (float_v)
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const SIMDWrapper::scalar::float_v& val ) {
    return ntuple->column( colname, val.cast() );
  }

  // fill for scalar SIMD types (int_v)
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const SIMDWrapper::scalar::int_v& val ) {
    return ntuple->column( colname, val.cast() );
  }

  // fill for uint64_t (unsigned long): Special since we cast it to unsigned long long
  // as done in TupleObj.h in Gaudi. This is to side step a platform specific warning flagged by TupleObj.cpp in
  // Gaudi. (see issue): https://gitlab.cern.ch/lhcb/DaVinci/-/issues/48 (Particularly see suggestion by Marco about
  // TupleObj.cpp methods "to use std::uint64_t but this may have unexpected effects with ROOT as they may store the
  // type of the value as the underlying type")
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname, const std::uint64_t& val ) {
    return ntuple->column( colname, static_cast<unsigned long long>( val ) );
  }

  // fill for std::vector forward to farray
  template <typename ValueType>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const std::vector<ValueType>& val ) {
    if ( val.size() > maxSizeArray ) { return StatusCode{ EC::ErrorCode::MaxBuffer }; }
    std::string array_idxName = get_length_name( colname );
    std::string fieldname     = colname;
    if ( array_idxName.c_str() ) fieldname = erase_array_idxname( colname, array_idxName );
    return ntuple->farray( fieldname, val, array_idxName, maxSizeArray );
  }

  // fill for std::vector<SIMDWrapper::scalar::float_v>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const std::vector<SIMDWrapper::scalar::float_v>& val ) {
    if ( val.size() > maxSizeArray ) return StatusCode{ EC::ErrorCode::MaxBuffer };
    std::string array_idxName = get_length_name( colname );
    std::string fieldname     = colname;
    if ( array_idxName.c_str() ) fieldname = erase_array_idxname( colname, array_idxName );
    auto cast = []( const SIMDWrapper::scalar::float_v& vali ) { return vali.cast(); };
    return ntuple->farray( fieldname, cast, std::begin( val ), std::end( val ), array_idxName, maxSizeArray );
  }

  // fill for LinAlg vector with SIMD type
  template <typename ValueType, auto N>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const LHCb::LinAlg::Vec<ValueType, N>& val ) {
    using Utils::as_arithmetic;
    if constexpr ( N == 3 ) {
      return fill_(
          ntuple, colname,
          Gaudi::XYZVector( as_arithmetic( val( 0 ) ), as_arithmetic( val( 1 ) ), as_arithmetic( val( 2 ) ) ) );
    }
    if constexpr ( N == 4 ) {
      return fill_( ntuple, colname,
                    Gaudi::LorentzVector( as_arithmetic( val( 0 ) ), as_arithmetic( val( 1 ) ),
                                          as_arithmetic( val( 2 ) ), as_arithmetic( val( 3 ) ) ) );
    }
    return StatusCode{ EC::ErrorCode::SizeExceeded };
  }

  // fill for LinAlg Vec3 with SIMD type
  template <typename ValueType>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const LHCb::LinAlg::Vec3<ValueType>& val ) {
    using Utils::as_arithmetic;
    return fill_( ntuple, colname,
                  Gaudi::XYZVector( as_arithmetic( val.X() ), as_arithmetic( val.Y() ), as_arithmetic( val.Z() ) ) );
  }

  // fill for ROOT::Math::SVector<T, N>
  template <typename ValueType, auto N>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const ROOT::Math::SVector<ValueType, N>& val ) {
    return ntuple->array( colname, val );
  }

  // fill for symmetric matrix forward it to matrix
  template <typename ValueType, auto N>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const ROOT::Math::SMatrix<ValueType, N, N, ROOT::Math::MatRepSym<ValueType, N>>& val ) {
    return ntuple->matrix( colname, val );
  }

  // fill for non-symmetric matrix forward it to matrix
  template <typename ValueType, auto N, auto M>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const ROOT::Math::SMatrix<ValueType, N, M, ROOT::Math::MatRepStd<ValueType, N, M>>& val ) {
    return ntuple->matrix( colname, val );
  }

  // fill for LinAlg sym-matrix with SIMD type
  template <typename ValueType, auto N>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const LHCb::LinAlg::MatSym<ValueType, N>& val ) {

    using cast_type = decltype( std::declval<ValueType>().cast() );
    return fill_( ntuple, colname, LHCb::LinAlg::convert<cast_type>( val ) );
  }

  // fill for LinAlg matrix with SIMD type
  template <typename ValueType, auto N, auto M>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           const LHCb::LinAlg::Mat<ValueType, N, M>& val ) {
    if constexpr ( N == M ) {
      return fill_( ntuple, colname, val.cast_to_sym() );
    } else {
      return fill_( ntuple, colname, LHCb::LinAlg::convert<ValueType>( val ) );
    }
  }

  // fill for state of a track (LHCb::State)
  // Note: This feature needs to be tested with the https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3491
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname, const LHCb::State& state ) {
    // store information related to the state location (see LHCb::State::Location)
    return fill_( ntuple, colname + "_flag", static_cast<unsigned int>( state.flags() ) )
        .andThen( [&] {
          // store information related to state vector (x, y, dx/dz, dy/dz, q/p)
          return fill_( ntuple, colname + "_statevector", state.stateVector() );
        } )
        .andThen( [&] {
          // store information related to the covariance matrix 5x5 matrix
          return fill_( ntuple, colname + "_covariance", state.covariance() );
        } )
        .andThen( [&] {
          // z location of the track state
          return fill_( ntuple, colname + "_Z", state.z() );
        } );
  }

  // fill for std::map<std::string, ValueType>
  template <typename ValueType>
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname,
                           std::map<std::string, ValueType> const& val ) {
    return std::accumulate(
        val.begin(), val.end(), StatusCode{ StatusCode::SUCCESS }, [&]( StatusCode sc, const auto& kv ) {
          return sc.andThen( [&] { return fill_( ntuple, fmt::format( "{}_{}", colname, kv.first ), kv.second ); } );
        } );
  }

  // make a map of type_index and function that calls fill function for all T and Functors::Optional<T>
  template <typename... T>
  inline auto make_map() {
    using Fun = StatusCode ( * )( const Tuples::Tuple&, const std::string&, const std::any& );
    // add map entry of type_index (basic types + few complex types) -> fill function
    auto m = std::unordered_map{ std::pair<std::type_index, Fun>{
        typeid( T ), []( const Tuples::Tuple& tuple, const std::string& column, std::any const& v ) {
          return fill_( tuple, column, std::any_cast<T const&>( v ) );
        } }... };

    // add map entry of type_index (std::optional<T>) -> fill function
    auto add_if_invalid_exists = [&m]( auto opt ) {
      using optional_type = decltype( opt );
      using value_type    = typename optional_type::value_type;
      if constexpr ( has_invalid_value_v<value_type> ) {
        m.emplace(
            typeid( optional_type ), []( const Tuples::Tuple& tuple, const std::string& column, std::any const& v ) {
              return fill_( tuple, column,
                            std::any_cast<optional_type const&>( v ).value_or( InvalidValue<value_type>::value ) );
            } );
      } else {
        m.emplace( typeid( optional_type ), []( const Tuples::Tuple&, const std::string&, std::any const& ) {
          return StatusCode{ EC::ErrorCode::NoInvalidValueSupport };
        } );
      };
    };
    ( add_if_invalid_exists( Functors::Optional<T>{} ), ... );

    // add map entry of type_index (std::map<std::string, T>) -> fill function
    auto add_map_val = [&m]( auto map ) {
      using map_type = decltype( map );
      m.emplace( typeid( map_type ), []( const Tuples::Tuple& tuple, const std::string& column, std::any const& v ) {
        return fill_( tuple, column, std::any_cast<map_type const&>( v ) );
      } );
    };
    ( add_map_val( std::map<std::string, T>{} ), ... );

    return m;
  }

  // list the supported types
  inline const auto map_valtype_callcolfun = make_map<
      bool, short int, unsigned short int, int, unsigned int, long int, unsigned long int, long long int,
      unsigned long long int, SIMDWrapper::scalar::int_v, float, double, std::uint64_t, LHCb::ODIN::BXTypes,
      LHCb::MCVertex::MCVertexType, IMCReconstructible::RecCategory, IMCReconstructed::RecCategory,
      std::vector<LHCb::FlavourTagging::originTagCodes>, LHCb::Tagger::TagResult, LHCb::Event::Enum::Track::History,
      LHCb::Event::Enum::Track::FitHistory, LHCb::Event::Enum::Track::Type, LHCb::Event::Enum::Track::FitStatus,
      LHCb::Event::Enum::Track::Flag, SIMDWrapper::scalar::float_v, Gaudi::LorentzVector, Gaudi::XYZVector,
      Gaudi::XYZPoint, Gaudi::SymMatrix3x3, Gaudi::SymMatrix4x4, Gaudi::SymMatrix5x5, std::vector<double>,
      std::vector<float>, std::vector<int>, std::vector<bool>, std::vector<unsigned int>,
      std::vector<unsigned long int>, std::vector<long long int>, std::vector<long int>,
      std::vector<SIMDWrapper::scalar::float_v>, LHCb::LinAlg::Vec<double, 3>, LHCb::LinAlg::Vec<double, 4>,
      LHCb::LinAlg::Vec<float, 3>, LHCb::LinAlg::Vec<float, 4>, LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>,
      LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>, LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3>,
      LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 4>, LHCb::LinAlg::Mat<SIMDWrapper::scalar::float_v, 3, 3>,
      LHCb::LinAlg::Mat<SIMDWrapper::scalar::float_v, 4, 4>, LHCb::LinAlg::Mat<SIMDWrapper::scalar::float_v, 4, 3>,
      LHCb::LinAlg::Vec3<double>, LHCb::LinAlg::Vec3<float>, LHCb::LinAlg::Vec3<SIMDWrapper::scalar::float_v>,
      std::map<std::string, std::any>, ROOT::Math::SVector<double, 5>, ROOT::Math::SVector<double, 6>,
      ROOT::Math::SVector<double, 7>, ROOT::Math::SVector<double, 8>, LHCb::State>();

  // fill function to loop through supported types invoking function that calls fill function
  inline StatusCode fill_( const Tuples::Tuple& ntuple, const std::string& colname, std::any const& val ) {
    auto f = map_valtype_callcolfun.find( val.type() );
    if ( f == map_valtype_callcolfun.end() ) { return StatusCode{ EC::ErrorCode::TypeNotSupported }; }
    return std::invoke( f->second, ntuple, colname, val );
  }
} // namespace LHCb::FTuple
