/***************************************************************************** \
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Producer.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// Event
#include "Event/GenerateSOATracks.h"
#include "Event/HltDecReports.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/ODIN.h"
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/SOATrackConversion.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Event/Vertex.h"
#include "Event/ZipUtils.h"

// Kernel
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"

// LoKi
#include "LoKi/IDecay.h"
#include "LoKi/IMCDecay.h"
#include "LoKi/PrintDecay.h"
#include "LoKi/PrintMCDecay.h"
#include "LoKi/Trees.h"

//// SelKernel (helper functions, remove later)
// #include "SelKernel/Utilities.h"
//

using Gaudi::Functional::Traits::useLegacyGaudiAlgorithm;

#ifndef MAKEDUMMYDATA
#  define MAKEDUMMYDATA

class MakeDummyMCParticles : public Gaudi::Functional::Producer<LHCb::MCParticles()> {
public:
  MakeDummyMCParticles( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, { "output_location", { "/Event/FunTuple/MCParticles" } } ) {}

  LHCb::MCParticles operator()() const override;

protected:
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid,
                           const int& prod1id, const int& prod2id, MsgStream& log ) const;
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid ) const;
  void checkDecay( Decays::IMCDecay::Finder& finder, LHCb::MCParticle::ConstVector& mcparticles, MsgStream& log ) const;

private:
  Gaudi::Property<std::string> m_headDecay{
      this, "decay_descriptor", "[B_s0 => (J/psi(1S) => mu+ mu- ) ^( phi(1020) => K+ K-)]CC", "Decay descriptor" };
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };
  ToolHandle<Decays::IMCDecay>              m_mcdecaytool = { this, "MCDecay", "LoKi::MCDecay" };
};

DECLARE_COMPONENT( MakeDummyMCParticles )

inline LHCb::MCParticles MakeDummyMCParticles::operator()() const {
  MsgStream log = info();
  MsgStream err = error();

  // parse/decode the decay descriptor
  const Decays::IMCDecay::Tree mcdecayTree = m_mcdecaytool->tree( m_headDecay );
  if ( !mcdecayTree ) { err << "Could not find mcdecayTree for " + m_headDecay << endmsg; }
  log << "Making dummy data with descriptor " << mcdecayTree << endmsg;

  // Define and fill container to hold the Jpsi MCParticles (only one Jpsi made) which decays to mu+ mu-
  std::vector<LHCb::MCParticle> mcparticles;
  LHCb::MCParticle              p1   = LHCb::MCParticle();
  LHCb::MCParticle              p1_a = LHCb::MCParticle();
  LHCb::MCParticle              p1_b = LHCb::MCParticle();
  LHCb::MCParticle              p2   = LHCb::MCParticle();
  LHCb::MCParticle              p2_a = LHCb::MCParticle();
  LHCb::MCParticle              p2_b = LHCb::MCParticle();
  LHCb::MCParticle              p    = LHCb::MCParticle();

  makeMCTwoBodyDecay( p1, p1_a, p1_b, 443, 13, -13, err );   // Jpis -> mu- mu+
  makeMCTwoBodyDecay( p2, p2_a, p2_b, 333, 321, -321, err ); // phi -> K+ K-
  makeMCTwoBodyDecay( p, p1, p2, 531 );                      // Bs -> Jpsi phi

  mcparticles.push_back( p1 );
  mcparticles.push_back( p1_a );
  mcparticles.push_back( p1_b );
  mcparticles.push_back( p2 );
  mcparticles.push_back( p2_a );
  mcparticles.push_back( p2_b );
  mcparticles.push_back( p );

  LHCb::MCParticle::ConstVector mcparticlescheck{ &mcparticles[mcparticles.size() - 1] };
  /*
  //Check all the particles are there
  for (const auto& mcp: mcparticlescheck) {
          log << *mcp << endmsg;
          for (const auto& mcv: (*mcp).endVertices()){
                  //log << *mcv << endmsg;
                  for (const auto& mcp1: (*mcv).products()){
                          log << *mcp1 << endmsg;
                          for (const auto& mcv1: (*mcp1).endVertices()){
                                  //log << *mcv1 << endmsg;
                                  for (const auto& mcp2: (*mcv1).products()){log << *mcp2 << endmsg;}
                          }
                  }
          }
  }
  */

  // create the decay finder
  Decays::IMCDecay::Finder finder( mcdecayTree );
  if ( !finder ) { err << "Unable to create decay finder'" << endmsg; }

  ////check that the finder can find the decay using the descriptor
  checkDecay( finder, mcparticlescheck, log );

  LHCb::MCParticles MC_particles;
  // for ( unsigned int i = 0; i < mcparticles.size(); i++ ) {MC_particles.insert(mcparticles[i], i);}
  for ( unsigned int i = 0; i < mcparticles.size(); i++ ) { MC_particles.add( &mcparticles[i] ); }
  return MC_particles;
}

inline void MakeDummyMCParticles::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2,
                                                      const int& parentid, const int& prod1id, const int& prod2id,
                                                      MsgStream& log ) const {
  using std::sqrt;

  // particle id of products
  const LHCb::ParticleID p1_id = LHCb::ParticleID( prod1id ); // mu+
  const LHCb::ParticleID p2_id = LHCb::ParticleID( prod2id ); // mu-
  p1.setParticleID( p1_id );
  p2.setParticleID( p2_id );
  // four momenta of products
  const LHCb::ParticleProperty* p1_prop = m_particlePropertySvc->find( p1_id );
  const LHCb::ParticleProperty* p2_prop = m_particlePropertySvc->find( p2_id );
  if ( !p1_prop || !p2_prop ) { log << "Could not find either p1_prop or p2_prop" << endmsg; }
  const double               p1_mass = p1_prop->mass();
  const double               p2_mass = p2_prop->mass();
  const Gaudi::XYZVector     p1_3vec{ 10.2, 10.9, 30.2 };
  const Gaudi::XYZVector     p2_3vec{ 20.5, 20.5, 60.6 };
  double                     p1_E    = sqrt( p1_3vec.Mag2() + p1_mass * p1_mass );
  double                     p2_E    = sqrt( p2_3vec.Mag2() + p2_mass * p2_mass );
  const Gaudi::LorentzVector p1_4vec = Gaudi::LorentzVector( p1_3vec.X(), p1_3vec.Y(), p1_3vec.Z(), p1_E );
  const Gaudi::LorentzVector p2_4vec = Gaudi::LorentzVector( p2_3vec.X(), p2_3vec.Y(), p2_3vec.Z(), p2_E );
  p1.setMomentum( p1_4vec );
  p2.setMomentum( p2_4vec );

  // Define origin vertex of parent (DecayVertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  const Gaudi::XYZPoint    p_orgvrtx{ 2.8, -1.5, 1.3 };
  p_orgvertx->setPosition( p_orgvrtx );
  p_orgvertx->setTime( 0.05 );
  p_orgvertx->setType( LHCb::MCVertex::DecayVertex );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{ 6.8, -4.5, 3.3 };
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.13 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of parent
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );

  // log << p << endmsg;
  // for (const auto& mcv: p.endVertices()){
  //	log << *mcv << endmsg;
  //	for (const auto& mcd: (*mcv).products()){log << *mcd << endmsg;}
  //}
}

inline void MakeDummyMCParticles::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2,
                                                      const int& parentid ) const {
  // get 4-momenta
  const Gaudi::LorentzVector p1_4vec = p1.momentum();
  const Gaudi::LorentzVector p2_4vec = p2.momentum();

  // Define origin vertex of parent (ppCollision vertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  p_orgvertx->setType( LHCb::MCVertex::ppCollision );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{ 2.8, -1.5, 1.3 };
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.05 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of p
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );
}

inline void MakeDummyMCParticles::checkDecay( Decays::IMCDecay::Finder&      finder,
                                              LHCb::MCParticle::ConstVector& mcparticles, MsgStream& log ) const {
  // make the output container
  typedef LHCb::MCParticle::ConstVector OUTPUT;
  OUTPUT                                output;

  // fill the container
  finder.findDecay( mcparticles.begin(), mcparticles.end(), output );
  // log << " found #" << output.size() << " decays" << endmsg;

  // print the marked particle in the container
  for ( const auto& p : output ) {
    log << *p << endmsg;
    // for (const auto& mcv: (*p).endVertices()){
    //	log << *mcv << endmsg;
    //	for (const auto& mcd: (*mcv).products()){log << *mcd << endmsg;}
    //}
  }

  //// print decay: Some reason the loKi services cannot import ParticlePropertyService properly
  // for ( OUTPUT::const_iterator idec = output.begin() ; output.end() != idec ; ++idec )
  //{
  //	const LHCb::MCParticle* dec = *idec ;
  //	if ( 0 == dec ) { continue ; }
  //	log << std::endl << " " << (idec-output.begin()+1) << " \t" ;
  //	LoKi::PrintMC::printDecay(dec,log,true);
  //}
  // log << endmsg ;
}

class MakeDummyParticles : public Gaudi::Functional::Producer<LHCb::Particles()> {
public:
  MakeDummyParticles( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, { "output_location", { "/Event/FunTuple/Particles" } } ) {}

  LHCb::Particles operator()() const override;

protected:
  void makeTwoBodyDecay( LHCb::Particle& p, LHCb::Particle& p1, LHCb::Particle& p2, const int& parentid,
                         const int& prod1id, const int& prod2id, MsgStream& log ) const;
  void makeTwoBodyDecay( LHCb::Particle& p, LHCb::Particle& p1, LHCb::Particle& p2, const int& parentid ) const;
  void checkDecay( Decays::IDecay::Finder& finder, LHCb::Particle::ConstVector& particles, MsgStream& log ) const;

private:
  Gaudi::Property<std::string> m_headDecay{
      this, "decay_descriptor", "[B_s0 -> (J/psi(1S) -> mu+ mu- ) ^( phi(1020) -> K+ K-)]CC", "Decay descriptor" };
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };
  ToolHandle<Decays::IDecay>                m_decaytool = { this, "Decay", "LoKi::Decay" };
};

DECLARE_COMPONENT( MakeDummyParticles )

inline LHCb::Particles MakeDummyParticles::operator()() const {
  MsgStream log = info();
  MsgStream err = error();

  // parse/decode the decay descriptor
  const Decays::IDecay::Tree decayTree = m_decaytool->tree( m_headDecay );
  if ( !decayTree ) { err << "Could not find decayTree for " + m_headDecay << endmsg; }
  log << "Making dummy data with descriptor " << decayTree << endmsg;

  // Define and fill container to hold the Jpsi Particles (only one Jpsi made) which decays to mu+ mu-
  std::vector<LHCb::Particle> particles;
  LHCb::Particle              p1   = LHCb::Particle();
  LHCb::Particle              p1_a = LHCb::Particle();
  LHCb::Particle              p1_b = LHCb::Particle();
  LHCb::Particle              p2   = LHCb::Particle();
  LHCb::Particle              p2_a = LHCb::Particle();
  LHCb::Particle              p2_b = LHCb::Particle();
  LHCb::Particle              p    = LHCb::Particle();

  makeTwoBodyDecay( p1, p1_a, p1_b, 443, 13, -13, err );   // Jpis -> mu- mu+
  makeTwoBodyDecay( p2, p2_a, p2_b, 333, 321, -321, err ); // phi -> K+ K-
  makeTwoBodyDecay( p, p1, p2, 531 );                      // Bs -> Jpsi phi

  particles.push_back( p1 );
  particles.push_back( p1_a );
  particles.push_back( p1_b );
  particles.push_back( p2 );
  particles.push_back( p2_a );
  particles.push_back( p2_b );
  particles.push_back( p );

  LHCb::Particle::ConstVector particlescheck{ &particles[particles.size() - 1] };
  /*
  //Check all the particles are there
  for (const auto& p: particlescheck) {
          log << *p << endmsg;
          for (const auto& v: (*p).endVertices()){
                  //log << *v << endmsg;
                  for (const auto& p1: (*v).products()){
                          log << *p1 << endmsg;
                          for (const auto& v1: (*p1).endVertices()){
                                  //log << *v1 << endmsg;
                                  for (const auto& p2: (*v1).products()){log << *p2 << endmsg;}
                          }
                  }
          }
  }
  */

  // create the decay finder
  Decays::IDecay::Finder finder( decayTree );
  if ( !finder ) { err << "Unable to create decay finder'" << endmsg; }

  ////check that the finder can find the decay using the descriptor
  checkDecay( finder, particlescheck, log );

  LHCb::Particles p_particles;
  // for ( unsigned int i = 0; i < particles.size(); i++ ) {p_particles.insert(particles[i], i);}
  for ( unsigned int i = 0; i < particles.size(); i++ ) { p_particles.add( &particles[i] ); }
  return p_particles;
}

inline void MakeDummyParticles::makeTwoBodyDecay( LHCb::Particle& p, LHCb::Particle& p1, LHCb::Particle& p2,
                                                  const int& parentid, const int& prod1id, const int& prod2id,
                                                  MsgStream& log ) const {
  using std::sqrt;

  // particle id of p1 and p2
  const LHCb::ParticleID p1_id = LHCb::ParticleID( prod1id ); // mu+
  const LHCb::ParticleID p2_id = LHCb::ParticleID( prod2id ); // mu-
  p1.setParticleID( p1_id );
  p2.setParticleID( p2_id );
  // mass and four momenta of p1 and p2
  const LHCb::ParticleProperty* p1_prop = m_particlePropertySvc->find( p1_id );
  const LHCb::ParticleProperty* p2_prop = m_particlePropertySvc->find( p2_id );
  if ( !p1_prop || !p2_prop ) { log << "Could not find either p1_prop or p2_prop" << endmsg; }
  const double p1_mass = p1_prop->mass();
  const double p2_mass = p2_prop->mass();
  p1.setMeasuredMass( p1_mass );
  p2.setMeasuredMass( p2_mass );
  // set momentum of p1 and p2
  const Gaudi::XYZVector     p1_3vec{ 10.2, 10.9, 30.2 };
  const Gaudi::XYZVector     p2_3vec{ 20.5, 20.5, 60.6 };
  double                     p1_E    = sqrt( p1_3vec.Mag2() + p1_mass * p1_mass );
  double                     p2_E    = sqrt( p2_3vec.Mag2() + p2_mass * p2_mass );
  const Gaudi::LorentzVector p1_4vec = Gaudi::LorentzVector( p1_3vec.X(), p1_3vec.Y(), p1_3vec.Z(), p1_E );
  const Gaudi::LorentzVector p2_4vec = Gaudi::LorentzVector( p2_3vec.X(), p2_3vec.Y(), p2_3vec.Z(), p2_E );
  p1.setMomentum( p1_4vec );
  p2.setMomentum( p2_4vec );
  // Set momentum reference point for p1 and p2
  const Gaudi::XYZPoint p_endvrtx{ 6.8, -4.5, 3.3 };
  p1.setReferencePoint( p_endvrtx );
  p2.setReferencePoint( p_endvrtx );

  // set properties of parent
  // particleid
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  // mass
  const LHCb::ParticleProperty* p_prop = m_particlePropertySvc->find( p_id );
  const double                  p_mass = p_prop->mass();
  p.setMeasuredMass( p_mass );
  // momentum
  p.setMomentum( p1_4vec + p2_4vec );
  // momentum ref pt
  const Gaudi::XYZPoint p_orgvrtx{ 2.8, -1.5, 1.3 };
  p.setReferencePoint( p_orgvrtx );
  // set daughters
  SmartRefVector<LHCb::Particle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  p.setDaughters( prods_endvertx );
  // set end vertex
  SmartRef<LHCb::Vertex> p_endvertx = new LHCb::Vertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTechnique( LHCb::Vertex::Unconstrained );
  p_endvertx->setOutgoingParticles( prods_endvertx ); // products in this vertex (particle 1 and 2)
  p.setEndVertex( p_endvertx );

  // log << p << endmsg;
  // for (const auto& v: p.endVertices()){
  //	log << *v << endmsg;
  //	for (const auto& d: (*v).products()){log << *d << endmsg;}
  //}
}

inline void MakeDummyParticles::makeTwoBodyDecay( LHCb::Particle& p, LHCb::Particle& p1, LHCb::Particle& p2,
                                                  const int& parentid ) const {
  // set properties of p
  // particleid
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  // mass
  const LHCb::ParticleProperty* p_prop = m_particlePropertySvc->find( p_id );
  const double                  p_mass = p_prop->mass();
  p.setMeasuredMass( p_mass );
  // momentum
  const Gaudi::LorentzVector p1_4vec = p1.momentum();
  const Gaudi::LorentzVector p2_4vec = p2.momentum();
  p.setMomentum( p1_4vec + p2_4vec );
  // momentum ref pt
  const Gaudi::XYZPoint p_orgvrtx{ 0.1, 0.1, 0.1 };
  p.setReferencePoint( p_orgvrtx );
  // set daughters
  SmartRefVector<LHCb::Particle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  p.setDaughters( prods_endvertx );
  // set end vertex
  const Gaudi::XYZPoint  p_endvrtx{ 2.8, -1.5, 1.3 };
  SmartRef<LHCb::Vertex> p_endvertx = new LHCb::Vertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTechnique( LHCb::Vertex::Unconstrained );
  p_endvertx->setOutgoingParticles( prods_endvertx ); // products in this vertex (particle 1 and 2)
  p.setEndVertex( p_endvertx );
}

inline void MakeDummyParticles::checkDecay( Decays::IDecay::Finder& finder, LHCb::Particle::ConstVector& particles,
                                            MsgStream& log ) const {
  // make the output container
  typedef LHCb::Particle::ConstVector OUTPUT;
  OUTPUT                              output;

  // fill the container
  finder.findDecay( particles.begin(), particles.end(), output );
  log << " found #" << output.size() << " decays" << endmsg;

  // print the marked particle in the container
  for ( const auto& p : output ) {
    log << *p << endmsg;
    // for (const auto& v: (*p).endVertices()){
    //	log << *v << endmsg;
    //	for (const auto& d: (*v).products()){log << *d << endmsg;}
    //}
  }

  //// print decay: Some reason the loKi services cannot import ParticlePropertyService properly
  // for ( OUTPUT::const_iterator idec = output.begin() ; output.end() != idec ; ++idec )
  //{
  //	const LHCb::Particle* dec = *idec ;
  //	if ( 0 == dec ) { continue ; }
  //	log << std::endl << " " << (idec-output.begin()+1) << " \t" ;
  //	LoKi::Print::printDecay(dec,log,true);
  //}
  // log << endmsg ;
}

class MakeODIN : public Gaudi::Functional::Producer<LHCb::ODIN()> {
public:
  MakeODIN( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, { "output", { "Event/DAQ/ODIN" } } ) {}

  LHCb::ODIN operator()() const override {
    LHCb::ODIN a{};
    a.setEventNumber( 200 );
    a.setRunNumber( 5000 );
    return a;
  }
};

DECLARE_COMPONENT( MakeODIN )

class MakeHltDecReports : public Gaudi::Functional::Producer<LHCb::HltDecReports()> {
public:
  MakeHltDecReports( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, { "output", { "Event/Hlt/DecReports" } } ) {}

  LHCb::HltDecReports operator()() const override {
    LHCb::HltDecReports::Container map;
    map.insert( std::make_pair( "Line1", LHCb::HltDecReport{ 0 } ) );
    map.insert( std::make_pair( "Line2", LHCb::HltDecReport{ 1 } ) );
    map.insert( std::make_pair( "Line3", LHCb::HltDecReport{ 1 } ) );
    LHCb::HltDecReports rep;
    rep.setDecReports( map );
    return rep;
  }
};

DECLARE_COMPONENT( MakeHltDecReports )

namespace MuonTag          = LHCb::Event::v2::Muon::Tag;
namespace ChargedBasicsTag = LHCb::Event::ChargedBasicsTag;
using MuonStatusMasks      = LHCb::Event::v2::Muon::StatusMasks;
using MuonFlags            = LHCb::Event::flags_v<SIMDWrapper::scalar::types, MuonStatusMasks>;

using output_t = std::tuple<LHCb::Event::ChargedBasics, std::unique_ptr<LHCb::Event::v2::Muon::PIDs>,
                            std::unique_ptr<LHCb::Event::v3::Tracks>>;

class ChargedBasicsProducer
    : public Gaudi::Functional::MultiTransformer<output_t( EventContext const&, LHCb::UniqueIDGenerator const& ),
                                                 useLegacyGaudiAlgorithm> {
public:
  ChargedBasicsProducer( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator,
                          { // input
                            KeyValue{ "InputUniqueIDGenerator", LHCb::UniqueIDGeneratorLocation::Default } },
                          { // output
                            KeyValue{ "Particles", "" }, KeyValue{ "MuonPIDs", "" }, KeyValue{ "Tracks", "" } } ) {}
  StatusCode initialize() override {
    this->info() << "Initialising ChargedBasicsProducer" << endmsg;

    // initialise multi transformer
    auto sc = MultiTransformer::initialize();
    if ( sc.isFailure() ) { return sc; }

    // create map long form -> short form that ParticlePropertySvc can understand
    const std::map<std::string, std::string> id_to_descriptor = {
        { "pion", "pi+" }, { "kaon", "K+" }, { "muon", "mu+" }, { "electron", "e+" }, { "proton", "p+" } };

    // check if the specified particle exists in predefined map
    const auto result = id_to_descriptor.find( m_particleid.value() );
    if ( result == id_to_descriptor.end() ) { return Error( "Unknown ParticleID value: " + m_particleid.value() ); }

    // get the long form recognised by ParticlePropertySvc
    auto descriptor = ( *result ).second;

    // get particle property
    m_particle_prop = m_particlePropertySvc->find( descriptor );

    if ( !m_particle_prop ) {
      return Error( "Could not find ParticleProperty for " + m_particleid.value() + ", descriptor " + descriptor );
    }
    // get anti particle property
    m_antiparticle_prop = m_particle_prop->antiParticle();

    return sc;
  }

  output_t operator()( EventContext const& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen ) const override {
    this->info() << "Executing ChargedBasicsProducer" << endmsg;

    auto                       zn        = Zipping::generateZipIdentifier();
    auto                       muon_pids = std::make_unique<LHCb::Event::v2::Muon::PIDs>( zn );
    auto                       tracks    = std::make_unique<LHCb::Event::v3::Tracks>( LHCb::Event::v3::generate_tracks(
        m_nTracks, unique_id_gen, m_eventCount.value(), zn, LHCb::getMemResource( evtCtx ) ) );
    LHCb::Event::ChargedBasics chargedbasics{ tracks.get(), muon_pids.get() };

    /// zip the SOA collection and loop through all the tracks
    for ( const auto& track : tracks->scalar() ) {

      auto part = chargedbasics.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

      /// set track
      part.field<ChargedBasicsTag::Track>().set( track.offset() );

      /// Get the (anti)particle property according charge of the track
      const LHCb::ParticleProperty* prop = nullptr;
      if ( track.charge() == m_particle_prop->charge() ) {
        prop = m_particle_prop;
        ++m_nparticles;
      } else if ( track.charge() == m_antiparticle_prop->charge() ) {
        prop = m_antiparticle_prop;
        ++m_nantiparticles;
      }

      /// set RICHPIDCode
      part.field<ChargedBasicsTag::RichPIDCode>().set( 0 );

      /// set Status (?) and Chi2Corr (?)
      auto mu_pid = muon_pids->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
      part.field<ChargedBasicsTag::MuonPID>().set( mu_pid.offset() );
      mu_pid.field<MuonTag::Status>().set( MuonFlags( 0 ) );
      mu_pid.field<MuonTag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );

      /// set mass
      part.field<ChargedBasicsTag::Mass>().set( prop->mass() );

      /// set particle id
      part.field<ChargedBasicsTag::ParticleID>().set( prop->particleID().pid() );

      /// set combDLL related quantities
      part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p ).set( std::numeric_limits<float>::lowest() );
      part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e ).set( std::numeric_limits<float>::lowest() );
      part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi ).set( std::numeric_limits<float>::lowest() );
      part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K ).set( std::numeric_limits<float>::lowest() );
      part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu ).set( std::numeric_limits<float>::lowest() );
    }

    // event counter
    ++m_eventCount;

    return std::make_tuple( std::move( chargedbasics ), std::move( muon_pids ), std::move( tracks ) );
  }

private:
  /// particle property service
  // LHCb::IParticlePropertySvc* m_particlePropertySvc = nullptr;
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };

  /// Particle property information for matter particles to be created
  const LHCb::ParticleProperty* m_particle_prop = nullptr;

  /// Particle property information for antimatter particles to be created
  const LHCb::ParticleProperty* m_antiparticle_prop = nullptr;

  /// Set the ID of the particle
  Gaudi::Property<std::string> m_particleid{ this, "ParticleID", "UNDEFINED",
                                             "Particle species hypothesis to apply to each created object." };

  /// Number of tracks to generate
  Gaudi::Property<std::size_t> m_nTracks{ this, "NumberToGenerate", 10, "Number of objects to generate" };

  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{ this, "Nb created particles" };

  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{ this, "Nb created anti-particles" };

  /// Event counter
  mutable Gaudi::Accumulators::Counter<> m_eventCount{ this, "Event" };
};

DECLARE_COMPONENT( ChargedBasicsProducer )

#endif /*MEMORY*/
