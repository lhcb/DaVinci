/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// std lib
#include <iostream>
#include <vector>

// ThOr functor
#include "Functors/FunctorDesc.h"

namespace ParticleProp {
  // define a struct to hold functor tupling properties for loki
  struct FunctorProp_loki {
    std::string Func_loki{ "" };            // LoKi Functor name
    std::string Func_fieldname_loki{ "" };  // LoKi Functor TBranch name
    std::string Func_returntype_loki{ "" }; // LoKi Functor return type
  };

  // define a struct to hold functor tupling properties for thor
  struct FunctorProp_thor {
    ThOr::FunctorDesc* Func_thor = nullptr;        // ThOr Functor (NB: Not name)
    std::string        Func_fieldname_thor{ "" };  // ThOr Functor TBranch name
    std::string        Func_returntype_thor{ "" }; // ThOr Functor return type
  };
} // namespace ParticleProp

class ParticleTupleProp {
public:
  ParticleTupleProp() {}
  // set functions
  void setFieldName( std::string fieldname ) { m_fieldname = fieldname; }
  void setDecayDescriptor( std::string decaydescriptor ) { m_decaydescriptor = decaydescriptor; }
  // setter for loki
  void setFunctorProps_loki( std::vector<ParticleProp::FunctorProp_loki> tupleprops ) {
    m_tupleprops_loki = tupleprops;
  }
  void setFunctorProps_loki( std::vector<std::string>& functors, std::vector<std::string>& funcfieldnames,
                             std::vector<std::string>& funcreturntypes ) {
    for ( unsigned int i = 0; i < functors.size(); i++ ) {
      ParticleProp::FunctorProp_loki tupleprop;
      tupleprop.Func_loki            = functors[i];
      tupleprop.Func_fieldname_loki  = funcfieldnames[i];
      tupleprop.Func_returntype_loki = funcreturntypes[i];
      m_tupleprops_loki.push_back( tupleprop );
    }
  }

  // setter for thor
  void setFunctorProps_thor( std::vector<ParticleProp::FunctorProp_thor> tupleprops ) {
    m_tupleprops_thor = tupleprops;
  }
  void setFunctorProps_thor( std::vector<ThOr::FunctorDesc>& functors, std::vector<std::string>& funcfieldnames,
                             std::vector<std::string>& funcreturntypes ) {
    for ( unsigned int i = 0; i < functors.size(); i++ ) {
      ParticleProp::FunctorProp_thor tupleprop;
      tupleprop.Func_thor            = &functors[i];
      tupleprop.Func_fieldname_thor  = funcfieldnames[i];
      tupleprop.Func_returntype_thor = funcreturntypes[i];
      m_tupleprops_thor.push_back( tupleprop );
    }
  }

  // get functions
  const std::string&                                 FieldName() const { return m_fieldname; }
  const std::string&                                 DecayDescriptor() const { return m_decaydescriptor; }
  const std::vector<ParticleProp::FunctorProp_loki>& FunctorProps_loki() const { return m_tupleprops_loki; }
  const std::vector<ParticleProp::FunctorProp_thor>& FunctorProps_thor() const { return m_tupleprops_thor; }
  // check and validate
  bool checks() {
    // check if field name is empty
    if ( m_fieldname.empty() ) { return ErrorMsg( "Error field name is empty" ); }
    // check if decay descriptor is empty
    if ( m_decaydescriptor.empty() ) { return ErrorMsg( "Error decay descriptor is empty" ); }
    // check if loki functor is empty
    if ( !m_tupleprops_loki.empty() ) {
      int count( 0 );
      for ( const auto& mtp : m_tupleprops_loki ) {
        if ( mtp.Func_loki.empty() ) { return ErrorMsg( "LoKi functor name is empty at " + std::to_string( count ) ); }
        if ( mtp.Func_fieldname_loki.empty() ) {
          return ErrorMsg( "Functor fieldname for Loki are empty at " + std::to_string( count ) );
        }
        if ( mtp.Func_returntype_loki.empty() ) {
          return ErrorMsg( "Functor return type for Loki is empty at " + std::to_string( count ) );
        }
        count++;
      }
    }
    // check if thor functor is empty
    if ( !m_tupleprops_thor.empty() ) {
      int count( 0 );
      for ( const auto& mtp : m_tupleprops_thor ) {
        if ( mtp.Func_thor == nullptr ) { return ErrorMsg( "ThOr functor is NULL at " + std::to_string( count ) ); }
        if ( mtp.Func_fieldname_thor.empty() ) {
          return ErrorMsg( "Functor fieldname for Thor are empty at " + std::to_string( count ) );
        }
        if ( mtp.Func_returntype_thor.empty() ) {
          return ErrorMsg( "Functor return type for Thor is empty at " + std::to_string( count ) );
        }
        count++;
      }
    }
    return true;
  }

private:
  std::string                                 m_fieldname{ "" };
  std::string                                 m_decaydescriptor{ "" };
  std::vector<ParticleProp::FunctorProp_loki> m_tupleprops_loki;
  std::vector<ParticleProp::FunctorProp_thor> m_tupleprops_thor;

  // Print error message and return false, only used by checks() method of this class
  // Better exception handling in this class can be implemented however this is simply an overkill
  // FunTuple anyway has throws appropriate exceptions if the checks() method returns false
  bool ErrorMsg( std::string stg ) { // print error message
    std::cout << stg << std::endl;
    return false;
  }
};
