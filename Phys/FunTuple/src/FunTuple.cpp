/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"

// Event
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/Particle_v2.h"

// LoKi
#include "LoKi/IDecay.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/IMCDecay.h"
#include "LoKi/IMCHybridFactory.h"

// Kernel
#include "Kernel/SynchronizedValue.h"

// boost
#include <boost/algorithm/string/join.hpp>

// stdlib
#include <deque>

// custom classes
#include "ParticleTupleProp.h"

// ThOr functor
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Functors/Optional.h"

// DetDesc
#include "DetDesc/GenericConditionAccessorHolder.h"

// decay finder
#include "Kernel/IDecayFinder.h"
#include "Kernel/Tree.h"

// Fill tuple method
#include "FillTuple.h"

namespace LHCb {
  // forward declare class InputTypeSignature
  template <typename T1>
  struct InputTypeSignature;

  namespace FTuple {
    // typedef of Consumer algorithm
    template <class T>
    using Consumer = Gaudi::Functional::Consumer<
        void( const T& ), Gaudi::Functional::Traits::BaseClass_t<LHCb::DetDesc::AlgorithmWithCondition<GaudiTupleAlg>>>;

    // typedefs for different containers of strings (to be used in definition of ParticleTupleProp objects)
    using vec_strings_t     = std::vector<std::string>;
    using vec_vec_strings_t = std::vector<vec_strings_t>;

    // typedefs to be used as input signature in making container of thor functors specified as GaudiProperty
    using vec_funcdesc_t     = std::vector<ThOr::FunctorDesc>;
    using vec_vec_funcdesc_t = std::vector<vec_funcdesc_t>;
    using map_funcdesc_t     = std::map<std::string, ThOr::FunctorDesc>;

    // typdefs to be used as input signature in making container of "instantiated" thor functors
    template <typename InputSignature>
    using vec_functor_t = std::vector<Functors::Functor<InputSignature>>;
    template <typename InputSignature>
    using vec_vec_functor_t = std::vector<vec_functor_t<InputSignature>>;
    template <typename InputSignature>
    using map_functor_t = std::map<std::string, Functors::Functor<InputSignature>>;

    // typedefs for enabling/disabling functions based on input type
    template <class T1>
    using en_if_v1 = typename std::enable_if_t<LHCb::InputTypeSignature<T1>::is_v1, bool>;
    template <class T1>
    using en_if_v2 = typename std::enable_if_t<!LHCb::InputTypeSignature<T1>::is_v1, bool>;
    template <class T1>
    using retype = typename LHCb::InputTypeSignature<T1>::FindCandReturnType;
  } // namespace FTuple

  // Class defining input type signatures to be used by ThOr and loki
  template <typename T1>
  struct InputTypeSignature {
    using InputType = T1;
    // return signature of find_candidates function
    using FindCandReturnType = T1;
    // ThOr related
    using ThOrItemType =
        typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar,
                                         InputType>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
    using ThOrSignature     = std::any( ThOrItemType const& );
    using ThOrSignatureVoid = std::any();
    // Loki related (for v2 LoKi functors do not work, so the below properties are completely non-sense)
    // Set them to the non-sense value below so that the loki related code doesn't break
    // Eventually just remove LoKi stuff
    using LoKiFactory  = LoKi::IHybridFactory;
    using LoKiFunType  = LoKi::Types::Fun;
    using LoKiItemType = LHCb::Particle;
    using LoKiIDecay   = Decays::IDecay;
    inline static const std::string LoKiIDecayName{ "Decay" };
    inline static const std::string LoKiFactoryString{ "LoKi::Hybrid::Tool/HybridFactory:PUBLIC" };
    inline static const bool        is_v1 = false;
  };

  // Class defining input type signatures to be used by ThOr and loki: specialisation for particles
  template <>
  struct InputTypeSignature<LHCb::Particle::Range> {
    using InputType = LHCb::Particle::Range;
    // return signature of find_candidates function
    using FindCandReturnType = std::vector<LHCb::Particle::ConstVector>;
    // ThOr related
    using ThOrItemType      = LHCb::Particle;
    using ThOrSignature     = std::any( ThOrItemType const& );
    using ThOrSignatureVoid = std::any();
    // Loki related
    using LoKiFactory  = LoKi::IHybridFactory;
    using LoKiFunType  = LoKi::Types::Fun;
    using LoKiItemType = LHCb::Particle;
    using LoKiIDecay   = Decays::IDecay;
    inline static const std::string LoKiIDecayName{ "Decay" };
    inline static const std::string LoKiFactoryString{ "LoKi::Hybrid::Tool/HybridFactory:PUBLIC" };
    inline static const bool        is_v1 = true;
  };

  // Class defining input type signatures to be used by ThOr and loki: specialisation for mc particles
  template <>
  struct InputTypeSignature<LHCb::MCParticle::Range> {
    using InputType = LHCb::MCParticle::Range;
    // return signature of find_candidates function
    using FindCandReturnType = std::vector<LHCb::MCParticle::ConstVector>;
    // ThOr related
    using ThOrItemType      = LHCb::MCParticle;
    using ThOrSignature     = std::any( ThOrItemType const& );
    using ThOrSignatureVoid = std::any();
    // Loki related
    using LoKiFactory  = LoKi::IMCHybridFactory;
    using LoKiFunType  = LoKi::Types::MCFun;
    using LoKiItemType = LHCb::MCParticle;
    using LoKiIDecay   = Decays::IMCDecay;
    inline static const std::string LoKiIDecayName{ "MCDecay" };
    inline static const std::string LoKiFactoryString{ "LoKi::Hybrid::MCTool/MCHybridFactory:PUBLIC" };
    inline static const bool        is_v1 = true;
  };
} // namespace LHCb

// Definition of the FunTupleBase class
template <class T>
class FunTupleBase : public LHCb::FTuple::Consumer<T> {

public:
  FunTupleBase( const std::string& name, ISvcLocator* pSvc );
  StatusCode initialize() override;
  void       operator()( const T& particles ) const override;

protected:
  // functions for conducting checks in initialise method
  StatusCode checks();
  StatusCode checks_with_vector_sizes( const int& fieldnames_size, const int& functors_size,
                                       const int& funcfieldnames_size, const int& returntypes_size,
                                       const std::string& functor_type ) const;

  // functions used in the definition of ParticleTupleProp objects
  void       fill_returntypes( const LHCb::FTuple::vec_vec_strings_t& funcreturntypes,
                               const LHCb::FTuple::vec_vec_strings_t& funcfieldnames_suffix,
                               LHCb::FTuple::vec_vec_strings_t&       vec_returntypes );
  StatusCode setParticleTupleProps();

  // functions for instantiating functors and counters
  StatusCode instantiate_loki();
  StatusCode instantiate_thor();
  StatusCode instantiate_dynamic_counters(); // Instantiation of the elements of Gaudi counters std::deque

  // function for preparing void thor
  StatusCode
  prepare_thor_void( LHCb::FTuple::map_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignatureVoid>& map_fun );

  // function for preparing non-void thor
  StatusCode
  prepare_thor( LHCb::FTuple::vec_vec_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignature>& vec_vec_fun );

  template <typename F, typename H>
  StatusCode prepare_loki( F& fun_loki, std::vector<std::vector<F>>& func_loki_vec, H& factory_loki );

  // find candidates for v1
  template <typename ObjType = T, LHCb::FTuple::en_if_v1<ObjType> = true>
  LHCb::FTuple::retype<ObjType> find_candidates( const ObjType& particles ) const;

  // find candidates for v2
  template <typename ObjType = T, LHCb::FTuple::en_if_v2<ObjType> = true>
  LHCb::FTuple::retype<ObjType> find_candidates( const ObjType& particles ) const;

  // writing tuple for v1
  template <typename ObjType = T, LHCb::FTuple::en_if_v1<ObjType> = true>
  void write_tuple( const LHCb::FTuple::retype<T>& vector_of_cands ) const;

  // writing tuple for v2
  template <typename ObjType = T, LHCb::FTuple::en_if_v2<ObjType> = true>
  void write_tuple( const LHCb::FTuple::retype<T>& vector_of_cands ) const;

  // functions for getting output of functors
  auto get_loki_output( const typename LHCb::InputTypeSignature<T>::LoKiItemType& cand, const unsigned int& nfield,
                        const unsigned int& nfunctor ) const {
    return m_fun_loki[nfield][nfunctor]( &cand );
  }
  auto get_thor_output( const typename LHCb::InputTypeSignature<T>::ThOrItemType& cand, const unsigned int& nfield,
                        const unsigned int& nfunctor ) const {
    auto prep = m_fun_thor[nfield][nfunctor].prepare();
    return prep( cand );
  }

  // function for filling void functors
  void fill_void_func_output( const Tuples::Tuple& ntuple ) const;

  // function for filling multiple candidate info
  void fill_multiple_cand_info( const Tuples::Tuple& ntuple, const std::size_t totcand,
                                const unsigned int icand ) const;

  // function for filling loki functors
  void fill_loki_func_output( const typename LHCb::InputTypeSignature<T>::LoKiItemType& cand,
                              const unsigned int& nfield, const Tuples::Tuple& ntuple ) const;

  // function for filling thor functors
  void fill_thor_func_output( const typename LHCb::InputTypeSignature<T>::ThOrItemType& cand,
                              const unsigned int& nfield, const Tuples::Tuple& ntuple ) const;

  // parse the decay descriptors for all the fields
  StatusCode parse_decay_descriptors();

private:
  // tuple name
  Gaudi::Property<std::string> m_ntupleTname{ this, "tuple_name", "DecayTree", "Set TTree name" };

  // vector of ParticleTupleProp custom objects.
  std::vector<ParticleTupleProp> m_particletupleprops;

  // field name vector
  Gaudi::Property<LHCb::FTuple::vec_strings_t> m_fieldnames_prefix{
      this, "field_names_prefix", LHCb::FTuple::vec_strings_t(), "List of TBranch name prefixes" };

  // decay descriptors
  Gaudi::Property<LHCb::FTuple::vec_strings_t> m_decaydescriptors{
      this, "decay_descriptors", LHCb::FTuple::vec_strings_t(), "List of DecayDescriptors" };

  // LoKi functors (code: string)
  Gaudi::Property<LHCb::FTuple::vec_vec_strings_t> m_functors_loki{
      this, "loki_functors", LHCb::FTuple::vec_vec_strings_t(), "Container of list of LoKi functor names" };

  // TBranch name for loki
  Gaudi::Property<LHCb::FTuple::vec_vec_strings_t> m_funcfieldnames_suffix_loki{
      this, "loki_functor_field_names", LHCb::FTuple::vec_vec_strings_t(),
      "Container of list of LoKi functor TBranch names" };

  // user specified precision to cast loki functor output (currently this not available since Gaudi needs to allow this)
  Gaudi::Property<LHCb::FTuple::vec_vec_strings_t> m_funcreturntypes_loki{
      this, "loki_functor_return_types", LHCb::FTuple::vec_vec_strings_t(),
      "Container of list of LoKi functor return types" };
  LHCb::FTuple::vec_vec_strings_t m_returntypes_loki;

  // loki preamble
  Gaudi::Property<std::vector<std::string>> m_preamble_def_loki{
      this, "loki_preamble", {}, "List of preamble strings" };
  std::string m_preamble_loki;

  /// boolean to store multiple candidate information (default: false)
  Gaudi::Property<bool> m_store_multiple_cand_info{ this, "store_multiple_cand_info", false,
                                                    "Store the candidate index and the total number of candidates" };

  //// Loki functor factory
  ToolHandle<typename LHCb::InputTypeSignature<T>::LoKiFactory> m_factory_loki = {
      LHCb::InputTypeSignature<T>::LoKiFactoryString, this };

  // the new decay finder
  ToolHandle<IDecayFinder> m_decay_finder = { this, "DecayFinder", "DecayFinder" };

  // flags to check if any loki or thor functors are empty or not
  bool m_all_empty_lokifuncs{ true };
  bool m_all_empty_thorfuncs{ true };

  //// Loki functors:
  typename LHCb::InputTypeSignature<T>::LoKiFunType m_vfun_loki{
      LoKi::Constant<const typename LHCb::InputTypeSignature<T>::LoKiItemType*, double>( -1.0e+10 ) };

  // Loki Vector of functors
  std::vector<std::vector<typename LHCb::InputTypeSignature<T>::LoKiFunType>> m_fun_loki;

  //// LoKi decay descriptor matching tools
  // ToolHandle<Decays::IDecay>   m_decaytool_loki   = {this, "Decay", "LoKi::Decay"};
  // ToolHandle<Decays::IMCDecay> m_mcdecaytool_loki = {this, "MCDecay", "LoKi::MCDecay"};
  ToolHandle<typename LHCb::InputTypeSignature<T>::LoKiIDecay> m_decaytool_loki = {
      this, LHCb::InputTypeSignature<T>::LoKiIDecayName, "LoKi::" + LHCb::InputTypeSignature<T>::LoKiIDecayName };

  // Non-void ThOr functors (used to tuple candidate info)
  Gaudi::Property<LHCb::FTuple::vec_vec_funcdesc_t> m_functors_thor{
      this, "thor_functors", {}, "List of non-void ThOr Functors" };

  // Void ThOr functors (used to tuple event info, called once per candidate)
  Gaudi::Property<LHCb::FTuple::map_funcdesc_t> m_void_functors_thor{
      this, "void_thor_functors", {}, "Map of void ThOr Functors" };

  // Thor TBranch names
  Gaudi::Property<LHCb::FTuple::vec_vec_strings_t> m_funcfieldnames_suffix_thor{
      this, "thor_functor_field_names", LHCb::FTuple::vec_vec_strings_t(),
      "Container of list of ThOr functor TBranch names" };

  // user specified precision to cast thor functor output (currently this not available since Gaudi needs to allow this)
  Gaudi::Property<LHCb::FTuple::vec_vec_strings_t> m_funcreturntypes_thor{
      this, "thor_functor_return_types", LHCb::FTuple::vec_vec_strings_t(),
      "Container of list of ThOr functor return types" };
  LHCb::FTuple::vec_vec_strings_t m_returntypes_thor; // return types (vectors where returntypes are actually stored)

  // Thor functor factory
  ServiceHandle<Functors::IFactory> m_factory_thor{ this, "Factory", "FunctorFactory" };

  // Thor Vector of functors: containers for holding thor
  LHCb::FTuple::map_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignatureVoid> m_void_thor;
  LHCb::FTuple::vec_vec_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignature> m_fun_thor;

  // Gaudi event monitoring counter
  mutable Gaudi::Accumulators::Counter<> m_processed_evt{ this, "# processed events" };

  // container for counters of non empty events for each field
  mutable LHCb::cxx::SynchronizedValue<std::deque<Gaudi::Accumulators::Counter<>>> m_vec_nonempty_evt;

  // container for counters of empty events for each field
  mutable LHCb::cxx::SynchronizedValue<std::deque<Gaudi::Accumulators::Counter<>>> m_vec_empty_evt;

  // container for counters of events with multiple cands for each field
  mutable LHCb::cxx::SynchronizedValue<std::deque<Gaudi::Accumulators::Counter<>>> m_vec_multiple_cand_evt;

  // Property to switch on/off the full counter mode (counter for each particle defined in fields)
  Gaudi::Property<bool> m_full_counter_mode{ this, "run_full_counter_mode", true, "Run the job in full counter mode" };

  // Monitoring methods interacting with Gaudi counters
  void update_counters( const unsigned int& field_indx, const unsigned int& ncands ) const;

  // Boolean that decides whether to use LoKi decay finder or the new finder.
  // For MC Particles this is set to true by default (on the python side).
  Gaudi::Property<bool> m_use_loki_decay_finder{
      this, "use_loki_decay_finder", false,
      "Boolean that decides whether to use LoKi decay finder or the new finder. For MCParticles this is set to true by "
      "default (on the python side)." };

  // vector of parsed decay tree with new decay finder
  std::vector<detail::vec_dt_t> m_parsed_descriptor;
  // vector of parsed decay tree with loki for LHCb::Particles and LHCb::MCParticles
  std::vector<typename LHCb::InputTypeSignature<T>::LoKiIDecay::Tree> m_parsed_descriptor_loki;
};

template <class T>
FunTupleBase<T>::FunTupleBase( const std::string& name, ISvcLocator* pSvc )
    : LHCb::FTuple::Consumer<T>( name, pSvc, { "input_location", { "" } } ) {}

template <class T>
StatusCode FunTupleBase<T>::initialize() {
  // initialise consumer
  this->verbose() << "Initialising FunTupleBase algorithm" << endmsg;
  return LHCb::FTuple::Consumer<T>::initialize().andThen( [&]() -> StatusCode {
    // check if all functors are empty
    for ( const auto& mft : m_functors_loki ) { m_all_empty_lokifuncs = mft.empty() && m_all_empty_lokifuncs; }
    for ( const auto& mft : m_functors_thor ) { m_all_empty_thorfuncs = mft.empty() && m_all_empty_thorfuncs; }

    // conduct checks to see if the parsed arguments are ok
    StatusCode sc_check = checks();
    if ( sc_check.isFailure() ) { return sc_check; }

    // Using the arguments make a list of ParticleTupleProp objects that hold info on (TBranch name, Decay descriptor,
    // List of FunctorProp objects). The FunctorProp objects hold info on (functor name, functor TBranch name, functor
    // return type (can handle precision but not used currently since that functionality is missing Gaudi tupling.))
    StatusCode sc_ptp = setParticleTupleProps();
    if ( sc_ptp.isFailure() ) { return sc_ptp; }

    // Instantiate loKi functors (unfortunately the FunctorProp objects could not hold the instantiated functors).
    if ( !m_all_empty_lokifuncs ) {
      StatusCode sc_func = instantiate_loki();
      if ( sc_func.isFailure() ) { return sc_func; }
    }

    // Instantiate ThOr the functors
    if ( !m_all_empty_thorfuncs ) {
      StatusCode sc_func = instantiate_thor();
      if ( sc_func.isFailure() ) { return sc_func; }
    }

    // instantiate dynamic counters
    StatusCode sc_dynamic_count = instantiate_dynamic_counters();
    if ( sc_dynamic_count.isFailure() ) { return sc_dynamic_count; }

    // parse decay descriptors for each field
    StatusCode sc_parse = parse_decay_descriptors();
    if ( sc_parse.isFailure() ) { return sc_parse; }

    this->verbose() << "Finished initialisation" << endmsg;
    return StatusCode::SUCCESS;
  } );
}

template <class T>
StatusCode FunTupleBase<T>::instantiate_thor() {
  // prepare void functors
  StatusCode sc_func_void = prepare_thor_void( m_void_thor );
  if ( sc_func_void.isFailure() ) { return sc_func_void; }

  // prepare nonvoid functors
  StatusCode sc_func = prepare_thor( m_fun_thor );
  if ( sc_func.isFailure() ) { return this->Error( "Error in preparing ThOr functors for Particles" ); }
  return sc_func;
}

template <class T>
StatusCode FunTupleBase<T>::prepare_thor(
    LHCb::FTuple::vec_vec_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignature>& vec_vec_fun ) {
  if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Preparing ThOr functors" << endmsg; }
  // retrieve factory
  m_factory_thor.retrieve().ignore();
  for ( auto& ptp : m_particletupleprops ) {
    // we need to ensure that the reference of the functor we pass to
    // register_functor does not change!! Thus let's just default construct the
    // entire vector with the right size.
    vec_vec_fun.emplace_back();
    vec_vec_fun.back().reserve( ptp.FunctorProps_thor().size() );
    for ( auto const& tup : ptp.FunctorProps_thor() ) {
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "prepare_thor: ThOr descendant" << *( tup.Func_thor ) << endmsg;
      }
      m_factory_thor->register_functor( this, vec_vec_fun.back().emplace_back(), *( tup.Func_thor ) );
    }
  }
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "prepare_thor: ThOr container size is " << vec_vec_fun.size() << endmsg;
  }
  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTupleBase<T>::prepare_thor_void(
    LHCb::FTuple::map_functor_t<typename LHCb::InputTypeSignature<T>::ThOrSignatureVoid>& map_fun ) {
  if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Preparing ThOr functors" << endmsg; }
  // retrieve factory
  m_factory_thor.retrieve().ignore();
  for ( auto const& [key, functor_desc] : m_void_functors_thor ) {
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "prepare_thor_void: ThOr descendant" << functor_desc << endmsg;
    }
    m_factory_thor->register_functor( this, map_fun[key], functor_desc );
  }
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "prepare_thor_void: ThOr container size is " << map_fun.size() << endmsg;
  }
  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTupleBase<T>::instantiate_loki() {
  if constexpr ( LHCb::InputTypeSignature<T>::is_v1 ) {
    StatusCode sc_func = prepare_loki( m_vfun_loki, m_fun_loki, m_factory_loki );
    if ( sc_func.isFailure() ) { return this->Error( "Error in preparing LoKi functors." ); }
  } else {
    return this->Error( "LoKi functors do not work with v2 objects. Please remove them." );
  }
  return StatusCode::SUCCESS;
}

template <class T>
template <typename F, typename H>
StatusCode FunTupleBase<T>::prepare_loki( F& fun_loki, std::vector<std::vector<F>>& func_loki_vec, H& factory_loki ) {
  MsgStream debug = this->debug();
  // Instantiate all the functors (the FunctorProp objects created can't hold some reason this info)
  if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Preparing LoKi functors" << endmsg; }
  m_preamble_loki = boost::algorithm::join( m_preamble_def_loki.value(), "\n" );
  for ( const auto& ptp : m_particletupleprops ) {
    std::vector<F> i_fun;
    for ( const auto& tup : ptp.FunctorProps_loki() ) {
      if ( this->msgLevel( MSG::DEBUG ) ) { debug << "prepare_loki: Getting LoKi functor" << endmsg; }
      StatusCode sc_func_mc = factory_loki->get( tup.Func_loki, fun_loki, m_preamble_loki );
      if ( sc_func_mc.isFailure() ) {
        return this->Error( "Error in preparing functor in particle field " + ptp.FieldName() +
                            " with functor field name " + tup.Func_fieldname_loki );
      }
      if ( this->msgLevel( MSG::DEBUG ) ) {
        debug << "prepare_loki: Storing the functor into a temporary vector" << endmsg;
      }
      i_fun.push_back( fun_loki ); // different
      if ( this->msgLevel( MSG::DEBUG ) ) {
        debug << "prepare_loki: Created a function in " + ptp.FieldName() + " with " + tup.Func_fieldname_loki +
                     " : The function is '"
              << i_fun.back() << "'" << endmsg;
      }
    }
    func_loki_vec.push_back( i_fun );
  }
  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTupleBase<T>::instantiate_dynamic_counters() {
  auto append_fields = [&]( auto& counters, std::string const& prefix, bool const& full_counter_mode ) {
    return counters.with_lock( [&]( std::deque<Gaudi::Accumulators::Counter<>>& counter ) {
      if ( full_counter_mode ) {
        for ( const auto& bn : m_fieldnames_prefix ) { counter.emplace_back( this, prefix + bn ); }
        return ( counter.size() == m_fieldnames_prefix.size() )
                   ? StatusCode{ StatusCode::SUCCESS }
                   : StatusCode{ LHCb::FTuple::EC::ErrorCode::NameNumberMismatch };
      }
      counter.emplace_back( this, prefix + m_fieldnames_prefix[0] );
      return ( counter.size() == 1 ) ? StatusCode{ StatusCode::SUCCESS }
                                     : StatusCode{ LHCb::FTuple::EC::ErrorCode::NumberNotUnitary };
    } );
  };

  return append_fields( m_vec_nonempty_evt, "# non-empty events for field ", m_full_counter_mode )
      .andThen( [&] {
        return append_fields( m_vec_empty_evt, "# events without candidate for field ", m_full_counter_mode );
      } )
      .andThen( [&] {
        return append_fields( m_vec_multiple_cand_evt, "# events with multiple candidates for field ",
                              m_full_counter_mode );
      } );
}

template <class T>
StatusCode FunTupleBase<T>::checks() {
  // check tuple name
  if ( m_ntupleTname.empty() ) { return this->Error( "Error tree name is empty" ); }

  // check related to containers
  if ( m_fieldnames_prefix.empty() ) { return this->Error( "Error field names container is empty" ); }
  if ( m_decaydescriptors.empty() ) { return this->Error( "Error decay descriptor container is empty" ); }
  if ( m_all_empty_lokifuncs && m_all_empty_thorfuncs ) {
    return this->Error( "Error both ThOr and LoKi functor containers are empty" );
  }
  if ( m_funcfieldnames_suffix_loki.empty() && m_funcfieldnames_suffix_thor.empty() ) {
    return this->Error( "Error both ThOr and LoKi functor field names container is empty" );
  }
  if ( m_fieldnames_prefix.size() != m_decaydescriptors.size() ) {
    return this->Error( "Error size of field names and decay descriptors containers do not match" );
  }

  if ( !m_all_empty_lokifuncs ) {
    if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Conducting checks with LoKi " << endmsg; }
    // If the return types are not given set to a default (double), currently property not used anywhere, so does not
    // matter
    fill_returntypes( m_funcreturntypes_loki, m_funcfieldnames_suffix_loki, m_returntypes_loki );

    // check vector sizes for loki
    StatusCode sc_vect =
        checks_with_vector_sizes( m_fieldnames_prefix.size(), m_functors_loki.size(),
                                  m_funcfieldnames_suffix_loki.size(), m_returntypes_loki.size(), "LoKi" );
    if ( sc_vect.isFailure() ) { return this->Error( "The checks_with_vector_sizes method failed for LoKi" ); }
  }

  if ( !m_all_empty_thorfuncs ) {
    if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Conducting checks with ThOr " << endmsg; }
    // If the return types are not given set to a default (double), currently property not used anywhere, so does not
    // matter
    fill_returntypes( m_funcreturntypes_thor, m_funcfieldnames_suffix_thor, m_returntypes_thor );

    // check vector sizes for thor
    StatusCode sc_vect =
        checks_with_vector_sizes( m_fieldnames_prefix.size(), m_functors_thor.size(),
                                  m_funcfieldnames_suffix_thor.size(), m_returntypes_thor.size(), "ThOr" );
    if ( sc_vect.isFailure() ) { return this->Error( "The checks_with_vector_sizes method failed for ThOr" ); }
  }

  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTupleBase<T>::checks_with_vector_sizes( const int& fieldnames_size, const int& functors_size,
                                                      const int& funcfieldnames_size, const int& returntypes_size,
                                                      const std::string& functor_type ) const {
  if ( fieldnames_size != functors_size ) {
    return this->Error( functor_type + ": Error size of field names (size: " + std::to_string( fieldnames_size ) +
                        ") and does not match size of functor lists (size: " + std::to_string( functors_size ) + ")" );
  }

  if ( fieldnames_size != funcfieldnames_size ) {
    return this->Error( functor_type + ": Error size of field names and functor field names containers do not match" );
  }

  if ( fieldnames_size != returntypes_size ) {
    return this->Error( functor_type + ": Error size of field names and functor return types containers do not match" );
  }

  return StatusCode::SUCCESS;
}

template <class T>
void FunTupleBase<T>::fill_returntypes( const LHCb::FTuple::vec_vec_strings_t& funcreturntypes,
                                        const LHCb::FTuple::vec_vec_strings_t& funcfieldnames_suffix,
                                        LHCb::FTuple::vec_vec_strings_t&       vec_returntypes ) {
  if ( funcreturntypes.empty() ) {
    // If the return types are not given set to a default (double), currently property not used anywhere, so does not
    // matter
    for ( const auto& fbnm : funcfieldnames_suffix ) {
      LHCb::FTuple::vec_strings_t returntypes( fbnm.size() );
      for ( std::size_t j = 0; j < fbnm.size(); ++j ) { returntypes[j] = std::string( "double" ); }
      vec_returntypes.push_back( returntypes );
    }
  } else {
    for ( const auto& rtn : funcreturntypes ) { vec_returntypes.push_back( rtn ); }
  }
}

template <class T>
StatusCode FunTupleBase<T>::setParticleTupleProps() {
  // make a list of ParticleTupleProp objects to hold info related to particle being tupled
  if ( this->msgLevel( MSG::VERBOSE ) ) {
    this->verbose() << "Setting the properties of ParticleTupleProp objects" << endmsg;
  }
  for ( unsigned int i = 0; i < m_fieldnames_prefix.size(); i++ ) {
    ParticleTupleProp ptp = ParticleTupleProp();
    ptp.setFieldName( m_fieldnames_prefix[i] );
    ptp.setDecayDescriptor( m_decaydescriptors[i] );
    // make a list of FunctorProp objects to hold functor properties
    if ( !m_all_empty_lokifuncs ) {
      ptp.setFunctorProps_loki( m_functors_loki[i], m_funcfieldnames_suffix_loki[i], m_returntypes_loki[i] );
    }
    if ( !m_all_empty_thorfuncs ) {
      ptp.setFunctorProps_thor( m_functors_thor[i], m_funcfieldnames_suffix_thor[i], m_returntypes_thor[i] );
    }
    // conduct checks on whether input are empty or not
    if ( !ptp.checks() ) { return this->Error( "Error conducting checks with ParticleTupleProp" ); }
    m_particletupleprops.push_back( ptp );
  }

  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTupleBase<T>::parse_decay_descriptors() {
  // throw an error if m_parsed_descriptor_loki and m_parsed_descriptor are not empty
  if ( !m_parsed_descriptor_loki.empty() || !m_parsed_descriptor.empty() )
    throw GaudiException(
        "Before parsing the decay descriptors, the vector of parsed decay descriptors should be empty",
        "parse_decay_descriptors", StatusCode::FAILURE );

  // reserve memory for the vectors
  m_parsed_descriptor_loki.reserve( m_particletupleprops.size() );
  m_parsed_descriptor.reserve( m_particletupleprops.size() );
  // loop over fields
  for ( unsigned int nfield = 0; nfield < m_particletupleprops.size(); nfield++ ) {
    ParticleTupleProp ptp = m_particletupleprops[nfield];

    // print only for the first field, as the rest of field will have the same decay descriptors
    if ( nfield == 0 ) this->info() << "User specified descriptor: " << ptp.DecayDescriptor() << endmsg;

    if ( m_use_loki_decay_finder ) {
      auto decayTree = m_decaytool_loki->tree( ptp.DecayDescriptor() );
      if ( !decayTree )
        throw GaudiException( "Error parsing the decay descriptor: " + ptp.DecayDescriptor(), "parse_decay_descriptors",
                              StatusCode::FAILURE );
      m_parsed_descriptor_loki.push_back( decayTree );
    } else {
      if ( !m_decay_finder )
        throw GaudiException( "Unable to create the new decay finder. Please check!", "find_candidates",
                              StatusCode::FAILURE );

      auto tree_possibilities = m_decay_finder->getTreePossibilities( ptp.DecayDescriptor() );

      if ( tree_possibilities.empty() )
        throw GaudiException( "Error parsing the decay descriptor: " + ptp.DecayDescriptor(), "parse_decay_descriptors",
                              StatusCode::FAILURE );
      m_parsed_descriptor.push_back( tree_possibilities );

      // print the tree possibilities only for the first field as the rest of the fields will have the same tree
      if ( nfield == 0 ) {
        this->info() << "Number of decay possibilities with specified descriptor: " << tree_possibilities.size()
                     << endmsg;
        for ( unsigned int i = 0; i < tree_possibilities.size(); ++i )
          this->info() << "Possibility #" << i << ": " << tree_possibilities[i] << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

template <class T>
void FunTupleBase<T>::operator()( const T& particles ) const {
  // Messaging service
  MsgStream debug = this->debug();

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "operator(): Finding candidates" << endmsg; }
  const auto vector_of_cands = find_candidates( particles );

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "operator(): If any candidates found book nTuple" << endmsg; }
  write_tuple( vector_of_cands );

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "operator(): Moving to next event updating the counter" << endmsg; }
  ++m_processed_evt;
}

template <class T>
template <typename ObjType, LHCb::FTuple::en_if_v1<ObjType>>
void FunTupleBase<T>::write_tuple( const LHCb::FTuple::retype<T>& vector_of_cands ) const {
  // Messaging service
  MsgStream debug = this->debug();

  // quit if current event has no candidate (check the head of the decay with zero'th index)
  if ( vector_of_cands[0].empty() ) {
    if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: No candidates booking skipped" << endmsg; }
    return;
  }

  // make nTuple instance
  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Candidates found booking their info" << endmsg; }
  Tuples::Tuple ntuple = this->nTuple( m_ntupleTname );

  // total number of candidates
  std::size_t totcand{ vector_of_cands[0].size() };
  for ( unsigned int icand = 0; icand < vector_of_cands[0].size(); icand++ ) { // number of cands (write tuple)
    // if requested store the total number of candidates and the candidate index
    if ( m_store_multiple_cand_info ) fill_multiple_cand_info( ntuple, totcand, icand );

    // fill void functors
    fill_void_func_output( ntuple );
    for ( unsigned int nfield = 0; nfield < vector_of_cands.size();
          nfield++ ) { // number of fields i.e decay descriptors
      // get the particle
      const auto cand = vector_of_cands[nfield][icand];
      // fill loki functors
      if ( !m_all_empty_lokifuncs ) fill_loki_func_output( *cand, nfield, ntuple );
      // fill thor functors
      if ( !m_all_empty_thorfuncs ) fill_thor_func_output( *cand, nfield, ntuple );
    }

    if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Writing tuple after booking" << endmsg; }
    StatusCode sc_write = ntuple->write();
    if ( sc_write.isFailure() ) { this->err() << "Unable to write the tuple" << endmsg; }
  }
}

// specialisation for v2
template <class T>
template <typename ObjType, LHCb::FTuple::en_if_v2<ObjType>>
void FunTupleBase<T>::write_tuple( const LHCb::FTuple::retype<T>& vector_of_cands ) const {
  // Messaging service
  MsgStream debug = this->debug();

  // quit if current event has no candidate
  if ( vector_of_cands.empty() ) {
    if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: No candidates booking skipped" << endmsg; }
    return;
  }

  // make nTuple instance
  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Candidates found booking their info" << endmsg; }
  Tuples::Tuple ntuple = this->nTuple( m_ntupleTname );

  // For v2 Composites, the decay descriptor parsing is not implemented. Running over all particles in TES.
  // Hard code "nfield" to zero ie head of the decay here
  unsigned int nfield{ 0 };
  auto const   iterable = LHCb::Event::make_zip<SIMDWrapper::Scalar>( vector_of_cands );
  // total number of candidates
  std::size_t  totcand{ vector_of_cands.size() };
  unsigned int icand{ 0 };
  for ( auto const& cand : iterable ) { // number of cands (write tuple)
    // if requested store the total number of candidates and the candidate index
    if ( m_store_multiple_cand_info ) fill_multiple_cand_info( ntuple, totcand, icand );

    // fill void functors
    fill_void_func_output( ntuple );
    // fill thor functors
    if ( !m_all_empty_thorfuncs ) fill_thor_func_output( cand, nfield, ntuple );

    if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Writing tuple after booking" << endmsg; }
    StatusCode sc_write = ntuple->write();
    if ( sc_write.isFailure() ) { this->err() << "Unable to write the tuple" << endmsg; }

    // increment counter
    ++icand;
  }
}

template <class T>
void FunTupleBase<T>::fill_void_func_output( const Tuples::Tuple& ntuple ) const {
  // Messaging service
  MsgStream err   = this->err();
  MsgStream debug = this->debug();

  if ( this->msgLevel( MSG::DEBUG ) ) debug << "fill_loki_functors: Getting void ThOr functors" << endmsg;
  // fill event level info with void ThOr functors
  for ( const auto& [key, value] : m_void_functors_thor ) {
    auto prep = m_void_thor.at( key ).prepare();
    // fill ntuple calling the functor
    StatusCode sc_book_event = LHCb::FTuple::fill_( ntuple, key, prep() );
    // helpful notifier on what might have gone wrong
    if ( sc_book_event.isFailure() ) {
      using LHCb::FTuple::EC::ErrorCode;
      if ( sc_book_event.getCode() == static_cast<StatusCode::code_t>( ErrorCode::TypeNotSupported ) ) {
        err << "Value type returned by functor not supported. Problematic ThOr functor : " << value.code
            << " with field name: " << key << endmsg;
      } else {
        err << "fill_void_func_output: Unable to fill with ThOr functor: " << value.code << " with field name: " << key
            << endmsg;
        err << "The reason could be: " << sc_book_event.message() << endmsg;
      }
    }
  }
}

template <class T>
void FunTupleBase<T>::fill_multiple_cand_info( const Tuples::Tuple& ntuple, const std::size_t totcand,
                                               const unsigned int icand ) const {
  // A warning is raised by GaudiTupleAlg about "'unsigned long' has different sizes on 32/64 bit systems, casting
  // 'totCandidates' to 'unsigned long long'". To avoid this warning thrown after event loop, we cast the size_t to
  // unsigned long long here itself.
  // See also issue: https://gitlab.cern.ch/lhcb/DaVinci/-/issues/48
  StatusCode sc_totcand = ntuple->column( "totCandidates", static_cast<unsigned long long>( totcand ) );
  if ( sc_totcand.isFailure() )
    throw GaudiException( "Error booking the total number of candidates (totCandidates)", "write_tuple",
                          StatusCode::FAILURE );

  StatusCode sc_ncand = ntuple->column( "nCandidate", icand );
  if ( sc_ncand.isFailure() )
    throw GaudiException( "Error booking the candidate number (nCandidate)", "write_tuple", StatusCode::FAILURE );
}

template <class T>
void FunTupleBase<T>::fill_loki_func_output( const typename LHCb::InputTypeSignature<T>::LoKiItemType& cand,
                                             const unsigned int& nfield, const Tuples::Tuple& ntuple ) const {
  // Messaging service
  MsgStream err   = this->err();
  MsgStream debug = this->debug();

  ParticleTupleProp ptp = m_particletupleprops[nfield];
  // book loki functors
  if ( this->msgLevel( MSG::DEBUG ) ) debug << "fill_loki_functors: Getting Loki functors" << endmsg;
  for ( unsigned int nfunctor = 0; nfunctor < ( ptp.FunctorProps_loki() ).size(); nfunctor++ ) {
    ParticleProp::FunctorProp_loki functup   = ( ptp.FunctorProps_loki() )[nfunctor];
    std::string                    fieldname = ptp.FieldName() + "_" + functup.Func_fieldname_loki;
    if ( this->msgLevel( MSG::DEBUG ) ) {
      debug << "fill_loki_func_output: Getting Loki output for fieldname " << fieldname << endmsg;
    }
    auto val = get_loki_output( cand, nfield, nfunctor );
    if ( this->msgLevel( MSG::DEBUG ) ) {
      debug << "fill_loki_func_output: Booking Loki output for fieldname " << fieldname << endmsg;
    }
    StatusCode sc_book = LHCb::FTuple::fill_( ntuple, fieldname, val );
    if ( sc_book.isFailure() ) {
      using LHCb::FTuple::EC::ErrorCode;
      if ( sc_book.getCode() == static_cast<StatusCode::code_t>( ErrorCode::TypeNotSupported ) ) {
        err << "Value type returned by functor not supported. Problematic LoKi functor : " << functup.Func_loki
            << " with field name: " << fieldname << endmsg;
      } else {
        err << "fill_loki_func_output: Unable to fill with LoKi functor: " << functup.Func_loki
            << " with field name: " << fieldname << endmsg;
        err << "The reason could be: " << sc_book.message() << endmsg;
      }
    }
  }
}

template <class T>
void FunTupleBase<T>::fill_thor_func_output( const typename LHCb::InputTypeSignature<T>::ThOrItemType& cand,
                                             const unsigned int& nfield, const Tuples::Tuple& ntuple ) const {
  // Messaging service
  MsgStream err   = this->err();
  MsgStream debug = this->debug();

  ParticleTupleProp ptp = m_particletupleprops[nfield];
  // book ThOr functors
  if ( this->msgLevel( MSG::DEBUG ) ) debug << "fill_thor_func_output: Getting non-void ThOr functors" << endmsg;
  for ( unsigned int nfunctor = 0; nfunctor < ( ptp.FunctorProps_thor() ).size(); nfunctor++ ) {
    ParticleProp::FunctorProp_thor functup   = ( ptp.FunctorProps_thor() )[nfunctor];
    std::string                    fieldname = ptp.FieldName() + "_" + functup.Func_fieldname_thor;
    StatusCode                     sc        = [&] {
      try {
        if ( this->msgLevel( MSG::DEBUG ) ) {
          debug << "fill_thor_func_output: Getting ThOr output for fieldname " << fieldname << endmsg;
        }
        auto val = get_thor_output( cand, nfield, nfunctor );
        if ( this->msgLevel( MSG::DEBUG ) ) {
          debug << "fill_thor_func_output: Booking ThOr output for fieldname " << fieldname << endmsg;
        }
        return LHCb::FTuple::fill_( ntuple, fieldname, val );
      } catch ( const GaudiException& ex ) {
        err << "Encountered exception while processing field " << fieldname << ": " << ex << endmsg;
        return ex.code();
      }
    }();
    if ( sc.isFailure() ) {
      using LHCb::FTuple::EC::ErrorCode;
      if ( sc.getCode() == static_cast<StatusCode::code_t>( ErrorCode::TypeNotSupported ) ) {
        err << "Value type returned by functor not supported. Problematic ThOr functor : "
            << ( *( functup.Func_thor ) ).code << " with field name: " << fieldname << endmsg;
      } else {
        err << "fill_thor_func_output: Unable to fill with ThOr functor: " << ( *( functup.Func_thor ) ).code
            << " with field name: " << fieldname << endmsg;
        err << "The reason could be: " << sc.message() << endmsg;
      }
    }
  }
}

// for v1
template <class T>
template <typename ObjType, LHCb::FTuple::en_if_v1<ObjType>>
LHCb::FTuple::retype<ObjType> FunTupleBase<T>::find_candidates( const ObjType& particles ) const {
  // Messaging service
  MsgStream err   = this->err();
  MsgStream debug = this->debug();

  // item_type and idecay for mcparticles and particles
  using ParticleType = typename LHCb::InputTypeSignature<T>::LoKiItemType;
  using LoKiIDecay   = typename LHCb::InputTypeSignature<T>::LoKiIDecay;

  // make input and vector of output constvector (it has to be ConstVector for decay finder to work)
  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "find_candidates : Preparing the input particle vector" << endmsg; }
  typename ParticleType::ConstVector input;
  if ( this->msgLevel( MSG::DEBUG ) ) {
    debug << "find_candidates : Using the particles passed through the algorithm" << endmsg;
  }
  for ( const auto& ip : particles ) {
    if ( this->msgLevel( MSG::DEBUG ) ) {
      debug << "find_candidates : Filling input vector with particle" << endmsg;
      debug << "find_candidates : Particle info: " << *ip << endmsg;
    }
    const ParticleType* particle = ( ip );
    input.push_back( particle );
  }

  // define output vector of cands
  if ( this->msgLevel( MSG::DEBUG ) ) {
    debug << "find_candidates : Defining the vector of output candidates" << endmsg;
  }
  std::vector<typename ParticleType::ConstVector> vector_of_cands;

  // loop over fields
  for ( unsigned int nfield = 0; nfield < m_particletupleprops.size(); nfield++ ) { // number of fields
    // Get the tuple properties
    if ( this->msgLevel( MSG::DEBUG ) ) {
      debug << "find_candidates : Inside the loop of fields with index " << nfield << endmsg;
    }
    ParticleTupleProp ptp = m_particletupleprops[nfield];

    // define candidates vector
    typename ParticleType::ConstVector cands;

    // use loki finder or the new one
    if ( m_use_loki_decay_finder ) {
      // parse the decay descriptor

      const typename LoKiIDecay::Tree decayTree = m_parsed_descriptor_loki[nfield];
      if ( this->msgLevel( MSG::DEBUG ) ) {
        if ( nfield == 0 ) { debug << "find_candidates : Decay descriptor parsed is " << decayTree << endmsg; }
      }
      // create the decay finder
      typename LoKiIDecay::Finder finder( decayTree );
      if ( !finder ) { err << "Unable to create decay finder'" << endmsg; }
      // find the decay
      if ( this->msgLevel( MSG::DEBUG ) ) {
        debug << "find_candidates : Finding the decay for field index " << nfield << endmsg;
      }
      finder.findDecay( input.begin(), input.end(), cands );
    } else {
      detail::vec_dt_t dt_possibilities = m_parsed_descriptor[nfield];
      StatusCode       sc               = m_decay_finder->findDecay( dt_possibilities, input, cands );

      if ( sc.isFailure() )
        throw GaudiException( "Could not find any candidates with the specified descriptor. Please check!",
                              "find_candidates", StatusCode::FAILURE );
    }

    if ( this->msgLevel( MSG::DEBUG ) )
      debug << "find_candidates : Updating the counters for field index " << nfield << endmsg;

    if ( m_full_counter_mode ) {
      update_counters( nfield, cands.size() );
    } else {
      if ( nfield == 0 ) { update_counters( nfield, cands.size() ); }
    }
    // push the cands to vector of cands
    if ( this->msgLevel( MSG::DEBUG ) ) {
      debug << "find_candidates : Storing the output candidates into a vector for field index " << nfield << endmsg;
    }
    vector_of_cands.push_back( cands );
  }
  return vector_of_cands;
}

// For v2
template <class T>
template <typename ObjType, LHCb::FTuple::en_if_v2<ObjType>>
LHCb::FTuple::retype<ObjType> FunTupleBase<T>::find_candidates( const ObjType& particles ) const {
  this->info() << "find_candidates: For v2 Composites, the decay descriptor parsing is not implemented. Running over "
                  "all particles in TES."
               << endmsg;

  // The following implementation will change for now
  // copy the input particles (will have to do it eventually (?) once we have the decay descriptor matching in place)
  // make a copy of Composites (no deep copy constructor?)
  ObjType cands{ particles.zipIdentifier(), particles };
  cands.reserve( particles.size() );
  const SIMDWrapper::InstructionSet simd_inst{ SIMDWrapper::InstructionSet::Best };
  auto const                        iterable = LHCb::Event::make_zip<simd_inst>( particles );
  for ( auto const& prts : iterable ) cands.template copy_back<simd_inst>( prts );
  return cands;
}

// Monitoring the size of the current event and updating corresponding counters
template <class T>
void FunTupleBase<T>::update_counters( const unsigned int& field_indx, const unsigned int& ncands ) const {
  MsgStream debug = this->debug();
  if ( this->msgLevel( MSG::DEBUG ) ) {
    debug << "update_counters: The number of candidates found in the event are " << ncands << " for field index "
          << field_indx << endmsg;
  }
  // define a function to increment the counters
  auto increment_counter = [&field_indx]( auto& counters ) {
    return counters.with_lock( [&]( std::deque<Gaudi::Accumulators::Counter<>>& counter ) { ++counter[field_indx]; } );
  };
  if ( ncands == 0 ) {
    increment_counter( m_vec_empty_evt );
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->Warning( "update_counters: No candidates found in this event. Skipping this event", StatusCode::SUCCESS )
          .ignore();
    }
    return;
  }
  increment_counter( m_vec_nonempty_evt );

  if ( ncands > 1 ) { increment_counter( m_vec_multiple_cand_evt ); }
}

DECLARE_COMPONENT_WITH_ID( FunTupleBase<LHCb::Particle::Range>, "FunTupleBase_Particles" )
DECLARE_COMPONENT_WITH_ID( FunTupleBase<LHCb::MCParticle::Range>, "FunTupleBase_MCParticles" )
DECLARE_COMPONENT_WITH_ID( FunTupleBase<LHCb::Event::Composites>, "FunTupleBase_Composites" )
DECLARE_COMPONENT_WITH_ID( FunTupleBase<LHCb::Event::ChargedBasics>, "FunTupleBase_ChargedBasics" )
