/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"

// ThOr functor
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Functors/Optional.h"

// DetDesc
#include "DetDesc/GenericConditionAccessorHolder.h"

// Fill tuple method
#include "FillTuple.h"

namespace LHCb::FTuple {
  // typedef of Consumer algorithm
  using Consumer = Gaudi::Functional::Consumer<
      void(), Gaudi::Functional::Traits::BaseClass_t<LHCb::DetDesc::AlgorithmWithCondition<GaudiTupleAlg>>>;

  // typedefs to be used as input signature in making container of thor functors specified as GaudiProperty
  using map_funcdesc_t = std::map<std::string, ThOr::FunctorDesc>;
  using map_functor_t  = std::map<std::string, Functors::Functor<std::any()>>;
} // namespace LHCb::FTuple

// Definition of the FunTupleEventBase class
class FunTupleEventBase : public LHCb::FTuple::Consumer {
public:
  FunTupleEventBase( const std::string& name, ISvcLocator* pSvc );
  StatusCode initialize() override;
  void       operator()() const override;

protected:
  // function for preparing void thor
  StatusCode prepare_thor_void( LHCb::FTuple::map_functor_t& map_fun );
  // writing tuple
  void write_tuple() const;
  // function for filling void functors
  void fill_void_func_output( const Tuples::Tuple& ntuple ) const;

private:
  // tuple name
  Gaudi::Property<std::string> m_ntupleTname{ this, "tuple_name", "EventTuple", "Set TTree name" };
  // Void ThOr functors (used to tuple event info, called once per candidate)
  Gaudi::Property<LHCb::FTuple::map_funcdesc_t> m_void_functors_thor{
      this, "void_thor_functors", {}, "Map of void ThOr Functors" };
  // Thor functor factory
  ServiceHandle<Functors::IFactory> m_factory_thor{ this, "Factory", "FunctorFactory" };
  // Thor Vector of functors: containers for holding thor
  LHCb::FTuple::map_functor_t m_void_thor;
  // Gaudi event monitoring counter
  mutable Gaudi::Accumulators::Counter<> m_processed_evt{ this, "# processed events" };
};

FunTupleEventBase::FunTupleEventBase( const std::string& name, ISvcLocator* pSvc )
    : LHCb::FTuple::Consumer( name, pSvc, {} ) {}

StatusCode FunTupleEventBase::initialize() {
  // initialise consumer
  this->verbose() << "Initialising FunTupleEventBase algorithm" << endmsg;
  return LHCb::FTuple::Consumer::initialize().andThen( [&]() -> StatusCode {
    // check tuple name
    if ( m_ntupleTname.empty() ) { return this->Error( "Error tree name is empty" ); }
    // check that the map is not empty
    if ( m_void_functors_thor.empty() ) { return this->Error( "No functors specified, exiting..." ); }
    // Instantiate ThOr the functors
    StatusCode sc_func = prepare_thor_void( m_void_thor );
    if ( sc_func.isFailure() ) { return sc_func; }
    this->verbose() << "Finished initialisation" << endmsg;
    return StatusCode::SUCCESS;
  } );
}

StatusCode FunTupleEventBase::prepare_thor_void( LHCb::FTuple::map_functor_t& map_fun ) {
  if ( this->msgLevel( MSG::VERBOSE ) ) { this->verbose() << "Preparing ThOr functors" << endmsg; }
  // retrieve factory
  m_factory_thor.retrieve().ignore();
  for ( auto const& [key, functor_desc] : m_void_functors_thor ) {
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "prepare_thor_void: ThOr descendant" << functor_desc << endmsg;
    }
    m_factory_thor->register_functor( this, map_fun[key], functor_desc );
  }
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "prepare_thor_void: ThOr container size is " << map_fun.size() << endmsg;
  }
  return StatusCode::SUCCESS;
}

void FunTupleEventBase::operator()() const {
  // Messaging service
  MsgStream debug = this->debug();

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "operator(): Book nTuple" << endmsg; }
  write_tuple();

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "operator(): Moving to next event updating the counter" << endmsg; }
  ++m_processed_evt;
}

void FunTupleEventBase::write_tuple() const {
  // Messaging service
  MsgStream debug = this->debug();

  // make nTuple instance
  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Candidates found booking their info" << endmsg; }
  Tuples::Tuple ntuple = this->nTuple( m_ntupleTname );

  // fill void functors
  fill_void_func_output( ntuple );

  if ( this->msgLevel( MSG::DEBUG ) ) { debug << "write_tuple: Writing tuple after booking" << endmsg; }
  StatusCode sc_write = ntuple->write();
  if ( sc_write.isFailure() ) { this->err() << "Unable to write the tuple" << endmsg; }
}

void FunTupleEventBase::fill_void_func_output( const Tuples::Tuple& ntuple ) const {
  // Messaging service
  MsgStream err   = this->err();
  MsgStream debug = this->debug();

  if ( this->msgLevel( MSG::DEBUG ) ) debug << "fill_loki_functors: Getting void ThOr functors" << endmsg;
  // fill event level info with void ThOr functors
  for ( const auto& [key, value] : m_void_functors_thor ) {
    auto prep = m_void_thor.at( key ).prepare();
    // fill ntuple calling the functor
    StatusCode sc_book_event = LHCb::FTuple::fill_( ntuple, key, prep() );
    // helpful notifier on what might have gone wrong
    if ( sc_book_event.isFailure() ) {
      using LHCb::FTuple::EC::ErrorCode;
      if ( sc_book_event.getCode() == static_cast<StatusCode::code_t>( ErrorCode::TypeNotSupported ) ) {
        err << "Value type returned by functor not supported. Problematic ThOr functor : " << value.code
            << " with field name: " << key << endmsg;
      } else {
        err << "fill_void_func_output: Unable to fill with ThOr functor: " << value.code << " with field name: " << key
            << endmsg;
        err << "The reason could be: " << sc_book_event.message() << endmsg;
      }
    }
  }
}

DECLARE_COMPONENT_WITH_ID( FunTupleEventBase, "FunTupleEventBase" )
