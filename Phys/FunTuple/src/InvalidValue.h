/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/detected.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

#include <limits>
#include <type_traits>

/** @class LHCb::FTuple::InvalidValue
 * @brief Helper class that returns a default value to store when functor returns a std::nullopt.
 *
 * Templated-helper class that returns a default (invalid) value to store in nTuples
 * when a functor returns "std::nullopt". This, for example, happens with "MAP_INPUT"
 * (Functors::detail::MapRelInputToFunOutput) functor that returns a std::nullopt
 * when the entry in the relations table is empty. Note that the "MAP_INPUT" functor can also
 * return a custom invalid value for the external functor
 * if a static member variable ("invalid_value") exists in the external functor
 * class definition, for example see F.BKGCAT (Functors::detail::Background_Category).
 *
 * @see Functors::detail::MapRelInputToFunOutput
 */
namespace LHCb {
  namespace FTuple {

    template <typename T, typename = void>
    struct InvalidValue {};

    /// Invalid values for all basic C++ types (for "int" this is "0" and for "double"/"float" it is an IEEE-754 defined
    /// "nan" )
    template <typename T>
    struct InvalidValue<T, std::enable_if_t<std::numeric_limits<T>::is_specialized && !std::is_integral_v<T> &&
                                            !std::is_same_v<T, SIMDWrapper::scalar::int_v>>> {
      constexpr static T value = std::numeric_limits<T>::quiet_NaN();
    };

    /// Invalid value for SIMD::scalar::float_v, int_v
    template <>
    struct InvalidValue<SIMDWrapper::scalar::float_v> {
      constexpr static auto value = SIMDWrapper::scalar::float_v{ InvalidValue<float>::value };
    };

    /// Invalid value for LorentzVector
    template <>
    struct InvalidValue<Gaudi::LorentzVector> {
      // helper function to get invalid value
      static Gaudi::LorentzVector get_invalid_val() {
        return Gaudi::LorentzVector( InvalidValue<double>::value, InvalidValue<double>::value,
                                     InvalidValue<double>::value, InvalidValue<double>::value );
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static Gaudi::LorentzVector value;
    };
    // assign a value to static member
    inline Gaudi::LorentzVector InvalidValue<Gaudi::LorentzVector>::value =
        InvalidValue<Gaudi::LorentzVector>::get_invalid_val();

    /// Invalid value for XYZVector
    template <>
    struct InvalidValue<Gaudi::XYZVector> {
      // helper function to get invalid value
      static Gaudi::XYZVector get_invalid_val() {
        return Gaudi::XYZVector( InvalidValue<double>::value, InvalidValue<double>::value,
                                 InvalidValue<double>::value );
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static Gaudi::XYZVector value;
    };
    // assign a value to static member
    inline Gaudi::XYZVector InvalidValue<Gaudi::XYZVector>::value = InvalidValue<Gaudi::XYZVector>::get_invalid_val();

    /// Invalid value for XYZPoint
    template <>
    struct InvalidValue<Gaudi::XYZPoint> {
      // helper function to get invalid value
      static Gaudi::XYZPoint get_invalid_val() {
        return Gaudi::XYZPoint( InvalidValue<double>::value, InvalidValue<double>::value, InvalidValue<double>::value );
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static Gaudi::XYZPoint value;
    };
    // assign a value to static member
    inline Gaudi::XYZPoint InvalidValue<Gaudi::XYZPoint>::value = InvalidValue<Gaudi::XYZPoint>::get_invalid_val();

    /// Invalid value for LinAlg::Vec
    template <typename T, auto N>
    using Vec = LHCb::LinAlg::Vec<T, N>;
    template <typename T, auto N>
    struct InvalidValue<Vec<T, N>> {
      // helper function to get invalid value
      static Vec<T, N> get_invalid_val() {
        Vec<T, N> v{};
        v.m.fill( InvalidValue<T>::value );
        return v;
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static Vec<T, N> value;
    };
    // assign a value to static member
    template <typename T, auto N>
    inline Vec<T, N> InvalidValue<Vec<T, N>>::value = InvalidValue<Vec<T, N>>::get_invalid_val();

    /// Invalid value for LinAlg::Vec
    template <typename T, auto N>
    using MatSym = LHCb::LinAlg::MatSym<T, N>;
    template <typename T, auto N>
    struct InvalidValue<MatSym<T, N>> {
      // helper function to get invalid value
      static MatSym<T, N> get_invalid_val() {
        MatSym<T, N> v{};
        v.m.fill( InvalidValue<T>::value );
        return v;
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static MatSym<T, N> value;
    };
    // assign a value to static member
    template <typename T, auto N>
    inline MatSym<T, N> InvalidValue<MatSym<T, N>>::value = InvalidValue<MatSym<T, N>>::get_invalid_val();

    /// Invalid value for LinAlg::Vec
    template <typename T, auto N, auto M>
    using Mat = LHCb::LinAlg::Mat<T, N, M>;
    template <typename T, auto N, auto M>
    struct InvalidValue<Mat<T, N, M>> {
      // helper function to get invalid value
      static Mat<T, N, M> get_invalid_val() {
        Mat<T, N, M> v{};
        v.m.fill( InvalidValue<T>::value );
        return v;
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static Mat<T, N, M> value;
    };
    // assign a value to static member
    template <typename T, auto N, auto M>
    inline Mat<T, N, M> InvalidValue<Mat<T, N, M>>::value = InvalidValue<Mat<T, N, M>>::get_invalid_val();

    /// Invalid value for std::vector
    template <typename T>
    using StdVec = std::vector<T>;
    template <typename T>
    struct InvalidValue<StdVec<T>> {
      // helper function to get invalid value
      static StdVec<T> get_invalid_val() { return StdVec<T>{}; }
      static StdVec<T> value;
    };
    // assign a value to static member
    template <typename T>
    inline StdVec<T> InvalidValue<StdVec<T>>::value = InvalidValue<StdVec<T>>::get_invalid_val();

    /// Invalid value for symmetrix matrix
    template <typename T, auto N>
    using SymMatrix = ROOT::Math::SMatrix<T, N, N, ROOT::Math::MatRepSym<T, N>>;
    template <typename T, auto N>
    struct InvalidValue<SymMatrix<T, N>> {
      // helper function to get invalid value
      static SymMatrix<T, N> get_invalid_val() {
        SymMatrix<T, N> m{};
        for ( unsigned int i = 0; i < N; i++ ) {
          for ( unsigned int j = i; j < N; j++ ) { m( i, j ) = InvalidValue<T>::value; }
        }
        return m;
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static SymMatrix<T, N> value;
    };
    // assign a value to static member
    template <typename T, auto N>
    inline SymMatrix<T, N> InvalidValue<SymMatrix<T, N>>::value = InvalidValue<SymMatrix<T, N>>::get_invalid_val();

    /// Invalid value for ROOT::Math::SVector<T, N>
    template <typename T, auto N>
    using SVector = ROOT::Math::SVector<T, N>;
    template <typename T, auto N>
    struct InvalidValue<SVector<T, N>> {
      // helper function to get invalid value
      static SVector<T, N> get_invalid_val() {
        SVector<T, N> v{};
        for ( unsigned int i = 0; i < N; i++ ) { v( i ) = InvalidValue<T>::value; }
        return v;
      }
      static SVector<T, N> value;
    };
    // assign a value to static member
    template <typename T, auto N>
    inline SVector<T, N> InvalidValue<SVector<T, N>>::value = InvalidValue<SVector<T, N>>::get_invalid_val();

    /// Invalid value for LHCb::State
    template <>
    struct InvalidValue<LHCb::State> {
      // helper function to get invalid value
      static LHCb::State get_invalid_val() {
        return LHCb::State( InvalidValue<SVector<double, 5>>::value, InvalidValue<SymMatrix<double, 5>>::value,
                            InvalidValue<double>::value, LHCb::State::Location::LocationUnknown );
      }
      static LHCb::State value;
    };
    // assign a value to static member
    inline LHCb::State InvalidValue<LHCb::State>::value = InvalidValue<LHCb::State>::get_invalid_val();

    /// Invalid value for non-symmetrix matrix
    template <typename T, auto N, auto M>
    using NonSymMatrix = ROOT::Math::SMatrix<T, N, M, ROOT::Math::MatRepStd<T, N, M>>;
    template <typename T, auto N, auto M>
    struct InvalidValue<NonSymMatrix<T, N, M>> {
      // helper function to fill matrix with invalid values
      static NonSymMatrix<T, N, M> get_invalid_val() {
        NonSymMatrix<T, N, M> m{};
        for ( unsigned int i = 0; i < N; i++ ) {
          for ( unsigned int j = 0; j < M; j++ ) { m( i, j ) = InvalidValue<T>::value; }
        }
        return m;
      }
      // not a literal type. Cannot be constexpr and cannot be instantiated within the class declaration
      static NonSymMatrix<T, N, M> value;
    };
    // assign a value to static member
    template <typename T, auto N, auto M>
    inline NonSymMatrix<T, N, M> InvalidValue<NonSymMatrix<T, N, M>>::value =
        InvalidValue<NonSymMatrix<T, N, M>>::get_invalid_val();

    template <typename T>
    using invalid_value_t_ = decltype( InvalidValue<T>::value );

    template <typename T>
    inline constexpr bool has_invalid_value_v = Gaudi::cpp17::is_detected_v<invalid_value_t_, T>;

  } // namespace FTuple
} // namespace LHCb
