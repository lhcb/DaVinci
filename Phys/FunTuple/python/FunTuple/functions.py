###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import FunTupleBase_Particles  # type: ignore[import]
from PyConf.Algorithms import FunTupleBase_MCParticles
from PyConf.Algorithms import FunTupleBase_Composites
from PyConf.Algorithms import FunTupleBase_ChargedBasics
from PyConf.Algorithms import FunTupleEventBase
from .common_util import conduct_checks_and_transform
from FunTuple import FunctorCollection as FC
import Functors as F  # type: ignore[import]


def _FunTuple_Base(
    funtuple_func,
    name,
    tuple_name,
    fields,
    variables,
    inputs,
    event_variables=None,
    loki_preamble=None,
    store_run_event_numbers=True,
    run_full_counter_mode=True,
    **kwargs
):
    if not loki_preamble:
        loki_preamble = []

    # conduct some type checks and transform to output_args that can be used to set the attributes of FunTupleBase_Particles class
    output_args = conduct_checks_and_transform(
        name, tuple_name, fields, variables, loki_preamble
    )
    field_names_prefix = output_args[0]
    decay_descriptors = output_args[1]
    loki_functors = output_args[2]
    loki_functor_field_names = output_args[3]
    thor_functors = output_args[4]
    thor_functor_field_names = output_args[5]
    thor_functor_dict_event_info = {}

    # if store_run_event_numbers is True, add the run and event numbers to the event_variables
    if store_run_event_numbers:
        from PyConf.reading import get_odin  # type: ignore[import]

        odin = get_odin()
        run_event_numbers = {
            "EVENTNUMBER": F.EVENTNUMBER(odin),
            "RUNNUMBER": F.RUNNUMBER(odin),
        }
        # if user has specified event variables, add runnumber and evtnumber to it
        # else, set event_variables to run_event_numbers
        if event_variables:
            event_variables += FC(run_event_numbers)
        else:
            event_variables = FC(run_event_numbers)

    # check if event_variables is specified and extract the thor_functors
    if event_variables:
        thor_functor_dict_event_info = event_variables.get_thor_functors()

    return funtuple_func(
        name=name,
        tuple_name=tuple_name,
        field_names_prefix=field_names_prefix,
        decay_descriptors=decay_descriptors,
        loki_functors=loki_functors,
        loki_functor_field_names=loki_functor_field_names,
        thor_functors=thor_functors,
        thor_functor_field_names=thor_functor_field_names,
        loki_preamble=loki_preamble,
        input_location=inputs,
        run_full_counter_mode=run_full_counter_mode,
        void_thor_functors=thor_functor_dict_event_info,
        use_loki_decay_finder=(
            kwargs.pop("use_loki_decay_finder")
            if "use_loki_decay_finder" in kwargs
            else False
        ),
        **kwargs
    )


def FunTuple_Particles(*args, **kwargs):
    """
    Wrapper function for FunTupleBase_Particles
        Parameters:
            name (string): Name of Algorithm
            tuple_name (string): Name of the TTree.
            fields (dict): Dictionary of {field (branch) name : decay descriptor}. The field name is used in naming the variable in the ntuple and decay descriptor in finding the candidates.
            variables (dict): Dictionary of {variable name: Functor code}. The variable name is used in naming the variable in nTuple (for a given field) and functor code used to retrieve LoKi or ThOr functors.
            event_variables (FunctorCollection): FunctorCollection of {variable name: Functor code} for event quantities to be added to every candidate.
            inputs (PyConf DataHandle): Input location of LHCb::Particles to run over.
            loki_preamble (string): A LoKi preamble that renames a complex functor built out of base functors that could be used as Functor code in "variables" dictionary above.
            run_full_counter_mode (bool): A boolean to set counters and monitor each of the fields.
            use_loki_decay_finder (bool): A boolean to use the LoKi DecayFinder to find the candidates instead of the new run-3 DecayFinder.
            store_run_event_numbers (bool): A boolean to store the runnumber and evtnumber in the ntuple. Default is True.
            **kwargs: extra arguments like OutputLevel (inherited from Gaudi Algorithms) or thor_preamble (which is list of extra header files to be included when instantiating ThOr functors, usage need to be known)
        Returns:
            FunTuple PyConf algorithm corresponding to LHCb::Particles as input
    """
    return _FunTuple_Base(FunTupleBase_Particles, *args, **kwargs)


def FunTuple_MCParticles(*args, **kwargs):
    """
    Wrapper function for FunTupleBase_MCParticles
        Parameters:
            name (string): Name of Algorithm
            tuple_name (string): Name of the TTree.
            fields (dict): Dictionary of {field (branch) name : decay descriptor}. The field name is used in naming the variable in the ntuple and decay descriptor in finding the candidates.
            variables (dict): Dictionary of {variable name: Functor code}. The variable name is used in naming the variable in nTuple (for a given field) and functor code used to retrieve LoKi or ThOr functors.
            event_variables (FunctorCollection): FunctorCollection of {variable name: Functor code} for event quantities to be added to every candidate.
            inputs (PyConf DataHandle): Input location of LHCb::MCParticles to run over.
            loki_preamble (string): A LoKi preamble that renames a complex functor built out of base functors that could be used as Functor code in "variables" dictionary above.
            run_full_counter_mode (bool): A boolean to set counters and monitor each of the fields.
            use_loki_decay_finder (bool): A boolean to use the LoKi DecayFinder to find the candidates instead of the new run-3 DecayFinder.
            **kwargs: extra arguments like OutputLevel (inherited from Gaudi Algorithms) or thor_preamble (which is list of extra header files to be included when instantiating ThOr functors, usage need to be known)
        Returns:
            FunTuple PyConf algorithm corresponding to LHCb::MCParticles as input
    """
    return _FunTuple_Base(
        FunTupleBase_MCParticles,
        store_run_event_numbers=False,
        use_loki_decay_finder=True,
        *args,
        **kwargs
    )


def FunTuple_Composites(*args, **kwargs):
    """
    Wrapper function for FunTupleBase_Composites
        Parameters:
            name (string): Name of Algorithm
            tuple_name (string): Name of the TTree.
            fields (dict): Dictionary of {field (branch) name : decay descriptor}. The field name is used in naming the variable in the ntuple and decay descriptor in finding the candidates.
            variables (dict): Dictionary of {variable name: Functor code}. The variable name is used in naming the variable in nTuple (for a given field) and functor code used to retrieve ThOr functors.
            event_variables (FunctorCollection): FunctorCollection of {variable name: Functor code} for event quantities to be added to every candidate.
            inputs (PyConf DataHandle): Input location of LHCb::Event::Composites to run over.
            run_full_counter_mode (bool): A boolean to set counters and monitor each of the fields.
            store_run_event_numbers (bool): A boolean to store the runnumber and evtnumber in the ntuple. Default is True.
            **kwargs: extra arguments like OutputLevel (inherited from Gaudi Algorithms) or thor_preamble (which is list of extra header files to be included when instantiating ThOr functors, usage need to be known)
        Returns:
            FunTuple PyConf algorithm corresponding to LHCb::Event::Composites as input
    """
    return _FunTuple_Base(FunTupleBase_Composites, *args, **kwargs)


def FunTuple_ChargedBasics(*args, **kwargs):
    """
    Wrapper function for FunTupleBase_Composites
        Parameters:
            tuple_name (string): Name of the TTree.
            fields (dict): Dictionary of {field (branch) name : decay descriptor}. The field name is used in naming the variable in the ntuple and decay descriptor in finding the candidates.
            variables (dict): Dictionary of {variable name: Functor code}. The variable name is used in naming the variable in nTuple (for a given field) and functor code used to retrieve ThOr functors.
            event_variables (FunctorCollection): FunctorCollection of {variable name: Functor code} for event quantities to be added to every candidate.
            inputs (PyConf DataHandle): Input location of LHCb::Event::Composites to run over.
            run_full_counter_mode (bool): A boolean to set counters and monitor each of the fields.
            store_run_event_numbers (bool): A boolean to store the runnumber and evtnumber in the ntuple. Default is True.
            **kwargs: extra arguments like OutputLevel (inherited from Gaudi Algorithms) or thor_preamble (which is list of extra header files to be included when instantiating ThOr functors, usage need to be known)
        Returns:
            FunTuple PyConf algorithm corresponding to LHCb::Event::Composites as input
    """
    return _FunTuple_Base(FunTupleBase_ChargedBasics, *args, **kwargs)


def FunTuple_Event(
    name, tuple_name, variables=None, store_run_event_numbers=True, **kwargs
):
    """
    Wrapper function for FunTupleEventBase.
        Parameters:
            name (string): Name of Algorithm
            tuple_name (string): Name of the TTree.
            variables (dict, optional): Dictionary of {variable name: Event-level functor code}.
                The variable name is used in naming the variable in the nTuple
                and event-level functor code used to retrieve ThOr functors.
                Defaults to None.
            store_run_event_numbers (bool, optional): A boolean to store the run and event numbers in the ntuple.
                Defaults to True.
            **kwargs: extra arguments like OutputLevel (inherited from Gaudi Algorithms).
        Returns:
            FunTupleEvent PyConf algorithm.
    """
    # if store_run_event_numbers is True, add the run and event numbers to the variables
    if store_run_event_numbers:
        from PyConf.reading import get_odin

        odin = get_odin()
        run_event_numbers = {
            "EVENTNUMBER": F.EVENTNUMBER(odin),
            "RUNNUMBER": F.RUNNUMBER(odin),
        }
        # if user has specified event variables, add runnumber and evtnumber to it
        # else, set variables to run_event_numbers
        if variables:
            variables += FC(run_event_numbers)
        else:
            variables = FC(run_event_numbers)

    # check if variables is specified and extract the thor_functors
    if variables:
        thor_functor_dict_event_info = variables.get_thor_functors()

    return FunTupleEventBase(
        name=name,
        tuple_name=tuple_name,
        void_thor_functors=thor_functor_dict_event_info,
        **kwargs
    )
