###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.reading import get_odin, get_decreports
from PyConf.application import default_raw_event, configure_input
from DaVinci import Options
from DaVinci.algorithms import (
    make_fsr_algs,
    create_lines_filter,
    configured_FunTuple,
)
from tests import reset_global_store


def test_define_write_fsr():
    reset_global_store()
    """
    Check if DaVinci imports correctly the algorithm to merge and write FSRs.
    """
    options = Options(
        input_process="Gen",
        evt_max=1,
        merge_genfsr=True,
        simulation=True,
        output_file="test_define_write_fsr.root",
    )
    test_algs = make_fsr_algs(options)["GenFSR"]
    assert any("GenFSRMerge" == x.name for x in test_algs)


def test_add_hlt2_filter():
    reset_global_store()
    """
    Check if DaVinci is able to implement correctly a filter on an HLT2 line.
    """
    options = Options(
        input_raw_format=0.5,
        evt_max=1,
        simulation=False,
        input_process="Hlt2",
        input_type="ROOT",
        input_files="/Some/Dummy.DST",
    )
    configure_input(options)
    # Note here that we need to manually apply a bind to the PyConf functions
    # as they are not automatically configured in the pytests.
    # When running DV, the PyConf functions are globally configured and one must avoid
    # "binding" as much as possible.
    with default_raw_event.bind(
        raw_event_format=options.input_raw_format, stream=options.input_stream
    ):
        test_filter = create_lines_filter("test_filter_{hash}", lines=["Hlt2TESTLine"])
    assert test_filter.fullname.startswith("VoidFilter/test_filter")


def test_add_spruce_filter():
    reset_global_store()
    """
    Check if DaVinci is able to implement correctly a filter on a Sprucing line.
    """
    options = Options(
        input_raw_format=0.5,
        evt_max=1,
        simulation=False,
        input_type="ROOT",
        input_process="Spruce",
        input_stream="default",
        input_files="/Some/Dummy.DST",
    )
    with default_raw_event.bind(
        raw_event_format=options.input_raw_format, stream=options.input_stream
    ):
        test_filter = create_lines_filter(
            "test_filter_{hash}", lines=["SpruceTESTLine"]
        )
    assert test_filter.fullname.startswith("VoidFilter/test_filter")


def test_configured_funtuple():
    reset_global_store()
    """
    Check if the configured_FunTuple provides a correct instance of FunTuple.
    """
    import FunTuple.functorcollections as FC

    fields = {"B0": "[B0 -> D_s- pi+]CC"}
    variables = {"B0": FC.Kinematics()}
    config = {
        "TestTuple": {
            "location": "/Event/Spruce/SpruceTestLine/Particles",
            "filters": ["SpruceTestLine1Decision"],
            "preamble": ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"],
            "tuple": "DecayTree",
            "fields": fields,
            "variables": variables,
        }
    }
    options = Options(
        input_process="TurboPass",
        input_type="ROOT",
        input_files="/Some/Dummy.DST",
        evt_max=1,
        output_level=3,
        input_raw_format=0.5,
        simulation=False,
    )
    with default_raw_event.bind(
        raw_event_format=options.input_raw_format, stream=options.input_stream
    ):
        test_dict = configured_FunTuple(config)
    assert any(
        "FunTupleBase_Particles/Tuple_TestTuple" in alg.fullname
        for alg in test_dict["TestTuple"]
    )


def test_get_odin():
    reset_global_store()
    """
    Check if get_odin provides a correct instance of ODIN.
    """
    options = Options(
        input_process="Gen",
        evt_max=1,
        input_raw_format=0.5,
        simulation=False,
    )
    with default_raw_event.bind(raw_event_format=options.input_raw_format):
        odin = get_odin()
    assert "/Event/Decode_ODIN/ODIN" == odin.location


def test_get_decreports():
    reset_global_store()
    """
    Check if get_decreports provide a correct instance of HltDecReportsDecoder.
    """
    options = Options(
        input_raw_format=0.5,
        evt_max=1,
        simulation=False,
        input_process="TurboPass",
        input_stream="TurboSP",
        input_type="ROOT",
        input_files="/Some/Dummy.DST",
    )
    with default_raw_event.bind(
        raw_event_format=options.input_raw_format, stream=options.input_stream
    ):
        decreports = get_decreports("Hlt2")
    assert decreports.location == "/Event/Hlt2/DecReports"
