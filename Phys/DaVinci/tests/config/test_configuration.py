###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
from pathlib import Path

from PyConf.Algorithms import Gaudi__Examples__VoidConsumer as VoidConsumer
from PyConf.reading import get_particles
from PyConf.application import configure_input
from FunTuple import FunctorCollection as FC
from FunTuple import FunTuple_Particles as Funtuple
import Functors as F
from DaVinci import Options
from DaVinci.config import DVNode, prepare_davinci_nodes, make_config
from DaVinci.algorithms import create_lines_filter
from tests import reset_global_store

DIR = Path(__file__).parent.resolve()


def test_DVNode():
    """
    Basic test of the DVNode class.
    """
    node = DVNode("MyAlgs", [VoidConsumer()])

    assert node.name == "MyAlgs"


def test_prepare_davinci_nodes():
    """
    Preparation of a minimalistic node with a single algorithm (has to be passed as a list).
    """
    list_of_algs = {"alg": [VoidConsumer()]}
    prepare_davinci_nodes(list_of_algs)


def test_prepare_davinci_nodes_TypeError():
    """
    Check that DaVinci raises an error when trying to prepare a node with a set of algorithms different from a list.
    """
    list_of_algs = {"alg": VoidConsumer()}
    with pytest.raises(TypeError):
        prepare_davinci_nodes(list_of_algs)


def test_funtuple_with_xdigi():
    reset_global_store()
    """
    Check that DaVinci raises an error when running Funtuple over a .xdigi input file.
    """
    jpsi_fields = {"Jpsi": "J/psi(1S) -> mu+ mu-"}
    jpsi_vars = {"Jpsi": FC({"PX": F.PX})}

    options = Options(
        input_files=["/Some/Dummy.xdigi"],
        input_type="ROOT",
        input_process="Hlt2",
        input_raw_format=0.5,
        simulation=True,
    )

    options._input_config = configure_input(options)

    jpsi_line = "Hlt2Dummy"
    jpsi_data = get_particles(f"/Event/HLT2/{jpsi_line}/Particles")
    tuple_filter = create_lines_filter(name="Hlt2DummyFilter_{hash}", lines=[jpsi_line])
    ntuple = Funtuple(
        name="TupleJpsi_{hash}",
        tuple_name="DecayTree",
        fields=jpsi_fields,
        variables=jpsi_vars,
        inputs=jpsi_data,
    )

    algs = {"user_algs": [tuple_filter, ntuple]}
    with pytest.raises(ValueError):
        make_config(options, algs)
