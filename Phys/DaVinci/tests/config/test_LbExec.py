###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
from pydantic import ValidationError

from DaVinci.LbExec import Options


def test_Spruce_job_wrong_input_type():
    with pytest.raises(ValidationError):
        _ = Options(
            input_process="Spruce",
            input_type="RAW",
            output_type="ROOT",
            simulation=False,
            input_files=["A/Dummy/File.RAW"],
        )


def test_Spruce_job_wrong_input_output_type():
    with pytest.raises(ValidationError):
        _ = Options(
            input_process="TurboPass",
            input_type="ROOT",
            output_type="NONE",
            simulation=False,
            input_files=["A/Dummy/File.DST"],
        )
