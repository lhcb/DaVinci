#!/bin/bash
###############################################################################
# (c) Copyright 2020-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euxo pipefail

default_dir=checker-hlt1-moore-examples-$(date '+%Y%m%d-%H%M%S')
dir=${1:-$default_dir}

mkdir -p $dir
cd $dir

# Efficiency

gaudirun.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt1_moore_lines_example.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt1_eff_example_moore.py \
    -o hlt1_moore_eff_job.opts.py --all-opts \
    2>&1 | tee hlt1_moore_eff_job.log

$HLTEFFICIENCYCHECKERROOT/scripts/hlt_line_efficiencies.py \
    --reconstructible-children=mup,mum,Kp,Km \
    --legend-header="B^{0}_{s} #rightarrow J/\#psi#phi" \
    --make-plots \
    hlt1_moore_eff_ntuple.root \
    2>&1 | tee hl1_moore_eff_results.log
