###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_config as hlt1_configuration
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.topological_b import all_lines
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from Hlt2Conf.settings.defaults import get_default_hlt1_filter_code_for_hlt2

# Current reconstruction configuration for 2024
from Hlt2Conf.settings.hlt2_binds import config_pp_2024

decay = "${Bs}[B_s0 => ( J/psi(1S) => ${mup}mu+ ${mum}mu- ) ( phi(1020) => ${Kp}K+ ${Km}K- )]CC"


def make_hlt2_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_hlt2_lines
options.set_input_and_conds_from_testfiledb("expected_2024_BsToJpsiPhi_xdigi")
options.evt_max = 50
options.ntuple_file = "hlt1_and_hlt2_eff_ntuple.root"
options.scheduler_legacy_mode = False
options.simulation = True

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with hlt1_configuration.bind(
    sequence="hlt1_pp_matching_no_ut_1000KHz"
), reconstruction.bind(from_file=False), get_default_hlt1_filter_code_for_hlt2.bind(
    code=""
), config_pp_2024():
    run_chained_hlt_with_tuples(options, decay, public_tools=public_tools)
