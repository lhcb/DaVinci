###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.settings.hlt2_binds import config_pp_2024_without_UT

options.set_input_and_conds_from_testfiledb("expected_2024_min_bias_hlt1_filtered_v2")
options.evt_max = 200
options.ntuple_file = "hlt2_rate_ntuple_withoutUT.root"

with reconstruction.bind(from_file=False), config_pp_2024_without_UT():
    run_moore_with_tuples(
        options, False, public_tools=[stateProvider_with_simplified_geom()]
    )
