###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples

from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

# Current reconstruction configuration for 2024
from Hlt2Conf.settings.hlt2_binds import config_pp_2024
from Hlt2Conf.settings.defaults import get_default_hlt1_filter_code_for_hlt2

decay = "${Bs}[B_s0 => ( J/psi(1S) => ${mup}mu+ ${mum}mu- ) ( phi(1020) => ${Kp}K+ ${Km}K- )]CC"
options.set_input_and_conds_from_testfiledb("expected_2024_BsToJpsiPhi_xdigi")
options.input_raw_format = 0.5
options.evt_max = 100
options.ntuple_file = "hlt2_eff_ntuple.root"
options.scheduler_legacy_mode = False

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False), get_default_hlt1_filter_code_for_hlt2.bind(
    code=""
), config_pp_2024():
    run_moore_with_tuples(options, False, decay, public_tools=public_tools)
