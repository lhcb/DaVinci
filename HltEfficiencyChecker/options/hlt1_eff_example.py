###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_allen_in_moore_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_config as hlt1_configuration

decay = "${Bs}[B_s0 => ( J/psi(1S) => ${mup}mu+ ${mum}mu- ) ( phi(1020) => ${Kp}K+ ${Km}K- )]CC"

options.set_input_and_conds_from_testfiledb("expected_2024_BsToJpsiPhi_xdigi")
options.evt_max = 100
options.ntuple_file = "hlt1_eff_ntuple.root"
options.scheduler_legacy_mode = False
options.simulation = True

with hlt1_configuration.bind(sequence="hlt1_pp_matching_no_ut_1000KHz"):
    run_allen_in_moore_with_tuples(options, decay)
