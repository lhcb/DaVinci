#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples
from Hlt2Conf.lines.topological_b import all_lines

from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

# Current reconstruction configuration for 2024
from Hlt2Conf.settings.hlt2_binds import config_pp_2024


def make_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_lines

options.set_input_and_conds_from_testfiledb("expected_2024_min_bias_hlt1_filtered_v2")
options.input_raw_format = 0.5
options.evt_max = 100
options.scheduler_legacy_mode = False
options.simulation = True
options.ntuple_file = "hlt2_overlap_ntuple.root"

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False), config_pp_2024():
    run_moore_with_tuples(options, False, public_tools=public_tools)
