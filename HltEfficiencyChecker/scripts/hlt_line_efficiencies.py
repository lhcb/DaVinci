#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import os
import ROOT
from itertools import product
from functools import partial
from math import isclose
from HltEfficiencyChecker.config import parse_descriptor_template
import HltEfficiencyChecker.utils as utils

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(f".x {os.path.dirname(__file__)}/lhcbStyle.C")
ROOT.gStyle.SetLabelSize(0.06, "xy")
ROOT.gStyle.SetTitleOffset(0.8, "y")
ROOT.gStyle.SetTitleOffset(1.1, "x")
ROOT.gStyle.SetNdivisions(507, "y")
ROOT.gStyle.SetNdivisions(507, "x")
ROOT.gStyle.SetLegendTextSize(0.04)
ROOT.gStyle.SetPadTopMargin(0.05)
ROOT.gStyle.SetPadLeftMargin(0.12)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetPadRightMargin(0.05)


announce = partial(utils.announce, linewidth=84)


def parse_annotated_decay_descriptor(metadata: dict[str, str]):
    ret = None
    try:
        ret = parse_descriptor_template(metadata["descriptor_template"])
    except KeyError:
        raise KeyError(
            "FATAL:\t An annotated decay descriptor is required to calculate efficiencies. Please provide one when running the Moore job."
        )

    return ret


def parse_custom_denoms(args) -> dict:
    # Give unique names to the custom denominators is any are passed
    custom_denoms = {}
    if args.custom_denoms is None:
        return custom_denoms

    for i, custom_denom in enumerate(args.custom_denoms.split(",")):
        if ":" in custom_denom:
            try:
                nickname = custom_denom.split(":")[0]
                custom_cut = custom_denom.split(":")[1]
            except AttributeError:
                raise AttributeError(
                    "FATAL:\t Could not disentangle custom denominator",
                    custom_denom,
                )
        else:
            nickname = f"CustomDenom{i+1}"
            custom_cut = custom_denom

        custom_denoms[nickname] = custom_cut
    return custom_denoms


def parse_denoms(args) -> dict[str, str]:
    """Select specified denominators from known denominators. Returns dict mapping denom key to a cut string."""
    ret = {}
    known_denoms = utils.get_denoms(args.parent, args.reconstructible_children)
    custom_denoms = parse_custom_denoms(args)
    for key in args.denoms.split(","):
        if key not in known_denoms.keys():
            raise KeyError(
                f'Parsed denominator "{key}" is not implemented. Please see docs on adding new denoms.'
            )

        ret[key] = known_denoms[key]
        # add any custom denominators also
        for custom_name, custom_cut in custom_denoms.items():
            custom_key = f"{key}And{custom_name}"
            # wrap custom cut in brackets in case it contains and OR / ||
            ret[custom_key] = " && ".join([known_denoms[key], f"({custom_cut})"])

    if not ret:
        print(
            'WARNING:\t None of the denominators specified are implemented. Defaulting to "CanRecoChildren" denominator...'
        )
        ret["CanRecoChildren"] = known_denoms["CanRecoChildren"]
    return ret


def shape_denom(args, denoms):
    if args.shape_distr_with_denom:
        if args.shape_distr_with_denom not in denoms:
            raise ValueError(
                f'Requested shape denominator "{args.shape_distr_with_denom}" must be in the list of specified denoms: {denoms}.'
            )
        ret = args.shape_distr_with_denom

    else:
        ret = denoms[0]
        if len(denoms) > 1:
            print(
                "WARNING:\t More than 1 denominators specified, but --shape-distr-with-denom unspecified.\n",
                f'WARNING:\t Will draw the variable shape histos subject to the "{ret}" denominator.',
            )
        return ret


def get_group_selections(
    evt_decisions: dict[str, str], all_decisions: dict[str, str], args
):
    """Compose selections (cut strings) for groups of lines for inclusive efficiencies. Returns a dictionary of group names to selections."""
    # Create users' specified groups
    specified_groups = {}
    for effs_group in args.effs_groups.split("::"):
        name, comma_separated_lines = effs_group.split(":")
        decs_in_group = set(comma_separated_lines.split(","))
        all_in_same_level = False
        for level in utils.KNOWN_LEVELS:
            all_in_same_level |= utils.all_one_level(decs_in_group, level)
        if not all_in_same_level:
            raise RuntimeError(
                f"Can't calculate an inclusive efficiency for group {name} as it appears to contain lines from multiple trigger levels (HLT1, HLT2, etc.).\n Lines in {name}: {decs_in_group}"
            )
        specified_groups[name] = decs_in_group

    # TrueSimEff groups for specified_groups
    for particle_pfx, group_name in product(
        args.true_signal_to_match_to.split(","), specified_groups.keys()
    ):
        print(
            f"INFO:\t Creating TrueSimEff group for {particle_pfx} and {group_name}..."
        )
        specified_groups[f"{particle_pfx}_{group_name}TrueSim"] = {
            f"{particle_pfx}_{dec}TrueSim" for dec in specified_groups[group_name]
        }

    # Default groups
    # FIXME upgrade groups to use intags and outtags like overlap checker.
    # Then remove these defaults
    # Don't add the default groups unless user has asked for some groups
    default_group_intags = {
        "Hlt1TrackMVAGroup": ["Track", "MVA"],
        "Hlt1MuonGroup": ["Muon"],
        "Hlt1CharmGroup": ["D2"],
    }
    default_groups = {
        name: {
            dec
            for dec in evt_decisions.values()
            if all([tag in dec for tag in tags])
            and utils.is_level(dec, utils.KNOWN_LEVELS.Hlt1)
        }
        for name, tags in default_group_intags.items()
    }

    groups = {**specified_groups, **default_groups}
    # Require groups to be subsets of all_decisions and not empty
    groups = {
        k: v for k, v in groups.items() if v and v.issubset(set(all_decisions.values()))
    }

    missing_specified_groups = specified_groups.keys() - groups.keys()
    for missing in missing_specified_groups:
        print(
            f"WARNING:\t Group {missing} was requested but was either empty or contained decisions not in the tree. Skipping this group."
        )

    print("INFO:\t Groups of lines for inclusive efficiencies:")
    for name, group in groups.items():
        print(f"INFO:\t {name} group contains {group}.")

    # Final step: turn into selections
    ret = {
        name: f"({' || '.join(filter(None, group))})" for name, group in groups.items()
    }

    return ret


def calculate_and_print_unbinned_effs(
    rdf, denom_cuts: dict[str, str], all_selections: dict[str, str], json_path=""
):
    """Returns unbinned_effs = {denom: {selection_name: {"eff": eff, "err": err}}} and valid_denom_cuts = {denom: cut} for denoms that give events."""

    denom_ptrs = {key: rdf.Filter(cut).Count() for key, cut in denom_cuts.items()}
    numer_ptrs = {
        key: {
            sel_key: rdf.Filter(
                " && ".join(filter(None, [denom_cut, selection]))
            ).Count()
            for sel_key, selection in all_selections.items()
        }
        for key, denom_cut in denom_cuts.items()
    }

    ptrs_lists = list(denom_ptrs.values())
    for denom_key in numer_ptrs.keys():
        ptrs_lists += list(numer_ptrs[denom_key].values())

    # Calls a single event loop to filter and count each numerator and denominator
    ROOT.RDF.RunGraphs(ptrs_lists)

    denom_evts = {key: ptr.GetValue() for key, ptr in denom_ptrs.items()}

    # Filter out denoms that give no events
    bad_denom_keys = [key for key, evts in denom_evts.items() if isclose(evts, 0)]
    for bad in bad_denom_keys:
        print(
            f"WARNING:\t Denominator {bad} gives no events. Skipping this denominator."
        )
    denom_evts = {
        key: evts for key, evts in denom_evts.items() if key not in bad_denom_keys
    }
    valid_denom_cuts = {
        key: cut for key, cut in denom_cuts.items() if key not in bad_denom_keys
    }

    # Now calculate some integrated efficiencies and make some plots
    unbinned_effs = {denom: {} for denom in denom_evts.keys()}

    length = max(
        [len(sel_name) for sel_name in all_selections.keys()]
    )  # for formatting
    for denom, this_denom_evts in denom_evts.items():

        # Sort alphabetically
        numerators = dict(sorted(numer_ptrs[denom].items(), key=lambda x: x[0]))

        announce(f'Unbinned efficiencies for the lines with denominator "{denom}":')
        for numer_name, numer_ptr in numerators.items():
            eff = numer_ptr.GetValue() / this_denom_evts
            eff_bin_err = ROOT.TMath.Sqrt(eff * (1.0 - eff) / this_denom_evts)
            unbinned_effs[denom][numer_name] = {"eff": eff, "err": eff_bin_err}
            print(
                "{0:<{1}}\t Efficiency: {2:.3f} +/- {3:.3f}".format(
                    numer_name, length, eff, eff_bin_err
                )
            )
        announce(f'Finished printing unbinned efficiencies for denominator "{denom}"')

    if json_path:
        with open(json_path, "w") as f:
            json.dump(
                unbinned_effs,
                f,
                indent=2,
            )
        print(f"INFO:\t Written results to {json_path}.")

    return unbinned_effs, valid_denom_cuts


def parse_var(var, parent):
    if ":" in var:
        selected_particle, var_sfx = var.split(":")
        draw_var = f"{selected_particle}_TRUE{var_sfx}"
    else:
        # Assume parent kinematics wanted
        selected_particle = parent
        draw_var = f"{selected_particle}_TRUE{var}"
    return selected_particle, draw_var


def hist_key(var, denom_key, extra=None):
    return "__".join(filter(None, [var, denom_key, extra]))


def key_bits(hist_key):
    return hist_key.split("__")


def get_denom_from_hist_key(hist_key):
    return key_bits(hist_key)[1]


def get_sel_from_effhist_key(hist_key):
    # Will work if called on an effhist key - will return wrong thing/KeyError if used on another type of hist
    return key_bits(hist_key)[2]


def make_hists(rdf, denom_cuts, selections, args):

    # First use min, max and stddev to get the x limits for the histograms
    quantities = {}
    for var in args.vars:

        _, draw_var = parse_var(var, args.parent)
        quantities[var] = [
            rdf.Min(draw_var),
            rdf.Mean(draw_var),
            rdf.StdDev(draw_var),
        ]

    ROOT.RDF.RunGraphs(
        [value for sets in quantities.values() for value in sets]
    )  # Run event loop to precalculate required quantities for xmin and xmax

    hist_ptrs = {}

    def _make_hist(column, selection, name, nbins):
        return rdf.Filter(selection).Histo1D((name, name, nbins, xmin, xmax), column)

    def make_pass_hist(column, numerator, denom, name):
        selection = " && ".join(filter(None, [numerator, denom]))
        return _make_hist(column, selection, name, nbins=args.nbins)

    def make_total_hist(column, denom, name, for_shape=False):
        return _make_hist(
            column, denom, name, nbins=args.shape_nbins if for_shape else args.nbins
        )

    for var in args.vars:
        xmin_ptr, mean_ptr, stddev_ptr = quantities[var]
        xmin, xmax = (
            xmin_ptr.GetValue(),
            mean_ptr.GetValue() + 3 * stddev_ptr.GetValue(),
        )

        _, draw_var = parse_var(var, args.parent)

        for denom_key, denom_cut in denom_cuts.items():
            for name, selection in selections.items():
                k = hist_key(var, denom_key, name)
                hist_ptrs[k] = make_pass_hist(draw_var, selection, denom_cut, k)

            k = hist_key(var, denom_key)
            hist_ptrs[k] = make_total_hist(draw_var, denom_cut, k)

            shape_k = hist_key(var, denom_key, "shape")
            hist_ptrs[shape_k] = make_total_hist(
                draw_var, denom_cut, shape_k, for_shape=True
            )

    ROOT.RDF.RunGraphs(
        list(hist_ptrs.values())
    )  # Run event loop to fill all numerator/denominator histograms

    hists = {}
    for k, hist_ptr in hist_ptrs.items():
        hists[k] = hist_ptr.GetValue()

    # Efficiency histograms; binomial errors
    for var in args.vars:
        for denom_key, denom_cut in denom_cuts.items():
            denom_hist = hists[hist_key(var, denom_key)]
            denom_hist.SetDirectory(0)
            for name, selection in selections.items():
                eff_hist = hists[hist_key(var, denom_key, name)]
                eff_hist.SetDirectory(0)
                eff_hist.Divide(eff_hist, denom_hist, 1.0, 1.0, "B")

    return hists


def xtitle_from_var(args, var):
    if args.xtitle:
        return args.xtitle

    matching_keys = [k for k in utils.units.keys() if k in var]
    if not matching_keys:
        print(
            f'WARNING:\t No unit known for variable "{var}", writing variable as-is with no unit...'
        )
        return var

    if len(matching_keys) > 1:
        print(
            f'WARNING:\t Multiple units found for variable "{var}": {matching_keys}. Using the first one found...'
        )

    known_latex_var, unit = [(k, v) for k, v in utils.units.items() if k in var][0]
    unit_str = f" / {unit}" if unit else ""
    selected_particle, _ = parse_var(var, args.parent)
    return f"{utils.var_latex_var_map[known_latex_var]}({selected_particle}){unit_str}"


def get_marker_and_colour(indx):
    markers = [
        ("open stars", ROOT.kOpenStar),
        ("stars", ROOT.kStar),
        ("circles", ROOT.kCircle),
        ("upward triangles", ROOT.kOpenTriangleUp),
        ("downward triangles", ROOT.kOpenTriangleDown),
    ]
    colours = [
        ("red", ROOT.kRed),
        ("green", ROOT.kGreen + 2),
        ("blue", ROOT.kBlue),
        ("orange", ROOT.kOrange + 1),
        ("magenta", ROOT.kMagenta),
        ("black", ROOT.kBlack),
    ]
    return markers[indx % len(markers)], colours[indx % len(colours)]


def prettify_eff_hist(eff_hist, marker_col_indx, print_colour_and_marker):
    (marker_name, marker), (col_name, colour) = get_marker_and_colour(marker_col_indx)
    eff_hist.SetMarkerStyle(marker)
    eff_hist.SetMarkerColor(colour)
    eff_hist.SetLineColor(colour)
    eff_hist.SetTitle("")
    if print_colour_and_marker:
        sel = get_sel_from_effhist_key(eff_hist.GetName())
        denom = get_denom_from_hist_key(eff_hist.GetName())
        print(
            f"INFO:\t The {col_name} line with {marker_name} corresponds to the {sel} selection with the {denom} denom."
        )


def prettify_shape_hist(hist, xtitle):
    eff_plot_ymax = 1.1
    eff_plot_ymin = 0.0
    # These shape distributions will always control the axes
    # so handle all the axes titles and so on once, now.
    # Also scale to fit in the efficiency y-range.
    oldmax = hist.GetMaximum()
    if oldmax:
        for ibin in range(1, hist.GetNbinsX() + 1):
            hist.SetBinContent(
                ibin,
                eff_plot_ymin
                + (0.9 * (eff_plot_ymax - eff_plot_ymin) / oldmax)
                * hist.GetBinContent(ibin),
            )
    hist.SetMinimum(eff_plot_ymin)
    hist.SetMaximum(eff_plot_ymax)
    hist.GetYaxis().SetTitle("Efficiency")
    hist.GetXaxis().SetTitle(xtitle)
    hist.SetFillColorAlpha(ROOT.kGray, 0.50)
    hist.SetLineWidth(0)
    hist.SetTitle("")


def get_line_at_one(xmin, xmax):
    ret = ROOT.TGraph(ROOT.TF1("onefunc", "1.0", xmin, xmax))
    ret.SetLineStyle(ROOT.kDashed)
    ret.SetLineColor(ROOT.kBlack)
    return ret


def make_legend(
    args, var, denoms, selections, events_in_plot, shape_denom_key, logx, unbinned_effs
):
    legend = ROOT.TLegend(
        0.13 if logx else 0.53,
        0.82 - (0.1 * len(denoms) * len(selections)),
        0.53 if logx else 0.93,
        0.92,
    )
    legend.SetHeader(args.legend_header)
    shape_denom_acronym = ".".join(
        [letter for letter in shape_denom_key if letter.isupper()]
    )
    for denom_key, dec in product(denoms, selections):
        short_dec = dec
        for long in ["Decision", "Line", "TrueSim"] + [
            level.name for level in utils.KNOWN_LEVELS
        ]:
            short_dec = short_dec.replace(long, "")
        denom_acronym = ".".join([letter for letter in denom_key if letter.isupper()])
        legend.AddEntry(
            hist_key(var, denom_key, dec),
            (f"{denom_acronym}, " if len(denoms) > 1 else "") + f"{short_dec},",
            "LP",
        )
        eff_str = f"#varepsilon{' (TrueSim)' if 'TrueSim' in dec else ''}"
        legend.AddEntry(
            hist_key(var, denom_key, dec),
            eff_str
            + r" = {:.2f} #pm  {:.2f}".format(
                unbinned_effs[denom_key][dec]["eff"],
                unbinned_effs[denom_key][dec]["err"],
            ),
            "",
        )

    legend.AddEntry(
        hist_key(var, shape_denom_key, "shape"),
        f"Distribution ({shape_denom_acronym}),",
        "F",
    )
    legend.AddEntry(
        hist_key(var, shape_denom_key, "shape"),
        f"{events_in_plot:.0f} events",
        "",
    )
    legend.SetFillColorAlpha(ROOT.kWhite, 0.70)
    legend.SetLineWidth(0)

    return legend


def make_plot(args, hists, var, selections, denom_keys, unbinned_effs, logx, plot_name):
    c = ROOT.TCanvas()
    c.SetLogx(logx)

    shape_hist_denom = shape_denom(args, denom_keys)
    if len(denom_keys) > 1:
        shape_hist = hists[hist_key(var, shape_hist_denom, "shape")]
    else:
        shape_hist = hists[hist_key(var, list(denom_keys)[0], "shape")]
    prettify_shape_hist(shape_hist, xtitle_from_var(args, var))
    shape_hist.Draw("HIST")

    for i_hist, (denom, selection) in enumerate(product(denom_keys, selections)):
        this_histo = hists[hist_key(var, denom, selection)]
        prettify_eff_hist(
            this_histo,
            marker_col_indx=i_hist,
            print_colour_and_marker=args.no_legend,
        )
        this_histo.Draw("SAME PE0")

    line_at_one = get_line_at_one(
        shape_hist.GetXaxis().GetXmin(), shape_hist.GetXaxis().GetXmax()
    )
    line_at_one.Draw("SAME")
    if not args.no_legend:
        legend = make_legend(
            args,
            var,
            denom_keys,
            selections,
            hists[hist_key(var, shape_hist_denom)].Integral(),
            shape_hist_denom,
            logx,
            unbinned_effs,
        )
        legend.Draw()
    c.Print(plot_name)


def plotting(
    args,
    hists,
    line_decisions,
    true_sim_decisions,
    group_selections,
    unbinned_effs,
    denom_cuts,
):

    if args.no_legend:
        print(
            "INFO:\t --no-legend used. Instead printing the (colour, marker) of each line..."
        )

    def plot_namer(title, logx, all_lines=False, all_denoms=False):
        var_bit = f"{'log' if logx else ''}{var.replace(':','_')}"
        denom_bit = "AllDenoms" if all_denoms else ""
        line_bit = "AllLines" if all_lines else ""
        bits = "__".join(filter(None, [line_bit, denom_bit, var_bit]))
        return f"{args.plot_prefix}{title}__{bits}.{args.plot_format}"

    if args.lines:
        # If args.lines was specified, we assume the user wants to see a plot of
        # all the specified lines, along with all the specified denoms, on 1 axis
        for var, logx in product(args.vars, [True, False]):
            if args.group_denoms:
                make_plot(
                    args,
                    hists,
                    var,
                    line_decisions.keys(),
                    denom_cuts.keys(),
                    unbinned_effs,
                    logx,
                    plot_namer("Efficiencies", logx, all_lines=True, all_denoms=True),
                )

            else:
                for denom_key in denom_cuts.keys():
                    make_plot(
                        args,
                        hists,
                        var,
                        line_decisions.keys(),
                        [denom_key],
                        unbinned_effs,
                        logx,
                        plot_namer(f"Efficiencies__{denom_key}", logx, all_lines=True),
                    )

    if args.effs_groups:
        # all the specified groups for each specified denom
        for var, logx in product(args.vars, [True, False]):
            if args.group_denoms:
                make_plot(
                    args,
                    hists,
                    var,
                    group_selections.keys(),
                    denom_cuts.keys(),
                    unbinned_effs,
                    logx,
                    plot_namer("Efficiencies__Groups", logx, all_denoms=True),
                )
            else:
                for denom_key in denom_cuts.keys():
                    make_plot(
                        args,
                        hists,
                        var,
                        group_selections.keys(),
                        [denom_key],
                        unbinned_effs,
                        logx,
                        plot_namer(f"Efficiencies__Groups__{denom_key}", logx),
                    )

    # All TrueSim effs for 1 trigger line, with 1 denominator
    for var, evt_dec, logx, denom_key in product(
        args.vars, line_decisions.keys(), [True, False], denom_cuts.keys()
    ):
        true_sim_decs_this_line = list(
            filter(lambda x: evt_dec in x, true_sim_decisions.values())
        )
        decs = [evt_dec] + true_sim_decs_this_line

        make_plot(
            args,
            hists,
            var,
            decs,
            [denom_key],
            unbinned_effs,
            logx,
            plot_namer(f"Efficiencies__{evt_dec}__{denom_key}", logx),
        )

    for var, mcp, denom_key, logx in product(
        args.vars,
        args.true_signal_to_match_to.split(","),
        denom_cuts.keys(),
        [True, False],
    ):
        # Plot all TrueSim effs w.r.t. this particle
        true_sim_decisions_this_mcp = [
            true_sim_dec
            for true_sim_dec in true_sim_decisions.values()
            if true_sim_dec.split("_Hlt")[0] == mcp
        ]
        make_plot(
            args,
            hists,
            var,
            true_sim_decisions_this_mcp,
            [denom_key],
            unbinned_effs,
            logx,
            plot_namer(
                f"{mcp}__TrueSimEfficiencies__{denom_key}", logx, all_lines=True
            ),
        )


def get_parser():
    parser = argparse.ArgumentParser(
        description="Analysis script to calculate and plot efficiencies from the tuple provided. Use --help for options or visit the Moore online documentation."
    )
    parser.add_argument("input", help="Input ROOT file with tuples.")
    parser.add_argument(
        "--metadata", help="JSON input metadata (defaults to <input>.metadata.json)"
    )
    parser.add_argument(
        "--denoms",
        type=str,
        default="CanRecoChildren",
        help="Comma-separated list of efficiency denominators.",
    )
    parser.add_argument(
        "--lines",
        type=str,
        default="",
        help="Comma-separated list of lines to plot results of.",
    )
    parser.add_argument(
        "--vars",
        type=str,
        default="PT",
        help='Comma-separated list of vars to plot against. Defaults to the parent. Specify a particle by prefixing with the branch name and a colon e.g. "B_s0:PT"',
    )
    parser.add_argument(
        "--threads",
        type=int,
        default=None,
        help="Number of threads to be used (defaults to 1).",
    )
    parser.add_argument(
        "--parent", type=str, default=None, help="Parent particle name."
    )
    parser.add_argument(
        "--reconstructible-children",
        required=True,
        help="Comma-separated list of child names.",
    )
    parser.add_argument(
        "--true-signal-to-match-to",
        type=str,
        default=None,
        help="Comma-separated list of particle names (specified in the annotated decay descriptor) that you'd like to see TrueSim efficiences for.",
    )
    parser.add_argument(
        "--effs-groups",
        type=str,
        default=None,
        help="Double-colon-separated list of groups of lines for which you'd like to see the inclusive efficiency of, each prefixed with a name and a colon e.g. dimuon_lines:Hlt1DiMuonHighMassDecision,Hlt1DiMuonLowMassDecision.",
    )
    parser.add_argument(
        "--custom-denoms",
        type=str,
        default=None,
        help='Comma-separated list of custom cut strings to be applied in addition to the other denominators. E.g. you could pass "Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision" to assess an Hlt2 efficiency with respect to Hlt1. You can also pass each custom denominator a nickname by passing a name with a colon e.g "Hlt1TrackMVAs:Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision"',
    )
    parser.add_argument("--make-plots", action="store_true")
    parser.add_argument(
        "--legend-header",
        help="Give your plot a nice header. TLatex is supported e.g. J/#psi #rightarrow #mu#mu. Defaults to decay descriptor.",
    )
    parser.add_argument(
        "--xtitle",
        help='Optional plot x-axis title. TLatex supported e.g. "p_{T} (B^{0}_{s})"',
    )
    parser.add_argument("--plot-format", default="pdf")
    parser.add_argument(
        "--plot-prefix", default="", help="Prefix for the plot file paths."
    )
    parser.add_argument(
        "--shape-distr-with-denom",
        default="",
        help="For plots where more than 1 denom is shown, specify the denom cut to apply on the shape histogram shown in the background of the plot. For plots with 1 denom only, that denom is used",
    )
    parser.add_argument(
        "--group-denoms",
        action="store_true",
        help="For plotting all denoms on a single plot for each line/group. Individual plots will be created per denom if this flag is not passed.",
    )
    parser.add_argument(
        "--nbins", type=int, default=20, help="Number of efficiency bins."
    )
    parser.add_argument(
        "--shape-nbins",
        type=int,
        default=100,
        help="Number of bins the underlying variable distribution will be plotted in.",
    )
    parser.add_argument(
        "--no-legend",
        action="store_true",
        help="Don't plot the legend. Instead print the colour and marker of each line.",
    )
    parser.add_argument(
        "--json", type=str, help="If set to a filename, write results as json"
    )
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    if args.threads:
        ROOT.EnableImplicitMT(args.threads)
    else:
        print(
            'INFO: argument "threads" not specified, defaulting to single-threaded mode.'
        )
        ROOT.DisableImplicitMT()

    announce(f"INFO:\t Starting {os.path.realpath(__file__)}...")

    metadata_path = args.metadata or (f"{args.input}.metadata.json")
    metadata = {}
    if not os.path.isfile(metadata_path):
        raise FileNotFoundError(
            f"FATAL:\t Metadata file {metadata_path} not found. Should've been created by the Moore job or provided with --metadata."
        )
    with open(metadata_path) as f:
        metadata = json.load(f)

    regular_descriptor, default_children, default_parent = (
        parse_annotated_decay_descriptor(metadata)
    )
    if not args.legend_header:
        args.legend_header = regular_descriptor

    if not args.parent:
        args.parent = default_parent

    args.reconstructible_children = args.reconstructible_children.split(",")
    missing_children = set(args.reconstructible_children).difference(default_children)
    if missing_children:
        parser.error(f"reconstructible_children {missing_children} are not in input")

    denom_cuts = parse_denoms(args)

    rdf = ROOT.RDataFrame("MCFunTuple/MCDecayTree", args.input)

    line_decisions, true_sim_decisions, all_decisions = utils.get_decisions(
        rdf, args, efficiency_mode=True
    )

    # Search for definitions of groups of lines
    group_selections = {}
    if args.effs_groups:
        group_selections = get_group_selections(line_decisions, all_decisions, args)

    all_selections = {**all_decisions, **group_selections}

    unbinned_effs, valid_denom_cuts = calculate_and_print_unbinned_effs(
        rdf, denom_cuts, all_selections, args.json
    )

    if args.make_plots:
        announce("INFO:\t Making plots...")
        args.vars = args.vars.split(",")
        ROOT.TH1.SetDefaultSumw2()
        plotting(
            args,
            make_hists(rdf, denom_cuts, all_selections, args),
            line_decisions,
            true_sim_decisions,
            group_selections,
            unbinned_effs,
            valid_denom_cuts,
        )
        announce("INFO\t Finished making plots. Goodbye.")


if __name__ == "__main__":
    main()
