#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

###############################################################################
#                                                                             #
#           HltOverlapTool :    A calculator for selection overlaps           #
#                                                                             #
###############################################################################
"""
Tool for calculating the overlap between selections (typically a trigger line/
group of lines). Overlap between selections A and B are quantified as follows:
Jaccard index, J(A,B): J(A,B) = |A n B| / |A u B|
Conditional probability, P(A|B): J(A,B) = |A n B| / |B|
Raw intersection, N(A,B,...): N(A,B,...) = |A n B n ...| (up to 5 items)

Author(s): Jamie Gooding (TU Dortmund)
Date: 08.12.2023
Email(s): jagoodin@cern.ch
"""

import argparse
import itertools
import json
import matplotlib.colors as clr
import matplotlib.pyplot as plt
import os
import pandas as pd
import ROOT

pd.set_option("display.expand_frame_repr", False)
pd.set_option("display.max_rows", None)


def get_parser():
    """
    Parses arguments provided when running script.
    """
    parser = argparse.ArgumentParser(
        description="Analysis script to calculate and plot overlap metrics (conditional probability, Jaccard index, absolute intersection) from the tuple provided. Use --help for options or visit the Moore online documentation."
    )
    parser.add_argument("input", type=str, help="Input ROOT file with tuples.")
    parser.add_argument(
        "config",
        type=str,
        help="Config json file configuring overlaps and corresponding groups.",
    )
    return parser


def filter_lines(lines_in_tree, intags=[], outtags=[]):
    """
    Searches through a given set of lines to identify only trigger lines which match the criteria of:
     -- Contains all tags specified in intags
     -- Contains none of the tags specified in outtags
    """
    lines = lines_in_tree
    for intag in intags:
        lines = [line for line in lines if intag in line]
    for outtag in outtags:
        lines = [line for line in lines if outtag not in line]
    return lines


def get_linesets(rdf, overlap, config):
    """
    Compiles a set of lines corresponding to each selection (line or group) specified in an overlap from the config
    """
    lines_in_tree = [
        str(branch)
        for branch in rdf.GetColumnNames()
        if str(branch).endswith("Decision")
    ]
    linesets = {}
    if type(overlap) == dict:
        # Identifies a set of selections which match the given in/outtags
        lines = filter_lines(
            lines_in_tree,
            intags=overlap.get("intags", []),
            outtags=overlap.get("outtags", []),
        )
        linesets.update({line: [line] for line in lines})
    elif type(overlap) == list:
        # Collects a list of linesets (lists of lines), corresponding to each selection specified in the list provided
        for item in overlap:
            if item.endswith("Group"):
                group = config["groups"][item]

                lines = filter_lines(
                    lines_in_tree,
                    intags=group.get("intags", []),
                    outtags=group.get("outtags", []),
                )
            else:
                item += "Decision"
                if item not in lines_in_tree:
                    raise RuntimeError(f'Unable to identify line "{item}"')
                lines = [item]
            linesets[item.replace("Group", "")] = lines
    return linesets


def get_OR_dec(lineset):
    """
    Compiles a cut string corresponding to events passing any line in a given lineset.
    """
    cut_string = " || ".join(lineset)
    if not cut_string:
        raise RuntimeError(f'Empty cut string for lineset "{lineset}"')
    return cut_string


def calculate_overlap_metrics(rdf, overlap, config):
    """
    Calculates the Jaccard index, J(A,B), and conditional probability P(A|B) for every combination of selections A, B in a specified overlap.
    """
    linesets = get_linesets(rdf, overlap, config)
    conditional_prob_df = pd.DataFrame()

    overlap_ptrs = {}
    for (labelA, linesetA), (labelB, linesetB) in itertools.product(
        linesets.items(), linesets.items()
    ):
        cutA = get_OR_dec(
            linesetA
        )  # Obtains the condition that events pass any line in lineset A
        cutB = get_OR_dec(linesetB)

        for key, label, cut in zip(("A", "B"), (labelA, labelB), (cutA, cutB)):
            overlap_ptrs[f"count{key}_{label}"] = rdf.Filter(cut).Count()
        overlap_ptrs[f"countAnB_{labelA}_{labelB}"] = rdf.Filter(
            f"({cutA}) && ({cutB})"
        ).Count()
        overlap_ptrs[f"countAuB_{labelA}_{labelB}"] = rdf.Filter(
            f"({cutA}) || ({cutB})"
        ).Count()

    ROOT.RDF.RunGraphs(overlap_ptrs.values())

    for (labelA, linesetA), (labelB, linesetB) in itertools.product(
        linesets.items(), linesets.items()
    ):
        cutA = get_OR_dec(
            linesetA
        )  # Obtains the condition that events pass any line in lineset A
        cutB = get_OR_dec(linesetB)

        temp_df = pd.DataFrame()
        for key, label in zip(("A", "B"), (labelA, labelB)):
            temp_df[f"lineset{key}"] = pd.Series(label)  # Selection A

            temp_df[f"count{key}"] = overlap_ptrs[
                f"count{key}_{label}"
            ].GetValue()  # |A|
            temp_df[f"count{key}_err"] = ROOT.TMath.Sqrt(
                temp_df[f"count{key}"]  # Error calculated as sqrt(N)
            )

        temp_df["countAnB"] = overlap_ptrs[
            f"countAnB_{labelA}_{labelB}"
        ].GetValue()  # |A n B|
        temp_df["countAnB_err"] = ROOT.TMath.Sqrt(temp_df["countAnB"])

        temp_df["countAuB"] = overlap_ptrs[f"countAuB_{labelA}_{labelB}"].GetValue()
        temp_df["countAuB_err"] = ROOT.TMath.Sqrt(temp_df["countAuB"])

        temp_df["probA|B"] = (
            temp_df["countAnB"] / temp_df["countB"]
        )  # P(A|B) = |A n B| / |B|
        temp_df["probA|B_err"] = temp_df["probA|B"] * ROOT.TMath.Sqrt(
            1 / temp_df["countAnB"] + 1 / temp_df["countB"]
        )

        temp_df["jaccardAB"] = (
            temp_df["countAnB"] / temp_df["countAuB"]
        )  # J(A,B) = |A n B| / |A u B|
        temp_df["jaccardAB_err"] = temp_df["jaccardAB"] * ROOT.TMath.Sqrt(
            1 / temp_df["countAnB"] + 1 / temp_df["countAuB"]
        )

        if conditional_prob_df.size > 0:
            conditional_prob_df = pd.concat(
                (conditional_prob_df, temp_df), ignore_index=True
            )
        else:
            conditional_prob_df = temp_df
    return conditional_prob_df


def calculate_intersect(rdf, linesets, depth_limit=8, exclusive=False):
    """
    Computes the absolute intersections between a set of lines, for increasingly many lines (up to a given limit).
    """
    length = len(linesets)
    comb_depth = 1
    counter = 0

    intersect_dict = {}
    while comb_depth < depth_limit + 1 and comb_depth < length + 1:
        combinations = list(itertools.combinations(range(length), comb_depth))
        for combination in combinations:
            dec_string = ""
            for i, list_of_lines in enumerate(linesets.values()):
                if i not in combination and exclusive:
                    dec_string += "!"
                if i in combination or exclusive:
                    dec_string += f"({get_OR_dec(list_of_lines)}) && "
            intersect_dict[counter] = {
                "combination": combination,
                "ptr": rdf.Filter(dec_string.rsplit(" && ", 1)[0]).Count(),
            }
            counter += 1
        comb_depth += 1

    ROOT.RDF.RunGraphs([item["ptr"] for item in intersect_dict.values()])

    intersects = []
    for item in intersect_dict.values():
        intersect = {
            key: i in item["combination"] for i, key in enumerate(linesets.keys())
        }
        intersect["count"] = item["ptr"].GetValue()
        intersects.append(intersect)
    df = pd.DataFrame.from_dict(intersects)

    if comb_depth < length:
        print(f"Reached intersection depth limit of {depth_limit}.")
    return df


def get_intersections(rdf, overlap, config, lines_limit=12, depth_limit=8):
    """
    Calculates the number of events in each intersection of a set of lines, up to a certain maximum depth. Limits on number of lines and depth of combinations provided to avoid computing unnecessarily many intersections without intention.
    """
    linesets = get_linesets(rdf, overlap, config)
    if len(linesets) > lines_limit:
        raise RuntimeError(
            f"Too many lines specified to calculate intersections. {len(linesets)} specified, limit is {lines_limit}."
        )
    excl_intersect_df = calculate_intersect(
        rdf, linesets, depth_limit=depth_limit, exclusive=True
    )
    incl_intersect_df = calculate_intersect(
        rdf, linesets, depth_limit=depth_limit, exclusive=False
    )
    return excl_intersect_df, incl_intersect_df


def sanitise_labels(labels, line_chars=20):
    """
    Prepares labels for plots of overlap metrics.
    """
    for i, label in enumerate(labels):
        if label.endswith("Decision"):
            label = label.replace("Decision", "")
        if len(label) > line_chars:
            new_label = ""
            for n, char in enumerate(label):
                new_label += char
                if n % line_chars == 0 and n > 0:
                    new_label += "\n"
            label = new_label
        labels[i] = label
    return labels


def heatmap(data, labels, cbar_label, ax=None, fontsize=20, vmin=0, vmax=1):
    """
    Produces a heatmap describes a given n x n matrix.
    """
    cmap = clr.LinearSegmentedColormap.from_list(
        "overlap cmap",
        [
            "#fff7fb",
            "#ece7f2",
            "#d0d1e6",
            "#a6bddb",
            "#74a9cf",
            "#3690c0",
            "#0570b0",
            "#045a8d",
            "#023858",
        ],
        N=256,
    )  # Color-blind friendly palette generated using https://colorbrewer2.org/

    im = ax.imshow(data, cmap=cmap)
    im.set_clim(vmin, vmax)
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel(cbar_label, rotation=0, va="bottom", fontsize=fontsize)
    cbar.ax.tick_params(labelsize=fontsize)

    ax.set_ylabel("Selection $A$", fontsize=fontsize)
    ax.set_title("Selection $B$", fontsize=fontsize)

    ax.set_xticks(range(data.shape[0]))
    ax.set_yticks(range(data.shape[1]))

    fontsize = fontsize / ((len(labels) - len(labels) % 12) / 12 + 1)
    ax.set_xticklabels(labels, fontsize=fontsize)
    ax.set_yticklabels(labels, fontsize=fontsize)

    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)
    plt.setp(ax.get_xticklabels(), rotation=-45, ha="right", rotation_mode="anchor")
    ax.tick_params(which="minor", bottom=False, left=False)
    return im


def annotate_heatmap(im, errors, fontsize=20):
    """
    Writes values of each matrix cell onto a heatmap.
    """
    data = im.get_array()

    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if data[i, j] > 0.5:
                color = "w"
            else:
                color = "k"

            error = errors[i, j]

            if data[i, j] > 0:
                error_power = 1 - int(ROOT.TMath.Floor(ROOT.TMath.Log10(abs(error))))
                string = (
                    r"${0:."
                    + str(error_power)
                    + r"f}\pm"
                    + "{1:."
                    + str(error_power)
                    + "f}$"
                )
                string = string.format(data[i, j], error)
            else:
                string = r"$0 \pm 0$"

            text = im.axes.text(
                j,
                i,
                string,
                color=color,
                fontsize=fontsize,
                horizontalalignment="center",
                verticalalignment="center",
            )
            texts.append(text)

    return texts


def plot_metric(overlap, metric, df, output_dir, fontsize=16):
    """
    Plots a matrix of a given overlap metric as a heatmap.
    """

    plot_opts = {
        "probA|B": ("cond_probs", r"$P(A\|B)$", "conditional probabilities"),
        "jaccardAB": ("jaccards", r"$J(A,B)$", "Jaccard indices"),
    }

    output_suffix, metric_label, quantity = plot_opts[metric]

    fig, ax = plt.subplots(figsize=(12, 10))

    l = int(ROOT.TMath.Sqrt(len(df["linesetA"])))
    data = df[metric].values.reshape(l, l)
    errors = df[f"{metric}_err"].values.reshape(l, l)
    labels = list(df["linesetB"][:l])

    labels = sanitise_labels(labels)
    im = heatmap(data, labels, metric_label, ax=ax, fontsize=fontsize)

    if l <= 5:  # Writes values onto cells if no more than 5 rows are specified
        annotate_heatmap(im, errors, fontsize=fontsize * 2.5 / l)

    fig.tight_layout()

    for ext in ("pdf", "png"):
        full_path = f"{output_dir}{output_suffix}.{ext}"
        plt.savefig(full_path)
        print(f'INFO: Saving plot of "{overlap}" {quantity} to "{full_path}".')
    plt.close()

    return fig, ax


def main():
    parser = get_parser()
    args = parser.parse_args()

    with open(args.config) as configfile:
        config = json.load(configfile)

    if "threads" in config:
        ROOT.EnableImplicitMT(config["threads"])
    else:
        print(
            'INFO: "threads" not specified in config file, defaulting to single-threaded mode.'
        )
        ROOT.DisableImplicitMT()

    for flag in ["tables", "plots"]:
        val = config.get(flag, False)
        if type(val) != bool:
            raise RuntimeError(
                f'Config key "{flag}" should be a boolean value. Found "{val}".'
            )
        setattr(args, flag, val)

    # Parse data path
    path = ""
    if "path" in config:
        path = config["path"]
    else:
        raise RuntimeError(
            "No data path specified. Please specify a data path and try again."
        )

    if "tree" not in config:
        print('No decay tree specified. Assuming "EventFunTuple/EventTree".')
    rdf = ROOT.RDataFrame(config.get("tree", "EventFunTuple/EventTree"), args.input)

    # Calculate across overlaps
    overlaps = config["overlaps"]
    for overlap in overlaps:
        print(f'Processing overlap "{overlap}".')

        # Calculating
        overlap_metrics_df = calculate_overlap_metrics(rdf, overlaps[overlap], config)

        excl_intersect_df, incl_intersect_df = get_intersections(
            rdf,
            overlaps[overlap],
            config,
            lines_limit=config.get("intersect_max_lines", 12),
            depth_limit=config.get("intersect_max_depth", 8),
        )

        if not os.path.isdir(os.path.join(path, overlap)):
            os.makedirs(os.path.join(path, overlap))

        # Write overlap metrics data to csv files
        for metric, metric_df in zip(
            ("overlap_metrics", "exclusive_intersection", "inclusive_intersection"),
            (overlap_metrics_df, excl_intersect_df, incl_intersect_df),
        ):
            metric_path = f"{path}{overlap}/{metric}.csv"
            metric_df.to_csv(metric_path)
            print(
                f'INFO: Writing "{overlap}" {metric.replace("_", " ")} to "{metric_path}".'
            )

            # Output human-readable tables of metrics
            if args.tables:
                table_path = f"{path}{overlap}/{metric}_table.txt"
                with open(table_path, "w") as outfile:
                    outfile.write(metric_df.__repr__())
                print(
                    f'INFO: Writing "{overlap}" {metric.replace("_", " ")} table to "{metric_path}".'
                )

        # Plot overlap metrics
        if args.plots:
            for metric in ("probA|B", "jaccardAB"):
                plot_metric(
                    overlap,
                    metric,
                    overlap_metrics_df,
                    path + overlap + "/",
                )


if __name__ == "__main__":
    main()
