#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import matplotlib.pyplot as plt
import argparse
import json
import os


def get_parser():
    parser = argparse.ArgumentParser(
        description="Short script to read results from hlt_line_efficiencies.py and hlt_calculate_rates.py in json format and make a rate vs. eff plot. Use --help for options or visit the Moore online documentation."
    )
    parser.add_argument(
        "--eff-json", required=True, help="JSON input with efficiencies."
    )
    parser.add_argument("--rate-json", required=True, help="JSON input with rates.")
    parser.add_argument(
        "--denoms",
        type=str,
        default="CanRecoChildren",
        help="Comma-separated list of efficiency denominators.",
    )
    parser.add_argument(
        "--lines",
        type=str,
        default=None,
        help="Comma-separated list of lines to plot results of.",
    )
    parser.add_argument(
        "--decay-name", type=str, help="Decay short name used in titles."
    )
    parser.add_argument(
        "--all-on-one-plot",
        action="store_true",
        help="Just produce 1 plot with all lines and denoms that were asked for.",
    )
    parser.add_argument("--no-legend", action="store_true")
    parser.add_argument("--plot-format", default="pdf")
    parser.add_argument(
        "--plot-prefix", default="", help="Prefix for the plot file paths."
    )
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    print("-" * 84)
    print("INFO:\t Starting %s..." % os.path.realpath(__file__))
    print("-" * 84)

    title = (
        args.decay_name
        if args.decay_name
        else args.eff_json.replace(".json", "")
        + "__"
        + args.rate_json.replace(".json", "")
    )

    eff_results = {}
    with open(args.eff_json) as efffile:
        eff_results = json.load(efffile)

    rate_results = {}
    with open(args.rate_json) as ratefile:
        rate_results = json.load(ratefile)

    rate_lines = list(rate_results[list(rate_results.keys())[0]].keys())
    rate_lines.sort()
    rate_lines_str = ", ".join(rate_lines)

    denoms = list(eff_results[list(eff_results.keys())[0]].keys())
    print("INFO:\t Found denoms:", denoms)
    first_denom_dec_lines = [
        l
        for l in eff_results[list(eff_results.keys())[0]][denoms[0]].keys()
        if "TrueSim" not in l
    ]
    first_denom_dec_lines.sort()
    first_denom_dec_lines_str = ", ".join(first_denom_dec_lines)
    for denom in denoms:
        this_denom_dec_lines = [
            l
            for l in eff_results[list(eff_results.keys())[0]][denom].keys()
            if "TrueSim" not in l
        ]
        this_denom_dec_lines.sort()
        this_denom_dec_lines_str = ", ".join(this_denom_dec_lines)
        assert (
            this_denom_dec_lines == first_denom_dec_lines
        ), "Different set of lines found for {} ({}) and {} ({})".format(
            denom, this_denom_dec_lines_str, denoms[0], first_denom_dec_lines_str
        )

    for line in first_denom_dec_lines:
        assert (
            line in rate_lines
        ), "Didn't find line {} in the rate lines. Rate lines are {}.".format(
            line, rate_lines_str
        )

    if args.all_on_one_plot and len(denoms) > 1:
        fig, axes = plt.subplots()
        for denom in denoms:
            this_denom_dec_lines = [
                l
                for l in eff_results[list(eff_results.keys())[0]][denom].keys()
                if "TrueSim" not in l
            ]
            for line in this_denom_dec_lines:
                plt.errorbar(
                    rate_results["rates"][line],
                    eff_results["integrated_efficiencies"][denom][line],
                    yerr=eff_results["integrated_efficiencies_errors"][denom][line],
                    xerr=rate_results["rates_errors"][line],
                    label=denom + ", " + line,
                    linewidth=0,
                )
        axes.set(xlabel="Rate / kHz", ylabel="Efficiency", title=title)
        axes.tick_params(direction="in", right=True, top=True, which="both")
        if not args.no_legend:
            plt.legend(loc="best")
        fig_str = "{}RateEfficiency__AllDenoms__{}.{}".format(
            args.plot_prefix, "__".join([title, line.split("_")[0]]), args.plot_format
        )
        fig.savefig(fig_str, bbox_inches="tight")
        print("INFO:\t Saved figure " + {fig_str})

    else:
        # 1 for each denom, all lines
        for denom in denoms:
            fig, axes = plt.subplots()
            this_denom_dec_lines = [
                l
                for l in eff_results[list(eff_results.keys())[0]][denom].keys()
                if "TrueSim" not in l
            ]
            for line in this_denom_dec_lines:
                plt.errorbar(
                    rate_results["rates"][line],
                    eff_results["integrated_efficiencies"][denom][line],
                    yerr=eff_results["integrated_efficiencies_errors"][denom][line],
                    xerr=rate_results["rates_errors"][line],
                    label=line,
                    marker="+",
                )
            axes.set(xlabel="Rate / kHz", ylabel="Efficiency")
            axes.tick_params(direction="in", right=True, top=True, which="both")
            if not args.no_legend:
                plt.legend(loc="best")
            fig_str = "{}RateEfficiency__{}.{}".format(
                args.plot_prefix,
                "__".join(
                    [
                        title,
                        denom,
                    ]
                ),
                args.plot_format,
            )
            fig.savefig(fig_str, bbox_inches="tight")
            print("INFO:\t Saved figure " + fig_str)


if __name__ == "__main__":
    main()
