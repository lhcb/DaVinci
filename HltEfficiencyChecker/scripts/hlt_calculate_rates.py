#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import os
import ROOT
import HltEfficiencyChecker.utils as utils
from functools import partial

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(f".x {os.path.dirname(__file__)}/lhcbStyle.C")
ROOT.gStyle.SetOptStat(False)

announce = partial(utils.announce, linewidth=110)


def get_group_requirements(args, decisions_in_tree: dict[str, set[str]]):

    specified_groups = {level: {} for level in utils.KNOWN_LEVELS}
    for rates_group in args.rates_groups.split("::"):
        name, comma_separated_lines = rates_group.split(":")
        lines_in_group = set(comma_separated_lines.split(","))
        this_group_level = "Unclear"
        for level in utils.KNOWN_LEVELS:
            if utils.all_one_level(lines_in_group, level):
                this_group_level = level
                break
        if this_group_level == "Unclear":
            raise RuntimeError(
                f"Can't calculate an inclusive rate for group {name} as it appears to contain lines from multiple trigger levels (HLT1, HLT2, etc.).\n Lines in {name}: {lines_in_group}"
            )
        specified_groups[this_group_level][name] = lines_in_group

    default_group_intags = {
        "Hlt1TrackMVAGroup": ["Track", "MVA"],
        "Hlt1MuonGroup": ["Muon"],
        "Hlt1CharmGroup": ["D2"],
    }
    default_hlt1_groups = {
        name: {
            dec
            for dec in decisions_in_tree[utils.KNOWN_LEVELS.Hlt1]
            if all([tag in dec for tag in tags])
        }
        for name, tags in default_group_intags.items()
    }

    all_groups = {level: {} for level in utils.KNOWN_LEVELS}
    if utils.KNOWN_LEVELS.Hlt1 in all_groups.keys():
        all_groups[utils.KNOWN_LEVELS.Hlt1] = default_hlt1_groups
    for level in all_groups.keys():
        all_groups[level].update(specified_groups[level])

    # Require groups to be subsets of all_decisions and not empty
    all_groups = {
        level: {
            k: v
            for k, v in all_groups[level].items()
            if v and v.issubset(decisions_in_tree[level])
        }
        for level in utils.KNOWN_LEVELS
    }

    for level, groups in all_groups.items():
        missing_specified_groups = specified_groups[level].keys() - groups.keys()
        for missing in missing_specified_groups:
            print(
                f"WARNING:\t Group {missing} was requested but was either empty or contained decisions not in the tree. Skipping this group."
            )

        if groups:
            print(f"INFO:\t Groups of decisions for inclusive {level.name} rates:")
        for name, list_of_decs in groups.items():
            print(f"INFO:\t {name} group contains {list_of_decs}.")

    return all_groups


def make_bar_chart(
    rates: dict[str, dict[str, dict[str, float]]],
    level: utils.KNOWN_LEVELS,
    plot_path: str,
):
    if len(rates) > 10:
        print(
            f"WARNING:\t More than 10 selections for {level.name} rates. This will make the plot unreadable. Skipping..."
        )
        return

    c = ROOT.TCanvas()
    c.SetGridy()
    c.SetRightMargin(0.15)
    c.SetBottomMargin(0.3)
    ROOT.gStyle.SetHistMinimumZero()

    rate_hist = ROOT.TH1F(
        f"{level.name}_rates",
        f"{level.name} Inclusive Rates",
        len(rates),
        0,
        len(rates),
    )
    for i, sel_name in enumerate(rates):
        rate_hist.SetBinContent(i + 1, rates[sel_name]["Incl"]["rate"])
        rate_hist.SetBinError(i + 1, rates[sel_name]["Incl"]["err"])
        rate_hist.GetXaxis().SetBinLabel(i + 1, utils.sanitise_line(sel_name))

    rate_hist.SetFillColor(ROOT.kBlue - 10)
    rate_hist.SetBarWidth(0.8)
    rate_hist.SetBarOffset(0.1)
    rate_hist.GetXaxis().SetTitle("")
    rate_hist.GetYaxis().SetTitle("Rate / kHz")
    rate_hist.SetMaximum(
        max(
            [
                rates[sel_name]["Incl"]["rate"] + rates[sel_name]["Incl"]["err"]
                for sel_name in rates.keys()
            ]
        )
        * 1.1
    )
    rate_hist.Draw("HIST BAR ")
    ROOT.gStyle.SetErrorX(0)
    rate_hist.SetLineColor(ROOT.kBlack)
    rate_hist.Draw("SAME E ")

    c.Print(plot_path)


def assemble_selections(
    rdf,
    requirements: dict[str, set[str]],
    decisions_in_tree: set[str],
    filter_cut: str,
) -> dict[str, dict[str, ROOT.RDF.RResultPtr[int]]]:
    """For 1 trigger stage turn the dict of line/decision names into a dict of RResultPtrs counting events that pass the decision.
    Done for 1 stage at a time to avoid heavily-nested dictionaries."""

    def get_numer(decisions: set[str]):
        return f"({' || '.join(decisions)}) && ({filter_cut if filter_cut else '1>0'})"

    def passed_no_other_line(decisions: set[str]):
        other_lines = decisions_in_tree.difference(decisions)
        if len(other_lines) > 0:
            return "!(" + " || ".join(other_lines) + ")"
        else:
            return "(1>0)"

    count_ptrs = {}
    for req_key, req in requirements.items():
        count_ptrs[req_key] = {
            "Incl": rdf.Filter(get_numer(req)).Count(),
            "Excl": rdf.Filter(
                f"{get_numer(req)} && {passed_no_other_line(req)}"
            ).Count(),
        }

    return count_ptrs


def get_rate_filter(filter_lines: str, decisions_in_tree: dict[str, set[str]]):
    if filter_lines is None:
        return "", ""

    if filter_lines is not None:
        if filter_lines.upper() == "ANYHLT1LINE":

            if not decisions_in_tree[utils.KNOWN_LEVELS.Hlt1]:
                # at construction, decisions_in_tree will have this key, but it may be empty
                raise ValueError(
                    "FATAL:\t Requested to filter on any Hlt1 line, but can't find any Hlt1 lines in the tree to calculate rates with respect to. Did you run any?"
                )
            filter_cut = " || ".join(decisions_in_tree[utils.KNOWN_LEVELS.Hlt1])
            filter_nickname = "AnyHlt1Line"
        else:
            for line in filter_lines.split(","):
                line_found = any(
                    [
                        line in this_lvl_decisions
                        for this_lvl_decisions in decisions_in_tree.values()
                    ]
                )
                if not line_found:
                    raise RuntimeError(
                        f"Can't find specified filter line {line} in the tree."
                    )
            filter_cut = " || ".join(filter_lines.split(","))
            filter_nickname = filter_cut.replace(" || ", "OR")
    return filter_cut, filter_nickname


def calculate_and_print_rates(
    count_ptrs: dict[str, dict[str, ROOT.RDF.RResultPtr[int]]],
    denom_evts: int,
    input_rate: float,
) -> dict[str, dict[str, dict[str, float]]]:
    """
    count_ptrs = { sel_name: { incl_or_excl: ptr }
    returns rates = { sel_name: { incl_or_excl: { rate: float, err: float } } }
    """

    def do_maths(ptr):
        eff = ptr.GetValue() / denom_evts
        rate = eff * input_rate  # in kHz
        rate_err = ROOT.TMath.Sqrt(eff * (1.0 - eff) / denom_evts) * input_rate
        return {"rate": rate, "err": rate_err}

    length = max([len(sel_name) for sel_name in count_ptrs.keys()])  # for formatting

    rates = {}
    for sel_name, ptr_dict in count_ptrs.items():
        rates[sel_name] = {}
        for type_of_rate, count_ptr in ptr_dict.items():
            rates[sel_name][type_of_rate] = do_maths(count_ptr)

    # Sort alphabetically
    rates = dict(sorted(rates.items(), key=lambda x: x[0]))

    def form_msg(sel_name):
        msg = "{0:<{1}}\t ".format(sel_name, length)
        msg += f"Incl: {rates[sel_name]['Incl']['rate']:.3f} +/- {rates[sel_name]['Incl']['err']:.3f} kHz"
        msg += f",\tExcl: {rates[sel_name]['Excl']['rate']:.3f} +/- {rates[sel_name]['Excl']['err']:.3f} kHz"
        return msg

    for sel_name in rates.keys():
        if sel_name == "Total":
            continue
        print(form_msg(sel_name))

    print(form_msg("Total"))  # Make sure it's last
    return rates


def get_parser():
    parser = argparse.ArgumentParser(
        description="Analysis script to compute high level trigger rates from a tuple."
    )
    parser.add_argument("input", help="Input ROOT file with tuples.")
    parser.add_argument(
        "--input-rate",
        type=float,
        default=30000,
        help="Input rate of min. bias sample in kHz. If sample is not HLT1-filtered, should be 30000. If HLT1-filtered, number will vary by sample but be close to 1000 - please see examples.",
    )
    parser.add_argument(
        "--lines",
        type=str,
        default=None,
        help="Comma-separated list of lines to compute rates of.",
    )
    parser.add_argument(
        "--filter-lines",
        type=str,
        default=None,
        help="Comma-separated list of lines that we calculate the rate with respect to (w.r.t.) i.e. at least one of these must pass in every considered event. Intended use case is for rates of a HLT2 line w.r.t passing some HLT1 lines.",
    )
    parser.add_argument(
        "--rates-groups",
        type=str,
        default=None,
        help="double-colon-separated list of groups of lines for which you'd like to see the inclusive rate of, each prefixed with a name and a colon e.g. dimuon_lines:Hlt1DiMuonHighMassDecision,Hlt1DiMuonLowMassDecision.",
    )
    parser.add_argument(
        "--threads", type=int, default=None, help="Number of threads to be used."
    )
    parser.add_argument("--plot-rates", action="store_true")
    parser.add_argument("--plot-format", dest="plot_format", default="pdf")
    parser.add_argument("--json", help="If set to a filename, write results as json")
    parser.add_argument("--plot-prefix", default="")
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    if args.threads:
        ROOT.EnableImplicitMT(args.threads)
    else:
        print(
            'INFO: argument "threads" not specified, defaulting to single-threaded mode.'
        )
        ROOT.DisableImplicitMT()

    announce(f"INFO:\t Starting {os.path.realpath(__file__)}...")

    rdf = ROOT.RDataFrame("EventFunTuple/EventTree", args.input)
    single_dec_requirements, decisions_in_tree = utils.get_decisions(
        rdf, args, efficiency_mode=False
    )

    all_requirements = single_dec_requirements
    if args.rates_groups:
        rates_groups = get_group_requirements(args, decisions_in_tree)
        for level in all_requirements.keys():
            all_requirements[level].update(rates_groups[level])

    filter_cut, filter_nickname = get_rate_filter(args.filter_lines, decisions_in_tree)

    count_ptrs = {
        level: assemble_selections(
            rdf, all_requirements[level], decisions_in_tree[level], filter_cut
        )
        for level in all_requirements.keys()
    }

    denom_ptr = rdf.Count()
    ROOT.RDF.RunGraphs(
        # count_ptrs = { level: { sel_name: { incl_or_excl: ptr } } }
        [denom_ptr]
        + [
            ptr
            for this_lvl_ptrs in count_ptrs.values()
            for ptr_dict in this_lvl_ptrs.values()
            for ptr in ptr_dict.values()
        ]
    )

    denom_evts = denom_ptr.GetValue()
    if not denom_evts:
        raise RuntimeError("FATAL:\t Tuple seems to be empty. Aborting...")

    rates = {}
    filter_title = f" w.r.t. passing {filter_nickname}" if filter_nickname else ""
    for level, this_lvl_ptrs in count_ptrs.items():
        if not this_lvl_ptrs:
            continue

        announce(f"{level.name} rates{filter_title}:")
        rates[level] = calculate_and_print_rates(
            this_lvl_ptrs,
            denom_evts,
            args.input_rate,
        )
        announce(f"Finished printing {level.name} rates!")

    if args.json:
        with open(args.json, "w") as f:
            # KNOWN_LEVELS not JSON-serializable, so convert to strings
            json.dump({k.name: v for k, v in rates.items()}, f, indent=4)

    if args.plot_rates:
        ROOT.TH1.SetDefaultSumw2()
        announce("INFO:\t Making bar chart(s) of the rates...")

        filter_sfx = f"wrt{filter_nickname}__" if filter_nickname else ""
        for level, this_lvl_rates in rates.items():
            make_bar_chart(
                this_lvl_rates,
                level,
                f'{args.plot_prefix}Rates__{filter_sfx if filter_sfx else ""}{level.name}.{args.plot_format}',
            )

        announce("INFO\t Finished making plots. Goodbye.")


if __name__ == "__main__":
    main()
