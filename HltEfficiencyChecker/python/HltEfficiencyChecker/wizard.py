###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import subprocess
import imp
import warnings
from jinja2 import Template


def unparse_args(parser, args):
    command = []
    args = dict(args)  # will mutate args
    for action in parser._actions:
        try:
            value = args.pop(action.dest)
        except KeyError:
            continue
        if not action.option_strings:
            command.append(str(value))
        elif action.nargs == 0:
            if value != action.default:
                assert value == action.const
                command.append(action.option_strings[0])
        else:
            command.append(action.option_strings[0] + "=" + str(value))

    assert not args, f"Unrecognized arguments {args}"
    return command


def guess_args(args):
    command = []
    deprecated_args = {
        "level": 'The "level" argument is no longer necessary in the *analysis* args (still needed in "job"). Please remove it from your config file.',
        "using_hlt1_filtered_MC": 'The "using_hlt1_filtered_MC" key is no longer supported. For HLT1-filtered MC, please instead provide the HLT1 output rate corresponding to this sample with the "input_rate" key.',
    }
    for name, value in args.items():
        if name in deprecated_args.keys():
            raise DeprecationWarning(deprecated_args[name])

        if not name:
            # a `null` key means positional argument
            command.append(value)
        elif value is not None:
            command.append("--{}={}".format(name.replace("_", "-"), value))
        else:
            # a `null` value means a flag
            command.append("--{}".format(name.replace("_", "-")))
    return command


def run(command, ignore_broken_inputs):
    try:
        subprocess.check_call(command)
    except subprocess.CalledProcessError as e:
        if e.returncode == 2:
            if ignore_broken_inputs:
                print(
                    "WARNING:\t ApplicationMgr failed with a FileInput error. --ignore-broken-inputs given so continuing regardless..."
                )
            else:
                print(
                    "INFO:\t Seems there was a FileInput issue - maybe one of the LFNs was broken/mistyped?"
                )
                print(
                    "INFO:\t Try removing the offending file, or use --ignore-broken-inputs if you wish to just continue HltEfficiencyChecker after this error."
                )
                raise e
        else:
            raise e


def main(config, run_cmd_path, dry_run=False, ignore_broken_inputs=False):
    # from pprint import pprint
    # pprint(config)

    # gaudi job
    options = []
    for opts in config["job"]["options"]:
        template = None
        if "\n" in opts:
            template = Template(opts, trim_blocks=True)
        elif opts.endswith(".jinja"):
            with open(os.path.expandvars(opts)) as f:
                template = Template(f.read(), trim_blocks=True)
        if template:
            content = template.render(**config)
            content_hash = f"{hash(content):08X}"[-8:]
            opts = f".hlt_eff_checker_options_{content_hash}.py"
            # TODO if file exists, check if content is identical
            with open(opts, "w") as f:
                f.write(content)

        options.append(os.path.expandvars(opts))
    job_command = ["gaudirun.py"] + options
    print(" ".join([repr(run_cmd_path)] + list(map(repr, job_command))))
    if not dry_run:
        run(job_command, ignore_broken_inputs)

    script = os.path.expandvars(config["analysis"]["script"])
    args = config["analysis"]["args"]

    for arg_label, arg in args.items():
        if type(arg) == str and "$" in arg:
            args[arg_label] = os.path.expandvars(arg)

    try:
        get_parser = imp.load_source("", script).get_parser
        analysis_command = [script] + unparse_args(get_parser(), args)
    except Exception as e:
        warnings.warn(
            "Guessing arguments (could not unparse because of "
            "exception below)\n{}".format(e)
        )
        analysis_command = [script] + guess_args(args)

    print(" ".join([repr(run_cmd_path)] + list(map(repr, analysis_command))))
    if not dry_run:
        run(analysis_command, ignore_broken_inputs=False)
