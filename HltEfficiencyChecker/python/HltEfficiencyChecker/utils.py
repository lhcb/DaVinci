###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Define the units and axis titles that the tool knows for plotting
# Make sure that none of the keys are substrings of any other keys, or the logic in hlt_line_efficiencies.py will break.
units = {"PT": "MeV", "TAU": "ns", "P_E": "MeV", "ETA": None}
var_latex_var_map = {"PT": r"p_{T}", "TAU": r"#tau", "P_E": r"E", "ETA": r"#eta"}

from enum import Enum

KNOWN_LEVELS = Enum("KnownLevels", ["Hlt1", "Hlt2"])


def announce(message, linewidth):
    print("-" * linewidth)
    print(message)
    print("-" * linewidth)


def is_level(decision: str, level: KNOWN_LEVELS):
    if decision.endswith("TrueSim"):
        return level.name in decision
    else:
        return decision.startswith(level.name)


def all_one_level(decisions, level):
    return all(is_level(decision, level) for decision in decisions)


def sanitise_line(line):
    new_line_name = str(line)
    for ugly in ["Line", "Decision"]:
        new_line_name = new_line_name.replace(ugly, "")
    return new_line_name


def get_denoms(parent_name, child_names):

    def _cut_string(cut_list):
        return " && ".join([("(" + cut + ")") for cut in cut_list])

    efficiency_denominators = {
        "AllEvents": "1",
    }

    efficiency_denominators["CanRecoChildren"] = _cut_string(
        [
            f"{kid}_TRUEETA > 2 && {kid}_TRUEETA < 5 && {kid}_MC_RECONSTRUCTIBLE == 2"
            for kid in child_names
        ]
    )  # 2 for Charged Long tracks: see functorcollections.MCReconstructible
    efficiency_denominators["CanRecoChildrenParentCut"] = (
        efficiency_denominators["CanRecoChildren"]
        + " && "
        + _cut_string(
            [f"{parent_name}_TRUETAU > 0.0002", f"{parent_name}_TRUEPT > 2000"]
        )
    )
    efficiency_denominators["CanRecoChildrenAndChildPt"] = (
        efficiency_denominators["CanRecoChildren"]
        + " && "
        + _cut_string([f"{kid}_TRUEPT > 250" for kid in child_names])
    )

    return efficiency_denominators


def get_decisions(rdf, args, efficiency_mode=True):
    """
    Find all trigger decisions in the tree, and from those return the ones the user asked for in --lines.
    efficiency_mode triggers the additional of TrueSim branches to be calculated for efficiencies.
    If false you get the "rates" mode, where decisions are also categorised by level.
    If --lines not specified, return all decisions.
    """
    dectypes = Enum("DecisionType", ["DEC", "TRUE_SIM"])
    branch_suffixes = {dectypes.DEC: "Decision", dectypes.TRUE_SIM: "DecisionTrueSim"}
    try:
        all_branches = [str(br) for br in rdf.GetColumnNames()]
        decisions_in_tree = {
            dectype: {
                branch
                for branch in all_branches
                if any([is_level(branch, level) for level in KNOWN_LEVELS])
                and branch.endswith(branch_suffix)
            }
            for dectype, branch_suffix in branch_suffixes.items()
        }
    except AttributeError:
        raise AttributeError(
            "FATAL:\t Seems there are no branches in the tree. Was the tuple created correctly and without failure?"
        )

    specified_decisions = set(args.lines.split(",")) if args.lines else set()
    if specified_decisions:
        decisions = decisions_in_tree[dectypes.DEC].intersection(specified_decisions)
        missing_decisions = specified_decisions.difference(
            decisions_in_tree[dectypes.DEC]
        )
        for missing in missing_decisions:
            print(
                f'WARNING:\t No branch "{missing}" found in tree. Skipping this decision.'
            )
    else:
        print("INFO:\t No lines specified. Defaulting to all...")
        decisions = decisions_in_tree[dectypes.DEC]
        missing_decisions = set()

    assert (
        decisions
    ), "FATAL:\t Didn't find any decisions to calculate rates/efficiencies for. Aborting..."

    if efficiency_mode:
        # Also add TrueSim decisions (not applicable for rates on min bias)
        # and output 3 dictionaries
        from itertools import product

        if not args.true_signal_to_match_to:
            print(
                f"INFO:\t Which particles to show TrueSim efficiencies for was unspecified. Defaulting to parent ({args.parent})..."
            )
            args.true_signal_to_match_to = args.parent

        specified_true_sim_decisions = {
            f"{particle_pfx}_{dec}TrueSim"
            for particle_pfx, dec in product(
                args.true_signal_to_match_to.split(","), decisions
            )
        }
        true_sim_decisions = decisions_in_tree[dectypes.TRUE_SIM].intersection(
            specified_true_sim_decisions
        )
        missing_true_sim_decisions = specified_true_sim_decisions.difference(
            decisions_in_tree[dectypes.TRUE_SIM]
        )
        for missing in missing_true_sim_decisions:
            print(
                f'WARNING:\t No branch "{missing}" found in tree. Skipping this decision.'
            )
        # Most useful for groups, but do it also here to be uniform
        decisions = {dec: dec for dec in decisions}
        true_sim_decisions = {dec: dec for dec in true_sim_decisions}

        all_decisions = {**decisions, **true_sim_decisions}
        return decisions, true_sim_decisions, all_decisions
    else:
        # Add a "Total" decision so you can get the total rate (dont think ppl want a "total" efficiency by default)
        # Also categorise by level
        # output 2 dictionaries
        decisions_in_tree = {
            level: {
                dec for dec in decisions_in_tree[dectypes.DEC] if is_level(dec, level)
            }
            for level in KNOWN_LEVELS
        }
        # Put them as key-value pairs to be uniform with the groups later
        decisions = {
            level: {dec: {dec} for dec in decisions if is_level(dec, level)}
            for level in KNOWN_LEVELS
        }
        for level in decisions.keys():
            if decisions_in_tree[level]:
                decisions[level]["Total"] = decisions_in_tree[level]

        return decisions, decisions_in_tree
