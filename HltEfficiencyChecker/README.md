## Using HltEfficiencyChecker

Full up-to-date documentation, including how to build and run `HltEfficiencyChecker` with a series
of examples, can be found in the Moore online documentation [here](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html).
