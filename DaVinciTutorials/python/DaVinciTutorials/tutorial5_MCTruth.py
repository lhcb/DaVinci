###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""
rst_title: 5. Monte Carlo Truth
rst_description: This tutorial shows how to add Monte Carlo truth information of your candidates into your tuple.
A list of available functors can be found in `functorcollections <https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/Phys/FunTuple/python/FunTuple/functorcollections.py>`_.
The tutorial also shows how to use the algorithm `mctruth_alg`, which also outputs a map between particle and `Background Category <https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolMCBackgroundInfo>`_.
rst_running: lbexec DaVinciTutorials.tutorial5_MCTruth:main $DAVINCITUTORIALSROOT/options.yaml
rst_yaml: ../DaVinciTutorials/options.yaml
"""
from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter


def main(options: Options):
    # Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 ->^(J/psi(1S) -> mu+ mu-)  (phi(1020) ->K+ K-)",
        "Phi": "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
        "Mup": "B_s0 ->  (J/psi(1S) ->^mu+ mu-) (phi(1020) ->K+ K-)",
        "Mum": "B_s0 ->  (J/psi(1S) -> mu+ ^mu-) (phi(1020) ->K+ K-)",
        "Kp": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->^K+ K-)",
        "Km": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ ^K-)",
    }

    # Import functor collections Kinematics, MCKinematics, MCHierarchy
    # (Note: import statements should really be at the top of files for almost all use-cases.
    # The local import is here to be close to the comments.)
    # There is also "MCVertexInfo" but we won't import it here.
    #
    # See what's available at: https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/Phys/FunTuple/python/FunTuple/functorcollections.py
    import FunTuple.functorcollections as FC

    # We can seek help on these functorcollections using following commands (if you run these commands, press "q" to exit and continue).
    # - print(help(FC.MCKinematics))
    # - print(help(FC.MCHierarchy))
    #
    # We see that it takes an input an algorithm MCTruthAndBkgCat(inputdata), so lets import that.
    from DaVinciMCTools import MCTruthAndBkgCat

    # Load data from dst onto a TES
    turbo_line = "Hlt2B2CC_BsToJpsiPhi_Detached"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")
    # Define an algorithm that builds a map i.e. one-to-one relation b/w Reco Particle -> Truth MC Particle.
    MCTRUTH = MCTruthAndBkgCat(input_data, name="MCTruthAndBkgCat_tuto")
    # print(mctruth_alg.MCAssocTable)

    # Pass it to collections
    kin = FC.Kinematics()
    mckin = FC.MCKinematics(mctruth_alg=MCTRUTH)
    mchierarchy = FC.MCHierarchy(mctruth_alg=MCTRUTH)
    # print(mckin)
    # print(mchierarchy)

    # Loop over and keep only what's required
    kin = FunctorCollection(
        {k: v for k, v in kin.get_thor_functors().items() if k == "P" or k == "M"}
    )
    mckin = FunctorCollection(
        {k: v for k, v in mckin.get_thor_functors().items() if k == "TRUEP"}
    )
    mchierarchy = FunctorCollection(
        {k: v for k, v in mchierarchy.get_thor_functors().items() if k == "TRUEID"}
    )
    # print(kin)
    # print(mckin)
    # print(mchierarchy)

    # To get truth information with a functor
    extra_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEEID": F.VALUE_OR(0) @ MCTRUTH(F.PARTICLE_ID),
        "TRUEEPHI": MCTRUTH(F.PHI),
    }
    extra_info = FunctorCollection(extra_info)

    # The algorithm mctruth_alg also outputs a map b/w particle and bkg category which can be obtained using the functor
    # For more info on background category see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolMCBackgroundInfo
    bkg_cat = FunctorCollection({"BKGCAT": MCTRUTH.BkgCat})

    # Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": kin + mckin + mchierarchy + bkg_cat, "Kp": extra_info}

    # Add a filter
    my_filter = create_lines_filter("HDRFilter_SeeNoEvil", lines=[f"{turbo_line}"])

    # Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        inputs=input_data,
    )

    config = make_config(options, [my_filter, mytuple])
    return config
